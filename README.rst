Who Is Sweeper Bot?
===================

Overview
--------

Sweeper Bot is the main Discord bot that powers the r/DestinyTheGame `Discord Server <https://discord.gg/DestinyReddit>`_ and GTFO `Discord Servers <https://discord.gg/GTFO>`_. She handles a lot of functions for us, mostly in the space of moderation tools, including but not limited to:

- Notes, Warnings, Mutes, Kicks, and Banning.
- Logging of deleted messages, reactions, voice activity, member joins/parts, and name/profile picture changes.
- AntiSpam features including auto muting those sending too many messages too quickly.
- Reaction based role assignment.
- Suggestions creation with voting reactions.
- Tags for call/response of frequent information.
- Reminders to be automatically reminded of something in the future.
- Mod Mail which allows users to send the bot a message and have it relayed to our mods and allows our mods to respond.

Private Version
----------------

We have a few private versions of Sweeper Bot running for the DestinyReddit, GTFO, and many other communities.

Public Version
---------------

We also have a Discord Verified public version of Sweeper Bot by the name of ``@Benedict 99-40#6997`` (511242194749882387) which is in 75+ communities. You can invite that bot with `this invite link <https://discord.com/oauth2/authorize?client_id=511242194749882387&scope=bot&permissions=419818743>`_.

Bot Dependencies
-----------------

To build the bot the following requirements must be satisfied:

- Python 3.7
- pipenv
- Redis
- PostgreSQL (10-11 tested)
- Sentry.io for error reporting

Installing the Major Components
--------------------------------

Redis:

- See the following for installing Redis as a Cluster (for High Availability): https://redis.io/topics/cluster-tutorial

PostgreSQL:

- See the following for installing PostreSQL on Ubuntu: https://www.digitalocean.com/community/tutorials/how-to-install-and-use-postgresql-on-ubuntu-20-04

Building The Bot
-----------------

Installing the bot:

- Make a copy of the ``botconfig.example.ini`` and rename to ``botconfig.ini``, then adjust the settings.
- At the root of the project run ``pipenv install --verbose --skip-lock -e .`` which will install all the packages needed.
- Change directories (cd) into the 'sweeperbot' subdirectory and run ``pipenv run python3 launch.py``
- Congrats, the bot is now running.

Additional notes:

- If a table does not exist, the bot will create the entire table for you, so long as the ``CREATE_TABLES`` is set to True. If the table exists and modifications are made then you'll need to manually update the table in the database.
- The bot follows the mentality that the only configuration file should be botconfig.ini and any data is either stored in the redis cache or the database. This allows for the bot to be destroyed and rebuilt without fear of data loss such as during auto deployments on version updates using Bamboo.
