import configparser
import logging
import logging.config
import sys
import time
from base64 import b64decode
from os import environ
from os.path import abspath, dirname, join

import coloredlogs
import discord
from layer7_utilities import LoggerConfig

from _version import __version__
from constants import __botname__

curdir = join(abspath(dirname(__file__)))
parentdir = join(curdir, "../")

botconfig = configparser.ConfigParser()
botconfig.read(join(parentdir, "botconfig.ini"))
# The Discord Token is a combination of a base 64 of user ID, timestamp, and crypto key.
# See here: https://imgur.com/7WdehGn
__token__ = botconfig.get("BotConfig", "DISCORDTOKEN")
tmp_token_split = __token__.split(".")
userid = int(b64decode(tmp_token_split[0]))
# Sets Database Encryption Key to Environment Variables
environ["db_encryption_key"] = botconfig.get("Database", "ENCRYPTION_KEY")

# Import Bot needs to come after setting the Encryption key otherwise the models.py won't be able to read it
from bot import MyBot

extensions = [
    # Mod commands
    "cogs.modtools.warn",
    "cogs.modtools.ban",
    "cogs.modtools.kick",
    "cogs.modtools.alert",
    "cogs.modtools.vote",
    "cogs.modtools.say",
    "cogs.modtools.allroles",
    "cogs.modtools.mute",
    "cogs.modtools.note",
    "cogs.modtools.history",
    "cogs.modtools.role",
    "cogs.modtools.send",
    "cogs.modtools.purge",
    "cogs.modtools.serverinfo",
    "cogs.modtools.rra_role_assignment",
    "cogs.modtools.blacklist",
    "cogs.modtools.publish",
    "cogs.modtools.nickname",
    "cogs.modtools.cleaninvites",
    # Misc commands
    "cogs.misc.ping",
    "cogs.misc.tags",
    "cogs.misc.botinfo",
    "cogs.misc.suggestion",
    "cogs.misc.reminder",
    "cogs.misc.guildstats",
    # Config commands
    "cogs.config.config",
    "cogs.config.modmail",
    # "cogs.config.setup",  # TO DO - Finish implementing
    "cogs.config.prefix",
    # Admin commands
    "cogs.admintools.shutdown",
    "cogs.admintools.showdm",
    "cogs.admintools.ownertools",
    # Profile commands
    "cogs.profile.userstats",
    "cogs.profile.avatar",
    # Other commands
    "cogs.admin",
    # "cogs.config",
    # Utilities
    "utilities.events",
    # Note: "utilities.modmail" is loaded later in the code due to Order of Operations
]


def setup_logger():
    # Create the logger
    # Try and get the logs path from the config file. If not set then use the packages internal default
    try:
        LOGSPATH = botconfig.get("Misc", "LOGSPATH")
        loggerconfig = LoggerConfig(
            __dsn__=None,
            __app_name__=f"{__botname__}_{userid}",
            __version__=__version__,
            logspath=LOGSPATH,
            raven=False,
            rfh_max_size=44485760,
            rfh_debug_max_size=10485760,
            rfh_backup_count=5,
            rfh_debug_backup_count=5,
        )
    except Exception:
        loggerconfig = LoggerConfig(
            __dsn__=None,
            __app_name__=f"{__botname__}_{userid}",
            __version__=__version__,
            raven=False,
        )

    # Adjust the log formatting to include milliseconds and the module name
    loggerconfigdict = loggerconfig.get_config()
    format = loggerconfigdict["formatters"]["default"]["format"].replace(
        "[%(asctime)s]", "[%(asctime)s,%(msecs)03d] | %(name)s"
    )
    loggerconfigdict["formatters"]["default"]["format"] = format
    formatters = loggerconfigdict.get("formatters").get("default")
    logging.config.dictConfig(loggerconfigdict)

    # root Logger - File and Console
    rootlogger = logging.getLogger("root")
    setup_color_logging(rootlogger, formatters["format"], formatters["datefmt"], reconfigure=True)
    # Discord Logger - Console
    discordlogger = logging.getLogger("discord")
    level = logging.INFO
    setup_color_logging(discordlogger, formatters["format"], formatters["datefmt"], reconfigure=True, level=level)
    # sqlalchemy Logger - Console
    sqlalchemylogger = logging.getLogger("sqlalchemy")
    level = logging.WARNING
    setup_color_logging(sqlalchemylogger, formatters["format"], formatters["datefmt"], reconfigure=True, level=level)
    # rediscluster Logger - Console
    redisclusterlogger = logging.getLogger("rediscluster")
    level = logging.INFO
    setup_color_logging(redisclusterlogger, formatters["format"], formatters["datefmt"], reconfigure=True, level=level)

    return rootlogger


def setup_color_logging(logger, fmt, datefmt, reconfigure=True, level=logging.INFO):
    coloredlogs.install(
        logger=logger,
        level=level,
        reconfigure=reconfigure,
        fmt=fmt,
        datefmt=datefmt,
        milliseconds=True,
        isatty=True,
        level_styles={
            "critical": {"bold": True, "color": "red"},  # 50
            "error": {"color": "red"},  # 40
            "success": {"bold": True, "color": "green"},  # 35
            "warning": {"color": "yellow"},  # 30
            "notice": {"color": "magenta"},  # 25
            "info": {"color": "white"},  # 20
            "verbose": {"color": "blue"},  # 15
            "debug": {"color": "green"},  # 10
            "spam": {"color": "green", "faint": True},  # 5
        },
    )


def main():
    try:
        __environment__ = botconfig.get("BotConfig", "ENVIRONMENT", fallback="Development")
        testing_guild_id = botconfig.getint("BotConfig", "TESTING_GUILD_ID", fallback=None)
        logger = setup_logger()

        bot = MyBot(logger=logger, initial_extensions=extensions, testing_guild_id=testing_guild_id)
        try:
            bot.run(token=__token__, reconnect=True, log_handler=None)
        except discord.errors.PrivilegedIntentsRequired as err:
            logger.critical(f"Privileged Intents not set correctly. {sys.exc_info()[0].__name__}: {err}")
            time.sleep(60 * 30)
        except Exception as err:
            logger.critical(f"Critical Generic Global Bot Error. {sys.exc_info()[0].__name__}: {err}")
            time.sleep(60 * 30)
    except discord.errors.LoginFailure as err:
        print(f"Exception logging in. {err}")
        time.sleep(300)
    except KeyboardInterrupt:
        print("Caught Keyboard Interrupt")
        sys.exit()


main()
