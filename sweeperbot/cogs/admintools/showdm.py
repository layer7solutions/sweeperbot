import sys
from distutils.util import strtobool
from typing import Literal

import discord
from discord.ext import commands


class ShowDM(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.hybrid_command(aliases=["showdm", "getdm"], with_app_command=True)
    @discord.app_commands.describe(
        user_id="The User ID to get the DM conversation for",
        limit="Default: 10 | Max # of messages in the DM to show",
        ephemeral="Default: No | If message should only be visible to you",
    )
    @commands.is_owner()
    @commands.guild_only()
    async def get_dm(
        self,
        ctx,
        user_id: str,
        limit: int = 10,
        ephemeral: Literal["Yes", "No"] = "No",
    ):
        """Gets the DM history between the Bot and the User. Owner Only.

        Usage:
        getdm userid
        showdm 123456789123456789
        """

        # Check if command is allowed to run here
        is_blocked = await self.bot.helpers.check_if_command_blocked(ctx)
        if is_blocked:
            self.bot.log.debug(
                f"CMD {ctx.command}: Blocked from running in gid:{ctx.message.guild.id}.cid:{ctx.message.channel.id}"
            )
            return await ctx.send("Sorry, this command is blocked.", ephemeral=True)
        # Set ephemeral to boolean value
        ephemeral = bool(strtobool(ephemeral)) if ephemeral else False
        # Defer to give us more time
        if ctx.interaction:
            await ctx.interaction.response.defer()

        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")
            user = ctx.bot.get_user(user_id)
            if not user:
                self.bot.log.debug(f"ShowDM: User {user_id} not found via get_user, trying fetch")
                user = await ctx.bot.fetch_user(user_id)
                if not user:
                    self.bot.log.debug(f"ShowDM: User {user_id} not found via fetch, exiting")
                    return await ctx.send(f"User {user_id} not found, exiting", ephemeral=True)

            channel = user.dm_channel
            if not channel:
                self.bot.log.debug(f"ShowDM: dm_channel not found for {user_id}, creating one")
                channel = await user.create_dm()
                if not channel:
                    self.bot.log.debug(f"ShowDM: Unable to create dm channel for {user_id}, exiting")
                    return ctx.send(f"Unable to create dm channel for {user_id}, exiting", ephemeral=True)

            # Oldest first, so that oldest gets sent to the channel first, and you read the older conversation to newer
            #   at the bottom
            messages = [message async for message in channel.history(limit=limit)]
            if len(messages) == 0:
                return await ctx.send(f"No message history with {user_id}.", ephemeral=True)

            message_list = []
            # Prepare all the text into the list
            for msg in messages:
                text = f"```css\n[{msg.created_at.replace(tzinfo=None, microsecond=0)}]: {msg.content}```"[:2000]
                # Add each new item to the front of the list. The history results are newest to oldest, but we want to
                #   output oldest to newest, so we need to reverse the list when adding to it
                message_list.insert(0, text)

            # Finally let's send it - First one for header info, which lets the replies look a little cleaner
            #   and lets the actual message contain more original text and less repeated header info
            await ctx.send(f"Showing DM History for: {user} ({user.id})", ephemeral=ephemeral)
            # Then the rest of them
            for text in message_list:
                await ctx.send(text, ephemeral=ephemeral)

        except discord.HTTPException as err:
            self.bot.log.error(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")


async def setup(bot):
    await bot.add_cog(ShowDM(bot))
