import csv
import sys

import discord
from PIL import UnidentifiedImageError
from db import models
from discord.ext import commands
from imagehash import phash
from sqlalchemy.exc import DBAPIError


class OwnerTools(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(slash_command=False, hidden=True)
    @commands.is_owner()
    async def find_imgur_notes(self, ctx):
        """
        Exports all notes that have imgur in them
        """
        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")
            db_type = "Not Set"

            # Try block for each DB type so we can do grouped sessions
            try:
                session = self.bot.helpers.get_db_session()
                try:
                    # Grab all records where the '_encrypted' column is NULL
                    db_type = "Note"
                    data = (
                        session.query(models.Note)
                        # .join(models.User, models.Note.user_id == models.User.id)
                        .join(models.Alias, models.Note.user_id == models.Alias.user_id)
                        .filter(models.Note.server_id == 1)
                        .all()
                    )  # DestinyTheGame

                    csv_header = ["user_id", "note_id", "text"]
                    csv_data = []
                    for idx, row in enumerate(data, start=1):
                        if "imgur" in row.text_encrypted.lower():
                            userid = row.user.discord_id
                            noteid = row.id
                            text = row.text_encrypted
                            csv_data.append([userid, noteid, text])

                    # Write to csv file
                    with open("dtg_note_data.csv", "wt") as f:
                        csv_writer = csv.writer(f, quoting=csv.QUOTE_ALL)
                        csv_writer.writerow(csv_header)
                        csv_writer.writerows(csv_data)

                    await ctx.send(f"All done", file=discord.File("dtg_note_data.csv"))
                except DBAPIError as err:
                    self.bot.log.exception(
                        f"Error processing find_imgur_notes for '{db_type}'. {sys.exc_info()[0].__name__}: {err}"
                    )
                    session.rollback()
                except Exception as err:
                    self.bot.log.exception(
                        f"Unknown Error during find_imgur_notes for '{db_type}' via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
                    )
                finally:
                    session.close()
                    db_type = "Not Set"
            except Exception as err:
                pass

        except discord.HTTPException as err:
            self.bot.log.error(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")

    @commands.command(slash_command=False, hidden=True)
    @commands.is_owner()
    async def migratedb(self, ctx):
        """
        Performs a migration of data to encrypted form.
        """
        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")
            db_type = "Not Set"

            # Try block for each DB type so we can do grouped sessions
            try:
                session = self.bot.helpers.get_db_session()
                try:
                    # Grab all records where the '_encrypted' column is NULL
                    db_type = "Alias"
                    data = session.query(models.Alias).filter(models.Alias.name_encrypted == None).all()
                    print(len(data))

                    for idx, row in enumerate(data, start=1):
                        row.name_encrypted = row.name
                        session.add(row)

                        if idx % 100 == 0:
                            # Commit every so often
                            session.commit()
                            self.bot.log.info(f"{db_type} DB Migration Status: {idx} / {len(data)}")
                            await ctx.send(f"{db_type} DB Migration Status: {idx} / {len(data)}")
                    # Commit all when done
                    session.commit()
                    await ctx.send(f"{db_type} DB Migration Status: Complete: {len(data)}")
                except DBAPIError as err:
                    self.bot.log.exception(
                        f"Error processing database migration for '{db_type}'. {sys.exc_info()[0].__name__}: {err}"
                    )
                    session.rollback()
                except Exception as err:
                    self.bot.log.exception(
                        f"Unknown Error during database migration for '{db_type}' via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
                    )
                finally:
                    session.close()
                    db_type = "Not Set"
            except Exception as err:
                pass

            # Try block for each DB type so we can do grouped sessions
            try:
                session = self.bot.helpers.get_db_session()
                try:
                    # Grab all records where the '_encrypted' column is NULL
                    db_type = "Note"
                    data = session.query(models.Note).filter(models.Note.text_encrypted == None).all()

                    for idx, row in enumerate(data, start=1):
                        row.text_encrypted = row.text
                        session.add(row)

                        if idx % 100 == 0:
                            # Commit every so often
                            session.commit()
                            self.bot.log.info(f"{db_type} DB Migration Status: {idx} / {len(data)}")
                            await ctx.send(f"{db_type} DB Migration Status: {idx} / {len(data)}")
                    # Commit all when done
                    session.commit()
                    await ctx.send(f"{db_type} DB Migration Status: Complete: {len(data)}")
                except DBAPIError as err:
                    self.bot.log.exception(
                        f"Error processing database migration for '{db_type}'. {sys.exc_info()[0].__name__}: {err}"
                    )
                    session.rollback()
                except Exception as err:
                    self.bot.log.exception(
                        f"Unknown Error during database migration for '{db_type}' via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
                    )
                finally:
                    session.close()
                    db_type = "Not Set"
            except Exception as err:
                pass

            # Try block for each DB type so we can do grouped sessions
            try:
                session = self.bot.helpers.get_db_session()
                try:
                    # Grab all records where the '_encrypted' column is NULL
                    db_type = "Warn"
                    data = session.query(models.Warn).filter(models.Warn.text_encrypted == None).all()

                    for idx, row in enumerate(data, start=1):
                        row.text_encrypted = row.text
                        session.add(row)

                        if idx % 100 == 0:
                            # Commit every so often
                            session.commit()
                            self.bot.log.info(f"{db_type} DB Migration Status: {idx} / {len(data)}")
                            await ctx.send(f"{db_type} DB Migration Status: {idx} / {len(data)}")
                    # Commit all when done
                    session.commit()
                    await ctx.send(f"{db_type} DB Migration Status: Complete: {len(data)}")
                except DBAPIError as err:
                    self.bot.log.exception(
                        f"Error processing database migration for '{db_type}'. {sys.exc_info()[0].__name__}: {err}"
                    )
                    session.rollback()
                except Exception as err:
                    self.bot.log.exception(
                        f"Unknown Error during database migration for '{db_type}' via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
                    )
                finally:
                    session.close()
                    db_type = "Not Set"
            except Exception as err:
                pass

            # Try block for each DB type so we can do grouped sessions
            try:
                session = self.bot.helpers.get_db_session()
                try:
                    # Grab all records where the '_encrypted' column is NULL
                    db_type = "Mute"
                    data = session.query(models.Mute).filter(models.Mute.text_encrypted == None).all()

                    for idx, row in enumerate(data, start=1):
                        row.text_encrypted = row.text
                        session.add(row)

                        if idx % 100 == 0:
                            # Commit every so often
                            session.commit()
                            self.bot.log.info(f"{db_type} DB Migration Status: {idx} / {len(data)}")
                            await ctx.send(f"{db_type} DB Migration Status: {idx} / {len(data)}")
                    # Commit all when done
                    session.commit()
                    await ctx.send(f"{db_type} DB Migration Status: Complete: {len(data)}")
                except DBAPIError as err:
                    self.bot.log.exception(
                        f"Error processing database migration for '{db_type}'. {sys.exc_info()[0].__name__}: {err}"
                    )
                    session.rollback()
                except Exception as err:
                    self.bot.log.exception(
                        f"Unknown Error during database migration for '{db_type}' via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
                    )
                finally:
                    session.close()
                    db_type = "Not Set"
            except Exception as err:
                pass

            # Try block for each DB type so we can do grouped sessions
            try:
                session = self.bot.helpers.get_db_session()
                try:
                    # Grab all records where the '_encrypted' column is NULL
                    db_type = "Kick"
                    data = session.query(models.Kick).filter(models.Kick.text_encrypted == None).all()

                    for idx, row in enumerate(data, start=1):
                        row.text_encrypted = row.text
                        session.add(row)

                        if idx % 100 == 0:
                            # Commit every so often
                            session.commit()
                            self.bot.log.info(f"{db_type} DB Migration Status: {idx} / {len(data)}")
                            await ctx.send(f"{db_type} DB Migration Status: {idx} / {len(data)}")
                    # Commit all when done
                    session.commit()
                    await ctx.send(f"{db_type} DB Migration Status: Complete: {len(data)}")
                except DBAPIError as err:
                    self.bot.log.exception(
                        f"Error processing database migration for '{db_type}'. {sys.exc_info()[0].__name__}: {err}"
                    )
                    session.rollback()
                except Exception as err:
                    self.bot.log.exception(
                        f"Unknown Error during database migration for '{db_type}' via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
                    )
                finally:
                    session.close()
                    db_type = "Not Set"
            except Exception as err:
                pass

            # Try block for each DB type so we can do grouped sessions
            try:
                session = self.bot.helpers.get_db_session()
                try:
                    # Grab all records where the '_encrypted' column is NULL
                    db_type = "Ban"
                    data = session.query(models.Ban).filter(models.Ban.text_encrypted == None).all()

                    for idx, row in enumerate(data, start=1):
                        row.text_encrypted = row.text
                        session.add(row)

                        if idx % 100 == 0:
                            # Commit every so often
                            session.commit()
                            self.bot.log.info(f"{db_type} DB Migration Status: {idx} / {len(data)}")
                            await ctx.send(f"{db_type} DB Migration Status: {idx} / {len(data)}")
                    # Commit all when done
                    session.commit()
                    await ctx.send(f"{db_type} DB Migration Status: Complete: {len(data)}")
                except DBAPIError as err:
                    self.bot.log.exception(
                        f"Error processing database migration for '{db_type}'. {sys.exc_info()[0].__name__}: {err}"
                    )
                    session.rollback()
                except Exception as err:
                    self.bot.log.exception(
                        f"Unknown Error during database migration for '{db_type}' via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
                    )
                finally:
                    session.close()
                    db_type = "Not Set"
            except Exception as err:
                pass

            # Try block for each DB type so we can do grouped sessions
            try:
                session = self.bot.helpers.get_db_session()
                try:
                    # Grab all records where the '_encrypted' column is NULL
                    db_type = "Tags"
                    data = session.query(models.Tags).filter(models.Tags.content_encrypted == None).all()

                    for idx, row in enumerate(data, start=1):
                        row.content_encrypted = row.content
                        session.add(row)

                        if idx % 100 == 0:
                            # Commit every so often
                            session.commit()
                            self.bot.log.info(f"{db_type} DB Migration Status: {idx} / {len(data)}")
                            await ctx.send(f"{db_type} DB Migration Status: {idx} / {len(data)}")
                    # Commit all when done
                    session.commit()
                    await ctx.send(f"{db_type} DB Migration Status: Complete: {len(data)}")
                except DBAPIError as err:
                    self.bot.log.exception(
                        f"Error processing database migration for '{db_type}'. {sys.exc_info()[0].__name__}: {err}"
                    )
                    session.rollback()
                except Exception as err:
                    self.bot.log.exception(
                        f"Unknown Error during database migration for '{db_type}' via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
                    )
                finally:
                    session.close()
                    db_type = "Not Set"
            except Exception as err:
                pass

            # Try block for each DB type so we can do grouped sessions
            try:
                session = self.bot.helpers.get_db_session()
                try:
                    # Grab all records where the '_encrypted' column is NULL
                    db_type = "Reminder"
                    data = session.query(models.Reminder).filter(models.Reminder.text_encrypted == None).all()

                    for idx, row in enumerate(data, start=1):
                        row.text_encrypted = row.text
                        session.add(row)

                        if idx % 100 == 0:
                            # Commit every so often
                            session.commit()
                            self.bot.log.info(f"{db_type} DB Migration Status: {idx} / {len(data)}")
                            await ctx.send(f"{db_type} DB Migration Status: {idx} / {len(data)}")
                    # Commit all when done
                    session.commit()
                    await ctx.send(f"{db_type} DB Migration Status: Complete: {len(data)}")
                except DBAPIError as err:
                    self.bot.log.exception(
                        f"Error processing database migration for '{db_type}'. {sys.exc_info()[0].__name__}: {err}"
                    )
                    session.rollback()
                except Exception as err:
                    self.bot.log.exception(
                        f"Unknown Error during database migration for '{db_type}' via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
                    )
                finally:
                    session.close()
                    db_type = "Not Set"
            except Exception as err:
                pass

        except discord.HTTPException as err:
            self.bot.log.error(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")

    @commands.command(slash_command=False)
    @commands.is_owner()
    async def guild_join_msg(self, ctx, *, welcome_text: str):
        """
        Sets the welcome message to be sent to the guild owner when the bot joins a guild. This should only be used by Bot Owners.

        This is _not_ for when a user joins a guild.

        Example:

        guild_join_msg Text that is sent to the Bot Owner. All formatting allowed.

        Requires Permission: Bot Owner

        Parameters
        -----------
        ctx: context
            The context message involved.
        welcome_text: str
            Text sent to the Guild Owner when the bot joins the Guild. Use {{bot_owners_string}} to auto replace with a list of the bot owners.
        """
        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")

            self.bot.helpers.redis.set(f"discord.bid:{self.bot.user.id}.welcome_message", welcome_text)
            await ctx.send(f"**__Welcome Message Set:__**\n\n{welcome_text}")

        except discord.HTTPException as err:
            self.bot.log.error(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")

    @commands.command(slash_command=False)
    @commands.is_owner()
    async def antispam_add_bad_hash(self, ctx, img_cat):
        """
        Adds the image hash to the list of bad hashes for either profile pictures or regular images posted

        Example:
        antispam_add_bad_hash img_cat
        antispam_add_bad_hash img

        Requires Permission: Bot Owner

        Parameters
        -----------
        ctx: context
            The context message involved.
        img_cat: str
            pfp_list = ["pfp", "avi", "avatar"]
            img_list = ["img", "images", "image"]
        """
        pfp_list = ["pfp", "avi", "avatar"]
        img_list = ["img", "images", "image"]
        if ctx.message.attachments:
            if img_cat.lower() in pfp_list:
                redis_key = "discord.naughty_data.global.bad_pfp_hashes"
            elif img_cat.lower() in img_list:
                redis_key = "discord.naughty_data.global.bad_hashes"
            else:
                return await ctx.send(
                    f"The category to add the hash to was not found. Options:\n"
                    f"**PFP:** {pfp_list}\n"
                    f"**IMG:** {img_list}"
                )
            for file in ctx.message.attachments:
                try:
                    # Hash the file
                    image_data = await file.read()
                    filehash = phash(image_data)
                    # Add hash to the set
                    self.bot.helpers.redis.sadd(redis_key, f"{filehash}")
                    await ctx.message.reply(
                        f"Added that file to the global blacklist of bad hashes for {img_cat}.\n"
                        f"**Hash:** {filehash}\n"
                        f"**File:** {file.url}"
                    )
                except UnidentifiedImageError:
                    pass
                except Exception as err:
                    self.bot.log.exception(
                        f"Error downloading and processing the file to hash. {sys.exc_info()[0].__name__}: {err}"
                    )
        else:
            return await ctx.send(f"No attachment found. Please be sure to upload an image you want hashed.")

    @commands.command(slash_command=False, hidden=True)
    @commands.is_owner()
    async def clear_global_slash(self, ctx):
        """
        Removes all global slash commands for the specific bot.

        WARNING: The commands would need to be re-uploaded via a restart of the bot.
        """
        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")

            ctx.bot.tree.clear_commands()
            await ctx.bot.tree.sync()
            await ctx.send(
                f"All global commands cleared."
                f"\n\nRemember to restart the bot and they can take ~1 hour to show again."
            )

        except discord.HTTPException as err:
            self.bot.log.error(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")

    @commands.command(slash_command=False, hidden=True)
    @commands.is_owner()
    async def clear_guild_slash(self, ctx, guild_id):
        """
        Removes all slash commands for the specific bot and guild.

        WARNING: The commands would need to be re-uploaded such as enabling testing guild flag and restarting the bot.
        """
        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")

            guild = discord.Object(id=guild_id)
            ctx.bot.tree.clear_commands(guild=guild)
            await ctx.bot.tree.sync(guild=guild)
            await ctx.send(
                f"All guild-level commands cleared for Guild: ({guild.id})"
                f"\n\nRemember to restart the bot for them to show again."
            )

        except discord.HTTPException as err:
            self.bot.log.error(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")

    @commands.is_owner()
    @commands.command(hidden=True, slash_command=False)
    async def api_version(self, ctx):
        try:
            # Get the HTTP API Version
            http_ver = discord.http.Route.BASE.split("/")[-1]
            # Get the Websocket Version
            wss_ver = await ctx.bot.http.get_bot_gateway()
            wss_ver = wss_ver[1].split("&")[1].replace("=", "")
            msg = await ctx.send(f"HTTP Version: {http_ver}" f"\nWebsocket Version: {wss_ver}")
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )

    @commands.is_owner()
    @commands.command(hidden=True, slash_command=False)
    async def populate_ban_cache(self, ctx, userids: str = None):
        try:
            await ctx.send(
                f"Starting ban cache population\n"
                f"Note: This could take a while if there are a lot of bans in the server."
            )
            count = 0
            # If a userid is provided, process just that
            if userids:
                all_bans = [userid for userid in userids.split(",")]
            # Otherwise get from the API
            else:
                all_bans = [entry.user.id async for entry in ctx.message.guild.bans(limit=None)]
            await ctx.send(f"total bans: {len(all_bans)}")
            for uid in all_bans:
                try:
                    result = self.bot.helpers.add_to_redis_ban_cache(uid, ctx.message.guild.id)
                    count += result
                except Exception as err:
                    self.bot.log.exception(f"{sys.exc_info()[0].__name__}: {err}")
            await ctx.send(f"Added {count} to redis cache")
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )

    @commands.is_owner()
    @commands.command(hidden=True)
    async def get_ban_cache(self, ctx):
        try:
            await ctx.send(f"getting ban cache list")
            try:
                result = self.bot.helpers.redis.smembers(f"discord.gid:{ctx.message.guild.id}.ban_cache")
                await ctx.send(f"total in redis ban cache: {len(result)}")
            except Exception as err:
                self.bot.log.exception(f"{sys.exc_info()[0].__name__}: {err}")

        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )

    @commands.is_owner()
    @commands.command(hidden=True)
    async def autovc_ban_add(self, ctx, member_id: int):
        try:
            try:
                result = self.bot.helpers.AddAutoVCBan(member_id)

                await ctx.send(f"Updated AutoVC Global Ban List, Adding User {member_id} with result: {result}")
            except Exception as err:
                self.bot.log.exception(f"{sys.exc_info()[0].__name__}: {err}")

        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )

    @commands.is_owner()
    @commands.command(hidden=True)
    async def autovc_ban_rem(self, ctx, member_id: int):
        try:
            try:
                result = self.bot.helpers.RemoveAutoVCBan(member_id)

                await ctx.send(f"Updated AutoVC Global Ban List, Removing User {member_id} with result: {result}")
            except Exception as err:
                self.bot.log.exception(f"{sys.exc_info()[0].__name__}: {err}")

        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )

    @commands.is_owner()
    @commands.command(hidden=True)
    async def autovc_ban_check(self, ctx, member_id: int):
        try:
            try:
                result = self.bot.helpers.CheckIfAutoVCBanned(member_id)

                await ctx.send(f"Checking AutoVC Global Ban List, User {member_id} with result: {result}")
            except Exception as err:
                self.bot.log.exception(f"{sys.exc_info()[0].__name__}: {err}")

        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )


async def setup(bot):
    await bot.add_cog(OwnerTools(bot))
