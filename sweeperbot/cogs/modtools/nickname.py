import sys
import typing

import discord
from discord.ext import commands


class Nickname(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(aliases=["nick"], slash_command=False)
    @commands.has_permissions(manage_nicknames=True)
    @commands.bot_has_permissions(manage_nicknames=True)
    @commands.guild_only()
    async def nickname(self, ctx, user_id: str, *, nickname: typing.Optional[str] = None):
        """Changes users nickname to the one supplied.

        Example:

        nick userID this is the new nick

        Requires Permission: Manage Nicknames

        Parameters
        -----------
        ctx: context
            The context message involved.
        user_id: str
            The user/member the change is related to. Can be an ID or a mention
        nickname: typing.Optional[str]
            The new nickname. Can be blank to remove it. Limit 32 characters.
        """

        # Check if command is allowed to run here
        is_blocked = await self.bot.helpers.check_if_command_blocked(ctx)
        if is_blocked:
            self.bot.log.debug(
                f"CMD {ctx.command}: Blocked from running in gid:{ctx.message.guild.id}.cid:{ctx.message.channel.id}"
            )
            return

        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")
            user = await self.bot.helpers.get_member_or_user(user_id, ctx.message.guild)
            if not user:
                return await ctx.send(
                    f"Unable to find the requested user. Please make sure the user ID or @ mention is valid."
                )
            # Truncate nickname to 32 characters per Discord limits
            nickname = nickname[:32] if nickname else None
            # Validate it's a Member object
            if isinstance(user, discord.member.Member):
                try:
                    await user.edit(
                        nick=nickname,
                        reason=f"Nickname change request by {ctx.message.author} ({ctx.message.author.id})",
                    )
                    await ctx.send(
                        f"Successfully changed nickname to: `{nickname if nickname else 'Nickname Cleared - None Set'}` for: {user} ({user.id})."
                    )
                except discord.Forbidden:
                    await ctx.send(
                        f"There was an error changing the nickname to: `{nickname}` for: {user} ({user.id}).\n\n"
                        f"Make sure the bot is above the user in the role hierarchy."
                    )
                except Exception as err:
                    self.bot.log.exception(f"Error changing users nickname. {sys.exc_info()[0].__name__}: {err}")
                    await ctx.send(
                        f"There was an error changing the nickname to: `{nickname}` for: {user} ({user.id})."
                    )
            else:
                await ctx.send(f"Unable to change nickname for: {user} ({user.id}).")

        except discord.HTTPException as err:
            self.bot.log.exception(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")


async def setup(bot):
    await bot.add_cog(Nickname(bot))
