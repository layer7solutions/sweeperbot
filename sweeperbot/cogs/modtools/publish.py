import sys

import discord
from discord.ext import commands
from utilities.helpers import set_sentry_scope


class Publish(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.hybrid_command(aliases=["publish"], with_app_command=True)
    @discord.app_commands.describe(
        channel="The channel the message you want to publish is in",
        message_id="The Message ID to publish",
    )
    @commands.bot_has_permissions(manage_messages=True)
    @commands.guild_only()
    async def publish_message(
        self,
        ctx,
        channel: discord.TextChannel,
        message_id: str,
    ):
        """Publishes a message in the channel specified."""

        # Check if command is allowed to run here
        is_blocked = await self.bot.helpers.check_if_command_blocked(ctx)
        if is_blocked:
            self.bot.log.debug(
                f"CMD {ctx.command}: Blocked from running in gid:{ctx.message.guild.id}.cid:{ctx.message.channel.id}"
            )
            return
        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")
            # Check if the user has permission to publish messages:
            member_perms = channel.permissions_for(ctx.message.author)

            # Get the message to publish to
            publish_message = None
            # Error text to use if getting the message to publish fails
            publish_message_not_found_text = f"Unable to find that message. Please make sure you supplied a correct message ID in the correct channel."
            try:
                # Try and publish the message
                publish_message = await channel.fetch_message(int(message_id))
            except (discord.NotFound, ValueError):
                await ctx.send(publish_message_not_found_text, ephemeral=True)

            if not publish_message:
                await ctx.send(publish_message_not_found_text, ephemeral=True)

            # Check if the channel is an announcement one:
            if not publish_message.channel.is_news():
                await ctx.send(
                    f"The channel the message belongs to is not a News channel, unable to publish.", ephemeral=True
                )

            # In order to publish messages, you either need to be the author, or have manage message perms
            if (
                publish_message.author == ctx.message.author and member_perms.send_messages
            ) or member_perms.manage_messages:
                try:
                    await publish_message.publish()
                    await ctx.send(f"That message has been published in {channel.mention}!", ephemeral=True)
                except discord.Forbidden:
                    await ctx.send(
                        f"The bot is missing the Manage Message permission needed to publish that message, or the message isn't eligible to be puslished.",
                        ephemeral=True,
                    )
                except discord.HTTPException as err:
                    await ctx.send(
                        f"Failed to publish that message. {sys.exc_info()[0].__name__}: {err}", ephemeral=True
                    )
            else:
                return await ctx.send(
                    f"Sorry, you don't have permissions to publish messages in {channel.mention}. You need Send Messages to publish your own, or Manage Messages to publish others.",
                    ephemeral=True,
                )

        except discord.HTTPException as err:
            set_sentry_scope(ctx)
            self.bot.log.exception(
                f"Discord HTTP Error responding to {ctx.command}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(
                f"Error processing {ctx.command}. Error has already been reported to my developers.", ephemeral=True
            )
        except Exception as err:
            set_sentry_scope(ctx)
            self.bot.log.exception(f"Error responding to {ctx.command}. {sys.exc_info()[0].__name__}: {err}")
            await ctx.send(
                f"Error processing {ctx.command}. Error has already been reported to my developers.", ephemeral=True
            )


async def setup(bot):
    await bot.add_cog(Publish(bot))
