import sys
from distutils.util import strtobool
from typing import Literal

import discord
from discord.ext import commands


class AllRoles(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.hybrid_command(with_app_command=True)
    @discord.app_commands.describe(
        ephemeral="Default: No | If message should only be visible to you",
    )
    @commands.has_permissions(manage_roles=True)
    @commands.guild_only()
    @commands.cooldown(1, 300, commands.BucketType.guild)
    async def allroles(
        self,
        ctx,
        ephemeral: Literal["Yes", "No"] = "No",
    ):
        """List all roles on the server. Max once per 5 minutes."""
        # Check if command is allowed to run here
        is_blocked = await self.bot.helpers.check_if_command_blocked(ctx)
        if is_blocked:
            self.bot.log.debug(
                f"CMD {ctx.command}: Blocked from running in gid:{ctx.message.guild.id}.cid:{ctx.message.channel.id}"
            )
            return await ctx.send("Sorry, this command is blocked.", ephemeral=True)
        # Defer since we get user counts and can take a while
        if ctx.interaction:
            await ctx.interaction.response.defer()
        # Set ephemeral to boolean value
        ephemeral = bool(strtobool(ephemeral)) if ephemeral else False
        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")
            # Check if user is blacklisted, if so, ignore.
            if await self.bot.helpers.check_if_blacklisted(ctx.message.author.id, ctx.message.guild.id):
                self.bot.log.debug(
                    f"User {ctx.message.author} ({ctx.message.author.id}) Blacklisted, unable to use command {ctx.command}"
                )
                return await ctx.send("Sorry, you're not allowed to use this command", ephemeral=True)

            # Let user know we're doing stuff and could take a while.
            await ctx.send(
                f"Processing request... This could take a bit depending on size of server and number of roles.",
                ephemeral=ephemeral,
            )

            # Chunk the guild to get total member count
            if ctx.guild.chunked is False:
                await ctx.guild.chunk()

            # little logic to split into embeds with 2000 characters max
            output = []
            roles = sorted(
                ctx.message.guild.roles,
                key=lambda role: len(role.members),
                reverse=True,
            )
            for role in roles:
                output.append(f"**{len(role.members):,}** | {role.mention} ({role.id}) \n")

            out = ""
            for line in output:
                out += line
                if len(out) > 1900:
                    embed = discord.Embed(title=f"Roles for {ctx.message.guild.name}", description=out)
                    await ctx.send(embed=embed, ephemeral=ephemeral)
                    out = ""

            if len(out) != 0:
                embed = discord.Embed(title=f"Roles for {ctx.message.guild.name}", description=out)
                await ctx.send(embed=embed, ephemeral=ephemeral)

        except discord.errors.Forbidden as err:
            self.bot.log.warning(
                f"Bot missing perms. {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.message.author.send(
                f"Bot is missing perms. Please make sure it has send messaages (to actually send the message), manage messages (to delete calling message), manage roles (to view the info), and embed links (to send the message) perms."
            )

        except discord.HTTPException as err:
            self.bot.log.exception(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(
                f"Error processing {ctx.command}. Error has already been reported to my developers.", ephemeral=True
            )
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(
                f"Error processing {ctx.command}. Error has already been reported to my developers.", ephemeral=True
            )


async def setup(bot):
    await bot.add_cog(AllRoles(bot))
