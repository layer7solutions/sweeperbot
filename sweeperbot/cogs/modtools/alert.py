import sys
import typing

import discord
from discord.ext import commands
from utilities.helpers import set_sentry_scope


class Alert(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.hybrid_command(with_app_command=True)
    @discord.app_commands.describe(
        role="The role to alert",
        channel="The channel to alert to (defaults to current)",
    )
    @commands.has_permissions(manage_roles=True)
    @commands.guild_only()
    async def alert(
        self,
        ctx,
        role: discord.Role,
        channel: typing.Optional[discord.TextChannel],
    ):
        """Send a msg pinging the role specified to the current or provided channel."""
        # Check if command is allowed to run here
        is_blocked = await self.bot.helpers.check_if_command_blocked(ctx)
        if is_blocked:
            self.bot.log.debug(
                f"CMD {ctx.command}: Blocked from running in gid:{ctx.message.guild.id}.cid:{ctx.message.channel.id}"
            )
            return
        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")
            # Check if user is blacklisted, if so, ignore.
            if await self.bot.helpers.check_if_blacklisted(ctx.message.author.id, ctx.message.guild.id):
                self.bot.log.debug(
                    f"User {ctx.message.author} ({ctx.message.author.id}) Blacklisted, unable to use command {ctx.command}"
                )
                return

            # If user tries to mention @everyone then deny it.
            if role.name.lower() == "@everyone":
                return await ctx.send(f"Sorry but I'm not permitted to mention everyone.")
            # Check if the role is above the bot
            if role > ctx.guild.me.top_role:
                return await ctx.send(f"Sorry, the role you want to alert is above the bot, and cannot be modified.")
            if channel is None:
                channel = ctx.message.channel

            # Finds the role, case-insensitive removing leading and trailing whitespace.
            try:
                # If role is already mentionable then it mentions it and leaves
                # enable. If it wasn't mentionable in the first place it will enable
                # it, mentions it, then disables mentioning.
                disable_mentions = True
                if role.mentionable:
                    disable_mentions = False
                else:
                    await role.edit(mentionable=True)

                try:
                    message_content = f"Hello {role.mention}"
                    # If no channel specified uses channel command was called from.
                    await channel.send(message_content)

                    if ctx.interaction:
                        await ctx.send("Command successful.", ephemeral=True)
                except Exception as err:
                    self.bot.log.exception(
                        f"Error mentioning role named '{role} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
                    )
                    await ctx.send(
                        f"Error processing {ctx.command}. Error has already been reported to my developers.",
                        ephemeral=True,
                    )
                # Regardless of the error, we want to disable mentions if we should
                finally:
                    if disable_mentions:
                        await role.edit(mentionable=False)

            except Exception as err:
                set_sentry_scope(ctx)
                self.bot.log.exception(f"Error finding role named '{role}'. {sys.exc_info()[0].__name__}: {err}")

        except discord.HTTPException as err:
            set_sentry_scope(ctx)
            self.bot.log.exception(
                f"Discord HTTP Error responding to {ctx.command}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(
                f"Error processing {ctx.command}. Error has already been reported to my developers.", ephemeral=True
            )
        except Exception as err:
            set_sentry_scope(ctx)
            self.bot.log.exception(f"Error responding to {ctx.command}. {sys.exc_info()[0].__name__}: {err}")
            await ctx.send(
                f"Error processing {ctx.command}. Error has already been reported to my developers.", ephemeral=True
            )


async def setup(bot):
    await bot.add_cog(Alert(bot))
