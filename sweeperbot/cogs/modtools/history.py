import sys

import discord
from cogs.utils.paginator import FieldPages
from discord.ext import commands
from sqlalchemy.exc import DBAPIError


class History(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.hybrid_command(aliases=["h"], with_app_command=True)
    @discord.app_commands.describe(user_id="The Users ID to get the history for")
    @commands.has_permissions(manage_messages=True)
    @commands.guild_only()
    async def history(
        self,
        ctx,
        user_id: str,
    ):
        """Gets a users history record."""
        if ctx.interaction:
            await ctx.interaction.response.defer(thinking=False)
        # Check if command is allowed to run here
        is_blocked = await self.bot.helpers.check_if_command_blocked(ctx)
        if is_blocked:
            self.bot.log.debug(
                f"CMD {ctx.command}: Blocked from running in gid:{ctx.message.guild.id}.cid:{ctx.message.channel.id}"
            )
            return await ctx.send("Sorry, this command is blocked.", ephemeral=True)

        session = self.bot.helpers.get_db_session()
        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")
            guild = ctx.message.guild
            user = await self.bot.helpers.get_member_or_user(user_id, guild)
            if not user:
                return await ctx.send(
                    f"Unable to find the requested user. Please make sure the user ID is supplied and valid."
                )

            (
                embed_result_entries,
                footer_text,
            ) = await self.bot.helpers.get_action_history(session, user, guild)

            p = FieldPages(
                ctx,
                per_page=8,
                entries=embed_result_entries,
                extra_message_info=f"{user} ({user.id})",
            )
            p.embed.color = 0xFF8C00
            p.embed.set_author(name=f"Member: {user} ({user.id})", icon_url=user.display_avatar.url)
            p.embed.set_footer(text=footer_text)
            await p.paginate()
        except discord.HTTPException as err:
            self.bot.log.exception(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except DBAPIError as err:
            self.bot.log.exception(f"Error logging note to database. {sys.exc_info()[0].__name__}: {err}")
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
            session.rollback()
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        finally:
            if ctx.interaction:
                await ctx.interaction.followup.send("_ _", ephemeral=True)
            session.close()


async def setup(bot):
    await bot.add_cog(History(bot))
