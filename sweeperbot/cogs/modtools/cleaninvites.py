import sys
import typing
from distutils.util import strtobool
from typing import Literal

import discord
from discord.ext import commands


class CleanInvites(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.hybrid_command(aliases=["cleaninvite"], with_app_command=True)
    @discord.app_commands.describe(
        less_than_eq_uses="Default: 0 | Invites must have less than or equal to this many uses or they are not remove",
        older_than_hours="Default: 12 | Invites must be older than this many hours or they are not removed",
        only_temporary="Default: No | Whether to remove invites that only grant temporary access to the guild",
        total_invites_above="Default: 500 | The guild must have more than this many existing invites or the command won't run",
    )
    @commands.guild_only()
    @commands.has_permissions(manage_guild=True)
    @commands.bot_has_permissions(manage_guild=True)
    @commands.cooldown(1, 600, commands.BucketType.guild)
    async def cleaninvites(
        self,
        ctx,
        less_than_eq_uses: typing.Optional[int] = 0,
        older_than_hours: typing.Optional[int] = 12,
        only_temporary: Literal["Yes", "No"] = "No",
        total_invites_above: typing.Optional[int] = 500,
    ):
        """Bulk removes invites from the guild"""

        # Check if command is allowed to run here
        is_blocked = await self.bot.helpers.check_if_command_blocked(ctx)
        if is_blocked:
            self.bot.log.debug(
                f"CMD {ctx.command}: Blocked from running in gid:{ctx.message.guild.id}.cid:{ctx.message.channel.id}"
            )
            return await ctx.send("Sorry, this command is blocked.", ephemeral=True)

        # Since the command can take a while, deferring
        if ctx.interaction:
            await ctx.interaction.response.defer(thinking=False)
        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")
            only_temporary = bool(strtobool(only_temporary))
            await self.bot.helpers.clean_guild_invites(
                ctx.guild,
                less_than_eq_uses,
                older_than_hours,
                only_temporary,
                total_invites_above,
                ctx,
            )

        except discord.HTTPException as err:
            self.bot.log.exception(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        finally:
            if ctx.interaction:
                await ctx.interaction.followup.send("_ _", ephemeral=True)


async def setup(bot):
    await bot.add_cog(CleanInvites(bot))
