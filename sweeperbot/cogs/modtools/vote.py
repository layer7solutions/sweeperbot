import sys
import typing

import discord
from discord import app_commands
from discord.ext import commands


class Vote(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.hybrid_command(with_app_command=True)
    # @app_commands.command(description="Create a vote!")
    @app_commands.describe(message_content="Optional message to send")
    # @app_commands.guild_only()
    @commands.bot_has_permissions(external_emojis=True, add_reactions=True)
    @commands.guild_only()
    async def vote(
        self,
        ctx,
        *,
        message_content: typing.Optional[str],
    ):
        """Adds voting reactions on the message made: upvote, downvote, and blank."""

        # Check if command is allowed to run here
        is_blocked = await self.bot.helpers.check_if_command_blocked(ctx)
        if is_blocked:
            self.bot.log.debug(
                f"CMD {ctx.command}: Blocked from running in gid:{ctx.message.guild.id}.cid:{ctx.message.channel.id}"
            )
            return await ctx.send("Sorry, this command is blocked.", ephemeral=True)
        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")
            # Check if user is blacklisted, if so, ignore.
            if await self.bot.helpers.check_if_blacklisted(ctx.message.author.id, ctx.message.guild.id):
                self.bot.log.debug(
                    f"User {ctx.message.author} ({ctx.message.author.id}) Blacklisted, unable to use command {ctx.command}"
                )
                return await ctx.send("Sorry, you're not allowed to use this command", ephemeral=True)

            # Get the reactions
            upvote = self.bot.get_emoji(self.bot.constants.reactions["upvote"])
            downvote = self.bot.get_emoji(self.bot.constants.reactions["downvote"])
            spacer = self.bot.get_emoji(self.bot.constants.reactions["spacer"])

            # If this is an interaction, the bot needs to send the message
            if ctx.interaction:
                # Embed of None allows a message to be sent even if message_content is None
                message = await ctx.send(
                    message_content if message_content else f"A vote has been requested by {ctx.message.author}"
                )
            else:
                message = ctx.message

            # Add the reactions
            for emoji in (upvote, downvote, spacer):
                if emoji:
                    await message.add_reaction(emoji)

        except discord.HTTPException as err:
            self.bot.log.exception(
                f"Discord HTTP Error responding to {ctx.invoked_with} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")


async def setup(bot):
    await bot.add_cog(Vote(bot))
