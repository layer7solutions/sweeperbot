import sys
from typing import Literal

import discord
from discord.ext import commands


class Role(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.guild_only()
    @commands.hybrid_command(with_app_command=True)
    @discord.app_commands.describe(
        action="Whether to add or remove the role",
        member="The member/ID to adjust the role for",
        role="The role/ID to modify for the specified member",
    )
    @commands.has_permissions(manage_roles=True)
    @commands.bot_has_permissions(manage_roles=True)
    async def role(
        self,
        ctx,
        action: Literal["add", "remove"],
        member: discord.Member,
        role: discord.Role,
    ):
        """Adds or removes a role for a user"""

        # Check if command is allowed to run here
        is_blocked = await self.bot.helpers.check_if_command_blocked(ctx)
        if is_blocked:
            self.bot.log.debug(
                f"CMD {ctx.command}: Blocked from running in gid:{ctx.message.guild.id}.cid:{ctx.message.channel.id}"
            )
            return await ctx.send(f"Sorry, that command is blocked.")

        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")

            # bool to make logic make more sense
            can_adjust_members_role = False
            if ctx.author.id == ctx.guild.owner_id:
                # Guild owner can adjust any roles
                can_adjust_members_role = True
            elif ctx.author.top_role > role:
                # Authors top role is above the role to add, same logic as Discord and thus allowed
                can_adjust_members_role = True
            if ctx.guild.me.top_role <= role:
                # If the bots top role is the same level or below the role to add, then deny
                can_adjust_members_role = False

            if not can_adjust_members_role:
                return await ctx.send(
                    f"Sorry, cannot adjust that role for that member. Make sure the bots top role "
                    f"is above the role you want to add/remove, and that *you who runs this "
                    f"command* has a top role that is above the role you want to add/remove."
                    f"\n\nIf you cannot add/remove this role in the Discord UI, then this bot will "
                    f"not let you do it either."
                )

            # Perform the action
            successful = None
            try:
                if action == "add":
                    if role in member.roles:
                        return await ctx.send(f"The member already has that role, skipping as I can't add it again")
                    await member.add_roles(
                        role, reason=f"Role {action} by ({ctx.message.author.id}) {ctx.message.author}"
                    )
                    successful = True
                elif action == "remove":
                    if role in member.roles:
                        await member.remove_roles(
                            role, reason=f"Role {action} by ({ctx.message.author.id}) {ctx.message.author}"
                        )
                    else:
                        return await ctx.send(
                            f"The member doesn't have that role, skipping removing as I can't remove something that doesn't exist"
                        )
                    successful = True
            except Exception as err:
                successful = False
                await ctx.send(
                    f"There was an error trying to modify that members roles. Please run the following command to validate the members current roles:"
                    f"\n```"
                    f"\n/userstats {member.id}"
                    f"\n```"
                )
            if successful:
                await ctx.send(
                    f"Successfully {'added' if action == 'add' else 'removed'} the role '**{role.name}**' for {member} ({member.id})."
                )

        except discord.Forbidden:
            await ctx.send("Unable to modify the members roles. The bot ran into a permissions error.")
        except discord.HTTPException as err:
            self.bot.log.exception(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")


async def setup(bot):
    await bot.add_cog(Role(bot))
