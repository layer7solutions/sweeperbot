import sys
from datetime import datetime, timezone
from typing import Optional

import discord
from discord.ext import commands


class Server(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.hybrid_command(aliases=["server"], with_app_command=True)
    @discord.app_commands.describe(guild_invite="Default: current guild | Invite Link/Code to get info about")
    @commands.has_permissions(send_messages=True)
    @commands.guild_only()
    async def serverinfo(self, ctx, guild_invite: Optional[str]):
        """Displays info about a guild based on Invite Link/Code"""
        # Check if command is allowed to run here
        is_blocked = await self.bot.helpers.check_if_command_blocked(ctx)
        if is_blocked:
            self.bot.log.debug(
                f"CMD {ctx.command}: Blocked from running in gid:{ctx.message.guild.id}.cid:{ctx.message.channel.id}"
            )
            return await ctx.send("Sorry, this command is blocked.", ephemeral=True)

        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")
            # Check if user is blacklisted, if so, ignore.
            if await self.bot.helpers.check_if_blacklisted(ctx.message.author.id, ctx.message.guild.id):
                self.bot.log.debug(
                    f"User {ctx.message.author} ({ctx.message.author.id}) Blacklisted, unable to use command {ctx.command}"
                )
                return await ctx.send("Sorry, you're not allowed to use this command", ephemeral=True)

            # Try and get the invite
            try:
                if guild_invite:
                    invite = await ctx.bot.fetch_invite(guild_invite, with_counts=True)
                else:
                    invite = await self.bot.helpers.get_guild_invite(ctx.message.guild)
                    if not invite:
                        return await ctx.send(f"No invites found. Please specify an invite code to use.")

            except discord.errors.NotFound:
                return await ctx.send(
                    f"Sorry, I was unable to find any guild from that invite. Please make sure the invite code is valid."
                )

            guild = invite.guild
            member_count = invite.approximate_member_count
            presence_count = invite.approximate_presence_count

            # Create the embed
            embed = discord.Embed(description=f">>> {guild.description}", timestamp=datetime.now(timezone.utc))
            embed.set_author(
                name=f"Info for: {guild.name} ({guild.id})",
                icon_url=guild.icon.url if guild.icon else None,
            )
            # Set the bottom image as the invite splash url (shows above channel list)
            if guild.splash:
                embed.set_image(url=guild.splash.url)
            # This is an approx number of members currently in the guild
            embed.add_field(
                name="Member Count",
                value=f"{member_count:,}",
                inline=True,
            )
            # This is an approx number of presence members, or people currently connected to the guild
            embed.add_field(
                name="Presence Count",
                value=f"{presence_count:,}",
                inline=True,
            )
            # Guild creation date
            embed.add_field(
                name="Guild Creation",
                value=f"{guild.created_at.replace(microsecond=0, tzinfo=None)} UTC",
                inline=True,
            )
            # Verification Level
            embed.add_field(
                name="Verification Level",
                value=f"{guild.verification_level}",
                inline=False,
            )
            # Guild features
            if guild.features:
                features_list = guild.features
                features_list.sort()
                features = ""
                for item in features_list:
                    feature = str(item).replace("_", " ").title()
                    features += f"• {feature}\n"
                embed.add_field(name="Features", value=features, inline=False)

            # Set the thumbnail at top right to the banner that shows below the channel list
            if guild.banner:
                embed.set_thumbnail(url=guild.banner.url)
            # Set footer text
            text = f"Invite URL: {invite.url}"
            if invite.inviter:
                text += f" | Inviter: {invite.inviter} ({invite.inviter.id})"
            embed.set_footer(text=text)
            await ctx.send(embed=embed)

        except discord.HTTPException as err:
            self.bot.log.exception(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")


async def setup(bot):
    await bot.add_cog(Server(bot))
