import re
import sys
from distutils.util import strtobool
from typing import Literal, Optional

import discord
from discord.ext import commands


class Purge(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.redis = bot.helpers.redis
        self.most_freq_used_key_name = f"discord.messages.channels.most_freq_used"
        # Max number of messages we'll let them delete
        self.max_msgs_allowed_to_delete = 1000

    @commands.hybrid_command(aliases=["clear"], with_app_command=True)
    @discord.app_commands.describe(
        number_of_messages="Number of messages to delete",
        channel="Channel to delete the messages in",
        user_id="Optional user to only delete their messages",
        ephemeral="Default: No | If message should only be visible to you",
    )
    @commands.has_permissions(manage_messages=True)
    @commands.bot_has_permissions(manage_messages=True, read_message_history=True, add_reactions=True)
    @commands.guild_only()
    async def purge(
        self,
        ctx,
        number_of_messages: int,
        channel: discord.TextChannel,
        user_id: Optional[str],
        ephemeral: Literal["Yes", "No"] = "No",
    ):
        """Purges up to the specified number of messages in current channel, optionally for only specified user"""

        # Check if command is allowed to run here
        is_blocked = await self.bot.helpers.check_if_command_blocked(ctx)
        if is_blocked:
            self.bot.log.debug(
                f"CMD {ctx.command}: Blocked from running in gid:{ctx.message.guild.id}.cid:{ctx.message.channel.id}"
            )
            return

        try:
            self.bot.log.info(
                f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id}) via {ctx.message.id}"
            )
            # Set ephemeral to boolean value
            ephemeral = bool(strtobool(ephemeral)) if ephemeral else False

            if ctx.interaction:
                await ctx.interaction.response.defer(thinking=False, ephemeral=ephemeral)

            # Set variable shortcuts
            guild = ctx.message.guild

            # Check if they surpass max messages
            if number_of_messages > self.max_msgs_allowed_to_delete:
                number_of_messages = self.max_msgs_allowed_to_delete
                await ctx.send(
                    f"Sorry, you can only delete up to {self.max_msgs_allowed_to_delete} messages. We've lowered this for you.",
                    ephemeral=True,
                )

            # Confirm the deletion (temp disabled for Interactions since ephemeral has issues)
            # TODO - Figure out how to add reactions or confirm an ephemeral message
            if ctx.interaction is None:
                confirm = await self.bot.prompt.send(
                    ctx, f"Are you sure you want to delete {number_of_messages} messages?"
                )
                if confirm:
                    await ctx.send(f"Continuing with the message purge.", ephemeral=True)
                    self.bot.log.info(f"Continuing with purge via {ctx.message.id}")
                elif confirm is False:
                    return await ctx.send("Cancelling request.", ephemeral=True)
                elif confirm is None:
                    return await ctx.send("Request timed out.", ephemeral=True)

            # ~~~~~~~~~~~~~~~~~~~~~~~~~~
            # Purge without any filters
            # ~~~~~~~~~~~~~~~~~~~~~~~~~~
            # If no user, just purge the last {number_of_messages}
            if not user_id:
                # Increase count by 2 to account for the calling command & confirmation, only if non interaction
                if ctx.interaction is None:
                    number_of_messages += 2
                return await self.start_purge_process(ctx, number_of_messages, guild.id, channel, "*", ephemeral)

            # ~~~~~~~~~~~~~~~~~~~~~~~
            # Purge with user filter
            # ~~~~~~~~~~~~~~~~~~~~~~~
            # If there's a User ID, get the user
            if user_id:
                member = await self.bot.helpers.get_member_or_user(user_id, ctx.message.guild)
                # If there's a member, actually do the work
                if member:
                    return await self.start_purge_process(
                        ctx, number_of_messages, guild.id, channel, member.id, ephemeral
                    )
                else:
                    return await ctx.send(
                        f"Unable to find the requested user. Please make sure the user ID is valid.", ephemeral=True
                    )

        except discord.HTTPException as err:
            self.bot.log.error(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")

    async def start_purge_process(
        self,
        ctx: discord.ext.commands.Context,
        number_of_messages: int,
        guild_id: int,
        channel: discord.TextChannel,
        member_id: str = "*",
        ephemeral=False,
    ):
        """
        Parameters:
            ctx: discord.ext.commands.Context - Message context object
            number_of_messages: int - How many messages to purge
            guild_id: int - The Guild ID the purge is happening in
            channel: discord.TextChannel - The channel the purge is happening in
            member_id: str = "*" - The Member ID to filter messages for; * is any message sent (user independent)
            ephemeral: whether only the author of interaction should see the messages
        Returns:
            None
        """

        # Create the redis_guild_channel_key, used for selecting messages under guild_id and channel.id
        redis_guild_channel_key = f"discord.messages.gid:{guild_id}.cid:{channel.id}"

        # 1. Get all of the userid:message combos from the Channel key that matches the User ID (or all users)
        # that are selected for removal
        msg_value_list = await self.redis_get_all_messages_by_users(
            number_of_messages, member_id, redis_guild_channel_key
        )

        # 2. Get the Message ID's from the value string
        all_messages_as_ids = await self.get_all_message_ids_from_value(msg_value_list)

        # 3. Convert the sorted list of ID's into objects
        all_messages_to_remove_as_obj = await self.convert_msg_ids_to_objects(all_messages_as_ids)

        # 4. Break the list of messages to remove into batches (via API)
        all_messages_to_remove_as_chunks = await self.chunk_the_list(all_messages_to_remove_as_obj, 100)

        # 5. Also break the msg_value_list into chunks to remove (via Redis)
        all_msg_values_as_chunks = await self.chunk_the_list(msg_value_list, 100)

        # 6, 7, 8. Send to the API to delete, clean the redis cache for the batch
        batches_success, batches_fail = await self.send_to_api_to_delete_the_batch(
            all_messages_to_remove_as_chunks,
            all_msg_values_as_chunks,
            channel,
            redis_guild_channel_key,
        )

        # 9. Let user know completion status
        total_chunks = len(all_messages_to_remove_as_chunks)
        total_messages = len(all_messages_to_remove_as_obj)
        await self.notify_complete(ctx, batches_success, batches_fail, total_chunks, total_messages, ephemeral)

    async def redis_get_all_messages_by_users(
        self,
        number_of_messages: int,
        member_id: str,
        redis_guild_channel_key: str,
    ):
        """
        Parameters:
            number_of_messages: int - How many messages are selected for removal
            member_id: str - Either the member ID or * for all members
            redis_guild_channel_key: str - A string key that contains the guild id and channel id the messages are under
        Returns:
            A list of values in the format: "uid:{member_id}.mid:{message_id}"
        """

        all_redis_values = []
        # 1A. If we want to get all members messages, then use zrange
        if member_id == "*":
            zrange_start = number_of_messages * -1
            # End is the last element, Start is the last element, minus number_of_messages
            # Result is ordered list of only the number_of_messages we want
            all_redis_values = self.redis.zrange(name=redis_guild_channel_key, start=zrange_start, end=-1)
        # 1B. Otherwise use zscan to get a filtered list
        else:
            all_redis_values_gen = self.redis.zscan_iter(
                name=redis_guild_channel_key,
                match=f"uid:{member_id}.mid:*",
                count=20,
            )

            # 1C. Convert the generator into a list
            unsorted_list = []
            for item in all_redis_values_gen:
                unsorted_list.append(item)

            def sortkey(item):
                # First item in the tuple is the element/member
                # Second is the Score, which we have as timestamp
                return item[1]

            # 1D. Now sort the list, newest message first, oldest at end
            unsorted_list.sort(key=sortkey, reverse=True)
            # 1E. Trim the list to only the most recent {number_of_messages}
            sorted_trimmed_list = unsorted_list[:number_of_messages]
            # 1F. Now let's return just the values we want (cuts out the score aka timestamp)
            all_redis_values = [item[0] for item in sorted_trimmed_list]

        # All done, return results
        return all_redis_values

    async def get_all_message_ids_from_value(self, msg_value_list: list):
        """
        Parameters:
            msg_value_list: list - List of all the message_value's in the format of "uid:{member_id}.mid:{message_id}"
        Returns:
            A list of of message ID's extracted from the message_value's
        """
        # Set RegEx Search Pattern
        regex_mid_pattern = re.compile("(?<=mid:).*")

        # Loop to extract the message ID
        all_message_ids = []
        for msg_value in msg_value_list:
            result = re.search(pattern=regex_mid_pattern, string=msg_value)
            if result:
                message_id = int(result.group(0))
                all_message_ids.append(message_id)

        # All done, return results
        return all_message_ids

    async def convert_msg_ids_to_objects(self, all_messages_as_ids: list):
        """
        Parameters:
            all_messages_as_ids: list - A list of of message ID's extracted from the message_value's
        Returns:
            A list of messages as snowflake objects
        """
        all_messages_to_remove_as_obj = []
        for msg_id in all_messages_as_ids:
            temp_message_obj = discord.Object(msg_id)
            all_messages_to_remove_as_obj.append(temp_message_obj)

        # All done, return results
        return all_messages_to_remove_as_obj

    async def chunk_the_list(self, list_to_chunk: list, chunk_size: int):
        """
        Parameters:
            list_to_chunk: list - All the messages to be removed, in object form
            chunk_size: int - What size chunks to split the list into
        Returns:
            A list that holds a list of chunks where each list is max {chunk_size}
        """

        chunked_list = [list_to_chunk[x : x + chunk_size] for x in range(0, len(list_to_chunk), chunk_size)]

        # All done, return results
        return chunked_list

    async def send_to_api_to_delete_the_batch(
        self,
        all_messages_to_remove_as_chunks: list,
        all_msg_values_as_chunks: list,
        channel: discord.TextChannel,
        redis_guild_channel_key: str,
    ):
        """
        Parameters:
            all_messages_to_remove_as_chunks: list - A list of the chunks of messages to remove (as objects)
            all_msg_values_as_chunks: list - A list of message_values to remove via Redis
            channel: discord.Textchannel - The Discord text channel the messages are being deleted from
            redis_guild_channel_key: str - A string key that contains the guild id and channel id the messages are under
        Returns:
            batches_success - How many batches were a success
            batches_fail - How many batches failed
        """
        batches_success = 0
        batches_fail = 0
        # Indexes the chunks so we can get the index of all_msg_values_as_chunks's chunks
        for idx, batched_messages in enumerate(all_messages_to_remove_as_chunks):
            about_to_purge_list_of_ids = None
            try:
                # 7. Purge the messages via Discord API
                about_to_purge_list_of_ids = [m.id for m in batched_messages]
                self.bot.log.debug(f"Purge: About to purge: {about_to_purge_list_of_ids}")
                await channel.delete_messages(batched_messages)

                # 8. Remove from the Redis Sorted Set so we don't process again
                result = await self.zrem_from_redis(all_msg_values_as_chunks[idx], redis_guild_channel_key)

                # The fact we got this far means the Discord delete was a success. We're not going to fail the counting
                # just because Redis could have failed to delete so...
                # Increment success counter:
                batches_success += 1
            except Exception as err:
                self.bot.log.exception(
                    f"Error purging Batched Msgs: {about_to_purge_list_of_ids}. {sys.exc_info()[0].__name__}: {err}"
                )
                # Increment fail counter:
                batches_fail += 1

        # All done, return results
        return batches_success, batches_fail

    async def zrem_from_redis(self, message_value_list: list, redis_guild_channel_key: str):
        """
        Parameters:
            message_value_list: list - List of message_value's to remove from Redis
            redis_guild_channel_key: str - A string key that contains the guild id and channel id the messages are under
        Returns:
            Whether the ZREM result was a success or not
        """

        # 8A. Let's ZREM remove from Redis
        # let's join to one command for mass removal via Redis ZREM
        zrem_member_all = " ".join(message_value_list)
        self.bot.log.debug(f"Purge: About to ZREM for: {zrem_member_all}")
        result = self.redis.zrem(redis_guild_channel_key, *message_value_list)

        # 8B. Also decrease the "discord.messages.channels.most_freq_used"
        amount_to_decrease = len(message_value_list) * -1
        self.redis.zincrby(
            name=self.most_freq_used_key_name,
            amount=amount_to_decrease,
            value=redis_guild_channel_key,
        )

        # All done, return results
        return result

    async def notify_complete(
        self,
        ctx: discord.ext.commands.Context,
        batches_success: int,
        batches_fail: int,
        total_chunks: int,
        total_messages: int,
        ephemeral: bool,
    ):
        """
        Parameters:
            ctx: discord.ext.commands.Context - Context message came from
            batches_success: int - How many batches were a success
            batches_fail: int - How many batches were a failure
            total_chunks: int - How many total batches/chunks there were
            total_messages: int - How many total messages there were selected to remove
            ephemeral: bool - Whether only author should see the message
        Returns:
            None
        """

        try:
            self.bot.log.info(
                f"Completed purging. There were: | "
                f"**Successful Batches:** {batches_success} | "
                f"**Failed Batches:** {batches_fail} | "
                f"**Total Batches:** {total_chunks} | "
                f"**Total Messages To Purge:** {total_messages}."
            )

            emoji = self.bot.get_emoji(self.bot.constants.reactions["animated_sweeperbot"])
            msg = (
                f"{emoji} {'Failure' if batches_fail > 0 else 'Successful'} purging. There were:\n"
                f"**Successful Batches:** {batches_success}\n"
                f"**Failed Batches:** {batches_fail}\n"
                f"**Total Batches:** {total_chunks}\n"
                f"**Total Messages Purged:** {total_messages}."
            )
            if ctx.interaction:
                await ctx.interaction.followup.send(msg, ephemeral=ephemeral)
            else:
                await ctx.send(
                    msg,
                    ephemeral=ephemeral,
                )
        except Exception as err:
            self.bot.log.warning(f"Failure sending completion message. {sys.exc_info()[0].__name__}: {err}")


async def setup(bot):
    await bot.add_cog(Purge(bot))
