import sys
import typing

import discord
from cogs.utils.paginator import FieldPages
from db import models
from discord.ext import commands
from sqlalchemy import exc


class Config(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.has_permissions(manage_guild=True)
    @commands.guild_only()
    @commands.group(
        name="Config.Server",
        aliases=["svrconfig", "svrconfigure", "configure", "config"],
        case_insensitive=True,
        invoke_without_command=False,
        slash_command=False,
    )
    async def config(self, ctx):
        """Allows for setting various server configuration."""
        pass

    @commands.guild_only()
    @commands.has_permissions(manage_guild=True)
    @config.command(aliases=["mutedrole", "muteroles", "mutedroles"], slash_command=False)
    async def muterole(self, ctx, *, mute_role: str):
        """Sets the muted role used by the mute command which is assigned to a user when they are muted.

        Example:

        config muterole role name
        config muterole role_id

        Requires Permission: Manage Guild

        Parameters
        -----------
        ctx: context
            The context message involved.
        mute_role: str
            Role name or ID of the role the bot will give when a user is muted.
        """

        # Check if command is allowed to run here
        is_blocked = await self.bot.helpers.check_if_command_blocked(ctx)
        if is_blocked:
            self.bot.log.debug(
                f"CMD {ctx.command}: Blocked from running in gid:{ctx.message.guild.id}.cid:{ctx.message.channel.id}"
            )
            return

        session = self.bot.helpers.get_db_session()
        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")

            # Get the muted role
            muterole = discord.utils.find(
                lambda r: r.name.lower() == mute_role.lower().strip(),
                ctx.message.guild.roles,
            )

            if not muterole:
                return await ctx.message.author.send(f"Unable to find a role named `{mute_role}`")

            # Get the guild settings and update the muted role
            settings = await self.bot.helpers.get_one_guild_settings(session, ctx.message.guild.id)
            settings.muted_role = muterole.id
            session.commit()

            # Update local cache
            self.bot.guild_settings[ctx.message.guild.id] = await self.bot.helpers.get_one_guild_settings(
                session, ctx.message.guild.id
            )

            return await ctx.send(f"Successfully set the muted role to: {muterole.mention}.")

        except discord.HTTPException as err:
            self.bot.log.error(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except exc.DBAPIError as err:
            self.bot.log.exception(f"Database error with {ctx.command} command. {sys.exc_info()[0].__name__}: {err}")
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
            session.rollback()
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        finally:
            session.close()

    @commands.guild_only()
    @commands.has_permissions(manage_guild=True)
    @config.command(aliases=["modchannels"], slash_command=False)
    async def modchannel(self, ctx, *, channel: discord.TextChannel):
        """Sets the mod channel used by the bot for major alerts.

        Example:

        config modchannel #channel_name

        Requires Permission: Manage Guild

        Parameters
        -----------
        ctx: context
            The context message involved.
        channel: discord.TextChannel
            Channel designated as the mod channel for major alerts.
        """

        # Check if command is allowed to run here
        is_blocked = await self.bot.helpers.check_if_command_blocked(ctx)
        if is_blocked:
            self.bot.log.debug(
                f"CMD {ctx.command}: Blocked from running in gid:{ctx.message.guild.id}.cid:{ctx.message.channel.id}"
            )
            return

        session = self.bot.helpers.get_db_session()
        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")

            # Get the guild settings and update the channel id
            settings = await self.bot.helpers.get_one_guild_settings(session, ctx.message.guild.id)
            settings.mod_channel = channel.id
            session.commit()

            # Update local cache
            self.bot.guild_settings[ctx.message.guild.id] = await self.bot.helpers.get_one_guild_settings(
                session, ctx.message.guild.id
            )

            return await ctx.send(f"Successfully set the mod channel to: {channel.mention}.")

        except discord.HTTPException as err:
            self.bot.log.error(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except exc.DBAPIError as err:
            self.bot.log.exception(f"Database error with {ctx.command} command. {sys.exc_info()[0].__name__}: {err}")
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
            session.rollback()
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        finally:
            session.close()

    @commands.guild_only()
    @commands.has_permissions(manage_guild=True)
    @config.command(slash_command=False)
    async def adminrole(self, ctx, *, role_name: str):
        """Sets the Admin role which is used for permissions access on the website. This will also do a sync of the admins currently in that role.

        Example:

        config adminrole role name

        Requires Permission: Manage Guild

        Parameters
        -----------
        ctx: context
            The context message involved.
        role_name: str
            The role name that cooresponds to the Admin role
        """

        # Check if command is allowed to run here
        is_blocked = await self.bot.helpers.check_if_command_blocked(ctx)
        if is_blocked:
            self.bot.log.debug(
                f"CMD {ctx.command}: Blocked from running in gid:{ctx.message.guild.id}.cid:{ctx.message.channel.id}"
            )
            return
        session = self.bot.helpers.get_db_session()
        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")

            # If user tries to mention @everyone then deny it.
            if role_name.lower() == "@everyone":
                return await ctx.message.author.send(f"Sorry, but we can't allow you to set the role to everyone.")

            # Finds the role, case-insensitive removing leading and trailing whitespace.
            role = discord.utils.find(
                lambda r: r.name.lower() == role_name.lower().strip(),
                ctx.message.guild.roles,
            )

            if not role:
                return await ctx.message.author.send(f"Unable to find a role named `{role_name}`")

            # Now that we have the role, let them know we're about to start
            await ctx.send(f"About to set the admin role and start the initial sync")
            # Get the guild settings and update the id
            # Get current settings stored from the database
            guild_settings = await self.bot.helpers.get_one_guild_settings(session, ctx.message.guild.id)
            guild_settings.admin_role = role.id

            # Update local cache
            self.bot.guild_settings[ctx.message.guild.id] = await self.bot.helpers.get_one_guild_settings(
                session, ctx.message.guild.id
            )

            # Now that we have the role, let's update the list of admins
            # We're going to start by removing all admins for the server and setting new ones. This is due to the fact
            # that the server may be changing the admin role, and we don't want any old/bad data.
            # Get the DB profile for the guild
            db_guild = await self.bot.helpers.db_get_guild(session, ctx.message.guild.id)
            rels = session.query(models.ServerAdminRels).filter(models.ServerAdminRels.server_id == db_guild.id).all()
            # Now let's make sure it's for the server we want
            if rels:
                for relationship in rels:
                    if relationship.server_id == db_guild.id:
                        self.bot.log.debug(
                            f"About to delete admin rel: Guild: {relationship.server_id} | User: {relationship.user_id} | Msg ID: {ctx.message.id}"
                        )
                        session.delete(relationship)
                    else:
                        self.bot.log.debug(
                            f"Skipping admin rel delete: Guild: {relationship.server_id} | User: {relationship.user_id} | Msg ID: {ctx.message.id}"
                        )

            # After which let's add the new admins
            members = role.members
            for member in members:
                try:
                    await self.bot.helpers.db_process_admin_relationship(member, session, True)
                except exc.IntegrityError:
                    self.bot.log.warning(
                        f"Duplicate Server Admin Relationship record for member {member} ({member.id}), ignoring"
                    )
                    session.rollback()

            # Now that the sync/setup is done, let's commit all our changes
            session.commit()
            # and let the user know we're done
            await ctx.send(f"Config and sync of admin role is complete.")

        except discord.HTTPException as err:
            self.bot.log.exception(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except exc.DBAPIError as err:
            self.bot.log.exception(f"Database error with {ctx.command} command. {sys.exc_info()[0].__name__}: {err}")
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
            session.rollback()
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        finally:
            session.close()

    @commands.guild_only()
    @commands.has_permissions(manage_guild=True)
    @config.command(slash_command=False)
    async def modrole(self, ctx, *, role_name: str):
        """Sets the Mod role which is used for permissions access on the website. This will also do a sync of the mods currently in that role.

        Example:

        config modrole role name

        Requires Permission: Manage Guild

        Parameters
        -----------
        ctx: context
            The context message involved.
        role_name: str
            The role name that corresponds to the Mod role
        """

        # Check if command is allowed to run here
        is_blocked = await self.bot.helpers.check_if_command_blocked(ctx)
        if is_blocked:
            self.bot.log.debug(
                f"CMD {ctx.command}: Blocked from running in gid:{ctx.message.guild.id}.cid:{ctx.message.channel.id}"
            )
            return
        session = self.bot.helpers.get_db_session()
        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")

            # If user tries to mention @everyone then deny it.
            if role_name.lower() == "@everyone":
                return await ctx.message.author.send(f"Sorry, but we can't allow you to set the role to everyone.")

            # Finds the role, case-insensitive removing leading and trailing whitespace.
            role = discord.utils.find(
                lambda r: r.name.lower() == role_name.lower().strip(),
                ctx.message.guild.roles,
            )

            if not role:
                return await ctx.message.author.send(f"Unable to find a role named `{role_name}`")

            # Now that we have the role, let them know we're about to start
            await ctx.send(f"About to set the mod role and start the initial sync")
            # Get the guild settings and update the id
            # Get current settings stored from the database
            guild_settings = await self.bot.helpers.get_one_guild_settings(session, ctx.message.guild.id)
            guild_settings.mod_role = role.id

            # Update local cache
            self.bot.guild_settings[ctx.message.guild.id] = await self.bot.helpers.get_one_guild_settings(
                session, ctx.message.guild.id
            )

            # Now that we have the role, let's update the list of admins
            # We're going to start by removing all admins for the server and setting new ones. This is due to the fact
            # that the server may be changing the admin role, and we don't want any old/bad data.
            # Get the DB profile for the guild
            db_guild = await self.bot.helpers.db_get_guild(session, ctx.message.guild.id)
            rels = session.query(models.ServerModRels).filter(models.ServerModRels.server_id == db_guild.id).all()
            # Now let's make sure it's for the server we want
            if rels:
                for relationship in rels:
                    if relationship.server_id == db_guild.id:
                        self.bot.log.debug(
                            f"About to delete mod rel: Guild: {relationship.server_id} | User: {relationship.user_id} | Msg ID: {ctx.message.id}"
                        )
                        session.delete(relationship)
                    else:
                        self.bot.log.debug(
                            f"Skipping mod rel delete: Guild: {relationship.server_id} | User: {relationship.user_id} | Msg ID: {ctx.message.id}"
                        )

            # After which let's add the new admins
            members = role.members
            for member in members:
                try:
                    await self.bot.helpers.db_process_mod_relationship(member, session, True)
                except exc.IntegrityError:
                    self.bot.log.warning(
                        f"Duplicate Server Mod Relationship record for member {member} ({member.id}), ignoring"
                    )
                    session.rollback()

            # Now that the sync/setup is done, let's commit all our changes
            session.commit()
            # and let the user know we're done
            await ctx.send(f"Config and sync of mod role is complete.")

        except discord.HTTPException as err:
            self.bot.log.exception(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except exc.DBAPIError as err:
            self.bot.log.exception(f"Database error with {ctx.command} command. {sys.exc_info()[0].__name__}: {err}")
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
            session.rollback()
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        finally:
            session.close()

    @commands.guild_only()
    @commands.has_permissions(manage_guild=True)
    @config.command(aliases=["appeal"], slash_command=False)
    async def appeals(self, ctx, *, invite: str):
        """Sets the Discord Invite to send to users for an Appeals server, a shared server so they can reach the mod team.

        Example:

        config appeals https://discord.gg/InviteCode

        Requires Permission: Manage Guild

        Parameters
        -----------
        ctx: context
            The context message involved.
        invite: str
            The full Discord invite link
        """

        # Check if command is allowed to run here
        is_blocked = await self.bot.helpers.check_if_command_blocked(ctx)
        if is_blocked:
            self.bot.log.debug(
                f"CMD {ctx.command}: Blocked from running in gid:{ctx.message.guild.id}.cid:{ctx.message.channel.id}"
            )
            return

        session = self.bot.helpers.get_db_session()
        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")

            # Get the guild settings and update the channel id
            settings = await self.bot.helpers.get_one_guild_settings(session, ctx.message.guild.id)
            # TODO - Add checks if given just the code. Lookup the link or code
            #   and make sure it's valid
            settings.appeals_invite_code = invite
            session.commit()

            # Update local cache
            self.bot.guild_settings[ctx.message.guild.id] = await self.bot.helpers.get_one_guild_settings(
                session, ctx.message.guild.id
            )

            return await ctx.send(f"Successfully set the Appeals Server link to: {invite}.")

        except discord.HTTPException as err:
            self.bot.log.error(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except exc.DBAPIError as err:
            self.bot.log.exception(f"Database error with {ctx.command} command. {sys.exc_info()[0].__name__}: {err}")
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
            session.rollback()
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        finally:
            session.close()

    # Suggestion Configuration
    @commands.guild_only()
    @commands.has_permissions(manage_guild=True)
    @config.group(invoke_without_command=False, slash_command=False)
    async def suggestion(self, ctx):
        """Base for the Suggestion configuration. See `suggestion channel` and `suggestino commands`"""
        pass

    @commands.guild_only()
    @commands.has_permissions(manage_guild=True)
    @suggestion.command(aliases=["feed", "board", "channel"], slash_command=False)
    async def suggestion_feed_channel(self, ctx, *, channel: discord.TextChannel):
        """Sets the suggestion channel which is where suggestions get sent to.

        Example:

        config suggestion feed #channel
        config suggestion board #channel
        config suggestion channel #channel

        Requires Permission: Manage Guild

        Parameters
        -----------
        ctx: context
            The context message involved.
        channel: discord.TextChannel
            The channel mention.
        """

        # Check if command is allowed to run here
        is_blocked = await self.bot.helpers.check_if_command_blocked(ctx)
        if is_blocked:
            self.bot.log.debug(
                f"CMD {ctx.command}: Blocked from running in gid:{ctx.message.guild.id}.cid:{ctx.message.channel.id}"
            )
            return

        session = self.bot.helpers.get_db_session()
        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")

            # Get the guild settings and update the channel id
            settings = await self.bot.helpers.get_one_guild_settings(session, ctx.message.guild.id)
            settings.suggestion_channel = channel.id
            session.commit()

            # Update local cache
            self.bot.guild_settings[ctx.message.guild.id] = await self.bot.helpers.get_one_guild_settings(
                session, ctx.message.guild.id
            )

            return await ctx.send(f"Successfully set the channel where suggestions are sent to: {channel.mention}.")

        except discord.HTTPException as err:
            self.bot.log.error(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except exc.DBAPIError as err:
            self.bot.log.exception(f"Database error with {ctx.command} command. {sys.exc_info()[0].__name__}: {err}")
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
            session.rollback()
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        finally:
            session.close()

    @commands.guild_only()
    @commands.has_permissions(manage_guild=True)
    @suggestion.command(aliases=["commands"], slash_command=False)
    async def suggestion_channel_allowed(self, ctx, *, channel: discord.TextChannel):
        """Sets the channel that users are allowed to use the suggestion command in.

        Example:

        config suggestion commands #channel

        Requires Permission: Manage Guild

        Parameters
        -----------
        ctx: context
            The context message involved.
        channel: discord.TextChannel
            The channel mention.
        """

        # Check if command is allowed to run here
        is_blocked = await self.bot.helpers.check_if_command_blocked(ctx)
        if is_blocked:
            self.bot.log.debug(
                f"CMD {ctx.command}: Blocked from running in gid:{ctx.message.guild.id}.cid:{ctx.message.channel.id}"
            )
            return

        session = self.bot.helpers.get_db_session()
        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")

            # Get the guild settings
            settings = await self.bot.helpers.get_one_guild_settings(session, ctx.message.guild.id)
            settings.suggestion_channel_allowed = [channel.id]
            session.commit()

            # Update local cache
            self.bot.guild_settings[ctx.message.guild.id] = await self.bot.helpers.get_one_guild_settings(
                session, ctx.message.guild.id
            )

            return await ctx.send(
                f"Successfully set the channel users are allowed to use the suggestion command in to: {channel.mention}."
            )

        except discord.HTTPException as err:
            self.bot.log.error(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except exc.DBAPIError as err:
            self.bot.log.exception(f"Database error with {ctx.command} command. {sys.exc_info()[0].__name__}: {err}")
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
            session.rollback()
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        finally:
            session.close()

    # AntiSpam Configuration
    @commands.guild_only()
    @commands.has_permissions(manage_guild=True)
    @config.group(aliases=["spam"], invoke_without_command=False, slash_command=False)
    async def antispam(self, ctx):
        """Base for the Antispam configuration. See `antispam quickmsg true/false`"""
        pass

    @commands.guild_only()
    @commands.has_permissions(manage_guild=True)
    @antispam.command(aliases=["quickmessage"], slash_command=False)
    async def quickmsg(self, ctx, *, enabled: bool):
        """Sets whether the quick message antispam feature is enabled or not.

        Example:

        config antispam quickmsg true
        config antispam quickmsg false

        Requires Permission: Manage Guild

        Parameters
        -----------
        ctx: context
            The context message involved.
        enabled: bool
            Whether to enable the feature.
        """

        # Check if command is allowed to run here
        is_blocked = await self.bot.helpers.check_if_command_blocked(ctx)
        if is_blocked:
            self.bot.log.debug(
                f"CMD {ctx.command}: Blocked from running in gid:{ctx.message.guild.id}.cid:{ctx.message.channel.id}"
            )
            return

        session = self.bot.helpers.get_db_session()
        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")

            # Get the guild settings
            settings = await self.bot.helpers.get_one_guild_settings(session, ctx.message.guild.id)
            settings.antispam_quickmsg = enabled
            session.commit()

            # Update local cache
            self.bot.guild_settings[ctx.message.guild.id] = await self.bot.helpers.get_one_guild_settings(
                session, ctx.message.guild.id
            )

            return await ctx.send(f"Successfully set the AntiSpam Quick Message feature to: {enabled}.")

        except discord.HTTPException as err:
            self.bot.log.error(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except exc.DBAPIError as err:
            self.bot.log.exception(f"Database error with {ctx.command} command. {sys.exc_info()[0].__name__}: {err}")
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
            session.rollback()
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        finally:
            session.close()

    @commands.guild_only()
    @commands.has_permissions(manage_guild=True)
    @antispam.command(aliases=["rate"], slash_command=False)
    async def antispam_rate(self, ctx, *, rate: int):
        """Sets the antispam on_message rate (how many messages) can be sent (whole number only). Be sure to set the `antispam time` value for per *seconds* the rate is for.

        Example:

        config antispam rate 4

        Requires Permission: Manage Guild

        Parameters
        -----------
        ctx: context
            The context message involved.
        rate: int
            How many messages can be sent before being triggered. Whole numbers only.
        """

        # Check if command is allowed to run here
        is_blocked = await self.bot.helpers.check_if_command_blocked(ctx)
        if is_blocked:
            self.bot.log.debug(
                f"CMD {ctx.command}: Blocked from running in gid:{ctx.message.guild.id}.cid:{ctx.message.channel.id}"
            )
            return

        session = self.bot.helpers.get_db_session()
        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")

            # Get the guild settings
            settings = await self.bot.helpers.get_one_guild_settings(session, ctx.message.guild.id)
            settings.cd_on_message_rate = rate
            session.commit()

            # Update local cache
            self.bot.guild_settings[ctx.message.guild.id] = await self.bot.helpers.get_one_guild_settings(
                session, ctx.message.guild.id
            )
            # Reinitialize all the cooldowns and their buckets
            await self.bot.antispam.set_cooldown_buckets()

            return await ctx.send(f"Successfully set the AntiSpam On Message # of Messages rate to: {rate}.")

        except discord.HTTPException as err:
            self.bot.log.error(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except exc.DBAPIError as err:
            self.bot.log.exception(f"Database error with {ctx.command} command. {sys.exc_info()[0].__name__}: {err}")
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
            session.rollback()
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        finally:
            session.close()

    @commands.guild_only()
    @commands.has_permissions(manage_guild=True)
    @antispam.command(aliases=["per", "time", "timer"], slash_command=False)
    async def antispam_timer(self, ctx, *, rate: float):
        """Sets the antispam on_message timeout (in seconds). This is how many seconds the messages must be sent in before being triggered. Be sure to set the `antispam rate` value for how many messages would need to be sent before being triggered.

        Example:

        config antispam timer 4
        config antispam per 7.5
        config antispam time 30

        Requires Permission: Manage Guild

        Parameters
        -----------
        ctx: context
            The context message involved.
        rate: float
            How many seconds the rate of messages must be in before being triggered.
        """

        # Check if command is allowed to run here
        is_blocked = await self.bot.helpers.check_if_command_blocked(ctx)
        if is_blocked:
            self.bot.log.debug(
                f"CMD {ctx.command}: Blocked from running in gid:{ctx.message.guild.id}.cid:{ctx.message.channel.id}"
            )
            return

        session = self.bot.helpers.get_db_session()
        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")

            # Get the guild settings
            settings = await self.bot.helpers.get_one_guild_settings(session, ctx.message.guild.id)
            settings.cd_on_message_time = rate
            session.commit()

            # Update local cache
            self.bot.guild_settings[ctx.message.guild.id] = await self.bot.helpers.get_one_guild_settings(
                session, ctx.message.guild.id
            )
            # Reinitialize all the cooldowns and their buckets
            await self.bot.antispam.set_cooldown_buckets()

            return await ctx.send(f"Successfully set the AntiSpam timer to: {rate} seconds.")

        except discord.HTTPException as err:
            self.bot.log.error(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except exc.DBAPIError as err:
            self.bot.log.exception(f"Database error with {ctx.command} command. {sys.exc_info()[0].__name__}: {err}")
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
            session.rollback()
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        finally:
            session.close()

    @commands.guild_only()
    @commands.has_permissions(manage_guild=True)
    @antispam.command(aliases=["mute", "mutetime", "mutelength"], slash_command=False)
    async def antispam_mute_time(self, ctx, *, mute_length: str):
        """Sets the amount of time that someone is muted for when the antispam is triggered such as via quick message exceeding.

        Example:

        config antispam mute 20m
        config antispam mutetime 1h
        config antispam mutelength 1d

        Requires Permission: Manage Guild

        Parameters
        -----------
        ctx: context
            The context message involved.
        mute_length: str
            How long the user should be muted for when antispam is triggered.
        """

        # Check if command is allowed to run here
        is_blocked = await self.bot.helpers.check_if_command_blocked(ctx)
        if is_blocked:
            self.bot.log.debug(
                f"CMD {ctx.command}: Blocked from running in gid:{ctx.message.guild.id}.cid:{ctx.message.channel.id}"
            )
            return

        session = self.bot.helpers.get_db_session()
        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")

            # Get the guild settings
            settings = await self.bot.helpers.get_one_guild_settings(session, ctx.message.guild.id)
            settings.antispam_mute_time = mute_length
            session.commit()

            # Update local cache
            self.bot.guild_settings[ctx.message.guild.id] = await self.bot.helpers.get_one_guild_settings(
                session, ctx.message.guild.id
            )

            return await ctx.send(f"Successfully set the AntiSpam Mute Time to: {mute_length}.")

        except discord.HTTPException as err:
            self.bot.log.error(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except exc.DBAPIError as err:
            self.bot.log.exception(f"Database error with {ctx.command} command. {sys.exc_info()[0].__name__}: {err}")
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
            session.rollback()
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        finally:
            session.close()

    # On Join Welcome Message Configuration
    @commands.guild_only()
    @commands.has_permissions(manage_guild=True)
    @config.group(invoke_without_command=False, slash_command=False)
    async def welcome(self, ctx):
        """Base for the Welcome message configuration. See `welcome msg` and `welcome enable true/false`"""
        pass

    @commands.guild_only()
    @commands.has_permissions(manage_guild=True)
    @welcome.command(aliases=["msg", "message"], slash_command=False)
    async def welcome_msg(self, ctx, *, text: str):
        """Sets message sent to the user upon joining the server.

        Example:

        config welcome msg text goes here that is sent to the user
        config welcome message text goes here that is sent to the user

        Requires Permission: Manage Guild

        Parameters
        -----------
        ctx: context
            The context message involved.
        text: tr
            The text that is sent to the user.
        """

        # Check if command is allowed to run here
        is_blocked = await self.bot.helpers.check_if_command_blocked(ctx)
        if is_blocked:
            self.bot.log.debug(
                f"CMD {ctx.command}: Blocked from running in gid:{ctx.message.guild.id}.cid:{ctx.message.channel.id}"
            )
            return

        session = self.bot.helpers.get_db_session()
        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")

            # Get the guild settings and update the channel id
            settings = await self.bot.helpers.get_one_guild_settings(session, ctx.message.guild.id)
            settings.welcome_msg = text
            session.commit()

            # Update local cache
            self.bot.guild_settings[ctx.message.guild.id] = await self.bot.helpers.get_one_guild_settings(
                session, ctx.message.guild.id
            )

            return await ctx.send(f"Successfully set the On Join Welcome Message to:\n{text}")

        except discord.HTTPException as err:
            self.bot.log.error(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except exc.DBAPIError as err:
            self.bot.log.exception(f"Database error with {ctx.command} command. {sys.exc_info()[0].__name__}: {err}")
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
            session.rollback()
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        finally:
            session.close()

    @commands.guild_only()
    @commands.has_permissions(manage_guild=True)
    @welcome.command(aliases=["enable"], slash_command=False)
    async def welcome_msg_enable(self, ctx, *, enabled: bool):
        """Sets whether the On Join Welcome Message feature is enabled or not.

        Example:

        config welcome enable true
        config welcome enable false

        Requires Permission: Manage Guild

        Parameters
        -----------
        ctx: context
            The context message involved.
        enabled: bool
            Whether to enable the feature.
        """

        # Check if command is allowed to run here
        is_blocked = await self.bot.helpers.check_if_command_blocked(ctx)
        if is_blocked:
            self.bot.log.debug(
                f"CMD {ctx.command}: Blocked from running in gid:{ctx.message.guild.id}.cid:{ctx.message.channel.id}"
            )
            return

        session = self.bot.helpers.get_db_session()
        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")

            # Get the guild settings and update the channel id
            settings = await self.bot.helpers.get_one_guild_settings(session, ctx.message.guild.id)
            settings.welcome_msg_enabled = enabled
            session.commit()

            # Update local cache
            self.bot.guild_settings[ctx.message.guild.id] = await self.bot.helpers.get_one_guild_settings(
                session, ctx.message.guild.id
            )

            return await ctx.send(f"Successfully set the On Join Welcome Message feature to: {enabled}.")

        except discord.HTTPException as err:
            self.bot.log.error(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except exc.DBAPIError as err:
            self.bot.log.exception(f"Database error with {ctx.command} command. {sys.exc_info()[0].__name__}: {err}")
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
            session.rollback()
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        finally:
            session.close()

    # On Join Role Configuration
    @commands.guild_only()
    @commands.has_permissions(manage_guild=True)
    @config.group(invoke_without_command=False, slash_command=False)
    async def joinrole(self, ctx):
        """Base for the On Join role configuration. See `joinrole name` and `joinrole enable true/false`"""
        pass

    @commands.guild_only()
    @commands.has_permissions(manage_guild=True)
    @joinrole.command(aliases=["name", "rolename"], slash_command=False)
    async def joinrole_name(self, ctx, *, role_name: str):
        """Sets role to add to the user upon joining the server.

        Example:

        config joinrole name role name
        config joinrole rolename role name

        Requires Permission: Manage Guild

        Parameters
        -----------
        ctx: context
            The context message involved.
        role_name: str
            The name of the role to add to the user. This is case-insensitive.
        """

        # Check if command is allowed to run here
        is_blocked = await self.bot.helpers.check_if_command_blocked(ctx)
        if is_blocked:
            self.bot.log.debug(
                f"CMD {ctx.command}: Blocked from running in gid:{ctx.message.guild.id}.cid:{ctx.message.channel.id}"
            )
            return

        session = self.bot.helpers.get_db_session()
        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")

            role = discord.utils.find(
                lambda r: r.name.lower() == role_name.lower().strip(),
                ctx.message.guild.roles,
            )

            if not role:
                return await ctx.send(f"Unable to find a role named `{role_name}`")

            # Get the guild settings and update the channel id
            settings = await self.bot.helpers.get_one_guild_settings(session, ctx.message.guild.id)
            settings.on_join_role_id = role.id
            session.commit()

            # Update local cache
            self.bot.guild_settings[ctx.message.guild.id] = await self.bot.helpers.get_one_guild_settings(
                session, ctx.message.guild.id
            )

            return await ctx.send(f"Successfully set the On Join Role to: '{role.name}'")

        except discord.HTTPException as err:
            self.bot.log.error(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except exc.DBAPIError as err:
            self.bot.log.exception(f"Database error with {ctx.command} command. {sys.exc_info()[0].__name__}: {err}")
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
            session.rollback()
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        finally:
            session.close()

    @commands.guild_only()
    @commands.has_permissions(manage_guild=True)
    @joinrole.command(aliases=["enable"], slash_command=False)
    async def joinrole_enable(self, ctx, *, enabled: bool):
        """Sets whether the On Join Role feature is enabled or not.

        Example:

        config joinrole enable true
        config joinrole enable false

        Requires Permission: Manage Guild

        Parameters
        -----------
        ctx: context
            The context message involved.
        enabled: bool
            Whether to enable the feature.
        """

        # Check if command is allowed to run here
        is_blocked = await self.bot.helpers.check_if_command_blocked(ctx)
        if is_blocked:
            self.bot.log.debug(
                f"CMD {ctx.command}: Blocked from running in gid:{ctx.message.guild.id}.cid:{ctx.message.channel.id}"
            )
            return

        session = self.bot.helpers.get_db_session()
        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")

            # Get the guild settings and update the channel id
            settings = await self.bot.helpers.get_one_guild_settings(session, ctx.message.guild.id)
            settings.on_join_role_enabled = enabled
            session.commit()

            # Update local cache
            self.bot.guild_settings[ctx.message.guild.id] = await self.bot.helpers.get_one_guild_settings(
                session, ctx.message.guild.id
            )

            return await ctx.send(f"Successfully set the On Join Role feature to: {enabled}.")

        except discord.HTTPException as err:
            self.bot.log.error(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except exc.DBAPIError as err:
            self.bot.log.exception(f"Database error with {ctx.command} command. {sys.exc_info()[0].__name__}: {err}")
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
            session.rollback()
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        finally:
            session.close()

    # Activity Status Configuration
    # Bot Owner only due to all guilds it's in seeing it
    @commands.guild_only()
    @commands.is_owner()
    @config.group(aliases=["as"], invoke_without_command=False, slash_command=False)
    async def activitystatus(self, ctx):
        """Base for the activity status (alias=as) configuration. See `activitystatus add/remove`, `activitystatus list`, `activitystatus enable true/false`"""
        pass

    @commands.guild_only()
    @commands.is_owner()
    @activitystatus.command(aliases=["add", "a"], slash_command=False)
    async def activitystatus_add(self, ctx, *, status_text: str):
        """Adds an Activity Status that the bot displays/plays.

        Example:

        config activitystatus add status text
        config activitystatus a status text

        Requires Permission: Bot Owner

        Parameters
        -----------
        ctx: context
            The context message involved.
        status_text: str
            The text to set for the status.
        """

        # Check if command is allowed to run here
        is_blocked = await self.bot.helpers.check_if_command_blocked(ctx)
        if is_blocked:
            self.bot.log.debug(
                f"CMD {ctx.command}: Blocked from running in gid:{ctx.message.guild.id}.cid:{ctx.message.channel.id}"
            )
            return

        session = self.bot.helpers.get_db_session()
        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")
            # Get current settings from the database
            guild_settings = await self.bot.helpers.get_one_guild_settings(session, ctx.message.guild.id)

            # If the list is null/broken, start fresh
            if guild_settings.activity_status is None:
                guild_settings.activity_status = []

            current = []
            for status in guild_settings.activity_status:
                current.append(status)

            if status_text not in current:
                current.append(status_text)
                # Save to database
                guild_settings.activity_status = current
                session.commit()
                # Update local cache
                self.bot.guild_settings[ctx.message.guild.id] = await self.bot.helpers.get_one_guild_settings(
                    session, ctx.message.guild.id
                )

                # Send update to user
                return await ctx.send(f"Added '{status_text}' to status text rotation.")
            else:
                return await ctx.send(f"That status already exists.")

        except discord.HTTPException as err:
            self.bot.log.error(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except exc.DBAPIError as err:
            self.bot.log.exception(f"Database error with {ctx.command} command. {sys.exc_info()[0].__name__}: {err}")
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
            session.rollback()
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        finally:
            session.close()

    @commands.guild_only()
    @commands.is_owner()
    @activitystatus.command(aliases=["remove", "r", "delete", "d"], slash_command=False)
    async def activitystatus_remove(self, ctx, *, status_text: str):
        """Removes an Activity Status that the bot displays/plays.

        Example:

        config activitystatus remove status text
        config activitystatus delete status text

        Requires Permission: Bot Owner

        Parameters
        -----------
        ctx: context
            The context message involved.
        status_text: str
            The text for the status to remove.
        """

        # Check if command is allowed to run here
        is_blocked = await self.bot.helpers.check_if_command_blocked(ctx)
        if is_blocked:
            self.bot.log.debug(
                f"CMD {ctx.command}: Blocked from running in gid:{ctx.message.guild.id}.cid:{ctx.message.channel.id}"
            )
            return

        session = self.bot.helpers.get_db_session()
        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")

            # Get current settings stored from the database
            guild_settings = await self.bot.helpers.get_one_guild_settings(session, ctx.message.guild.id)

            # If there is no data, don't continue
            if guild_settings.activity_status is None:
                return await ctx.send(f"You do not have any status set.")

            current = []
            for status in guild_settings.activity_status:
                current.append(status)

            # Make sure status is in the list before modifications
            if status_text in current:
                current.remove(status_text)
                # Save to the database
                guild_settings.activity_status = current
                session.commit()
                # Update local cache
                self.bot.guild_settings[ctx.message.guild.id] = await self.bot.helpers.get_one_guild_settings(
                    session, ctx.message.guild.id
                )

                # Send update to user
                return await ctx.send(f"removed '{status_text}' from status text rotation.")
            else:
                await ctx.send(f"That status was not found in the current list of rotation.")

        except discord.HTTPException as err:
            self.bot.log.error(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except exc.DBAPIError as err:
            self.bot.log.exception(f"Database error with {ctx.command} command. {sys.exc_info()[0].__name__}: {err}")
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
            session.rollback()
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        finally:
            session.close()

    @commands.guild_only()
    @commands.is_owner()
    @activitystatus.command(aliases=["enable"], slash_command=False)
    async def activitystatus_enable(self, ctx, *, enabled: bool):
        """Sets whether the Activity Status feature is enabled or not.

        Example:

        config activitystatus enable true
        config activitystatus enable false

        Requires Permission: Bot Owner

        Parameters
        -----------
        ctx: context
            The context message involved.
        enabled: bool
            Whether to enable the feature.
        """

        # Check if command is allowed to run here
        is_blocked = await self.bot.helpers.check_if_command_blocked(ctx)
        if is_blocked:
            self.bot.log.debug(
                f"CMD {ctx.command}: Blocked from running in gid:{ctx.message.guild.id}.cid:{ctx.message.channel.id}"
            )
            return

        session = self.bot.helpers.get_db_session()
        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")

            # Get the guild settings and update the channel id
            settings = await self.bot.helpers.get_one_guild_settings(session, ctx.message.guild.id)
            settings.activity_status_enabled = enabled
            session.commit()

            # Update local cache
            self.bot.guild_settings[ctx.message.guild.id] = await self.bot.helpers.get_one_guild_settings(
                session, ctx.message.guild.id
            )

            return await ctx.send(f"Successfully set the Activity Status to: {enabled}.")

        except discord.HTTPException as err:
            self.bot.log.error(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except exc.DBAPIError as err:
            self.bot.log.exception(f"Database error with {ctx.command} command. {sys.exc_info()[0].__name__}: {err}")
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
            session.rollback()
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        finally:
            session.close()

    @commands.guild_only()
    @commands.is_owner()
    @activitystatus.command(aliases=["list", "l"], slash_command=False)
    async def activitystatus_list(self, ctx):
        """Lists all Activity Status that the bot displays/plays.

        Example:

        config activitystatus list
        config activitystatus l

        Requires Permission: Bot Owner

        Parameters
        -----------
        ctx: context
            The context message involved.
        """

        # Check if command is allowed to run here
        is_blocked = await self.bot.helpers.check_if_command_blocked(ctx)
        if is_blocked:
            self.bot.log.debug(
                f"CMD {ctx.command}: Blocked from running in gid:{ctx.message.guild.id}.cid:{ctx.message.channel.id}"
            )
            return

        session = self.bot.helpers.get_db_session()
        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")

            # Get current settings stored from the database
            guild_settings = await self.bot.helpers.get_one_guild_settings(session, ctx.message.guild.id)

            # If there are is no data, don't continue
            if guild_settings.activity_status is None:
                return await ctx.send(f"You do not have any status set.")

            embed_result_entries = []
            for status in guild_settings.activity_status:
                embed_result_entries.append(["Status:", status])

            footer_text = "Current status texts"

            p = FieldPages(
                ctx,
                per_page=8,
                entries=embed_result_entries,
            )
            p.embed.color = 0xFF8C00
            p.embed.set_author(
                name=f"Member: {ctx.message.author} ({ctx.message.author.id})",
                icon_url=ctx.message.author.display_avatar.url,
            )
            p.embed.set_footer(text=footer_text)
            # Send the results to the user
            return await p.paginate()

        except discord.HTTPException as err:
            self.bot.log.error(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except exc.DBAPIError as err:
            self.bot.log.exception(f"Database error with {ctx.command} command. {sys.exc_info()[0].__name__}: {err}")
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
            session.rollback()
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        finally:
            session.close()

    # Message AutoPublish Configuration
    @commands.guild_only()
    @commands.has_permissions(manage_guild=True)
    @config.group(aliases=["autopub"], invoke_without_command=False, slash_command=False)
    async def autopublish(self, ctx):
        """Base for the AutoPublish message configuration.

        See:
        `autopublish setting [#channel] [option]` - The channel & option to use. One of: disabled, all, specific_id, specific_role, all_human, all_bot
        `autopublish value [role/user id]` - The value of that option. E.g. the User ID or Role ID if those were chosen. Disabled, All, All_Human, All_Bot do not have an additional value.
        """
        pass

    @commands.guild_only()
    @commands.has_permissions(manage_guild=True)
    @autopublish.command(aliases=["setting", "option"], slash_command=False)
    async def autopublish_setting(self, ctx, channel: discord.TextChannel, option: str):
        """Sets the channel that's getting published, and what qualifier option to filter on. Only one option per channel.

        One of: disabled, all, specific_id, specific_role, all_human, all_bot

        Example:

        config autopublish setting #twitter all
        config autopublish setting #twitter all_human
        config autopublish setting #twitter specific_id

        Requires Permission: Manage Guild

        Parameters
        -----------
        ctx: context
            The context message involved.
        channel: discord.TextChannel
            The channel the message is being published from.
        option: str
            The qualifier option to filter on.
        """

        # Check if command is allowed to run here
        is_blocked = await self.bot.helpers.check_if_command_blocked(ctx)
        if is_blocked:
            self.bot.log.debug(
                f"CMD {ctx.command}: Blocked from running in gid:{ctx.message.guild.id}.cid:{ctx.message.channel.id}"
            )
            return

        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")
            # Check if the channel is of News type that allows publishing.
            if not channel.is_news():
                return await ctx.send(
                    f"Sorry, this channel does not appear to be a News channel. Please adjust the channel settings. You may need to enable the Community feature in Server Settings first."
                )
            # Make sure the option is valid
            options_allowed = [
                "disabled",
                "disable",
                "all",
                "any",
                "specific_id",
                "specific_role",
                "all_human",
                "all_bot",
            ]
            option = option.lower()
            if option not in options_allowed:
                return await ctx.send(
                    f"Sorry, the option {option} is not valid. Try one of the following: {options_allowed}"
                )
            # Set the setting in Redis
            result = self.bot.helpers.redis.set(
                f"discord.settings.autopub.gid:{ctx.message.guild.id}.cid:{channel.id}.setting_options",
                option,
            )
            if result:
                return await ctx.send(
                    f"Successfully set AutoPublish. Settings:\n"
                    f"**Channel:** {channel.mention}\n"
                    f"**Option:** {option}"
                )

        except discord.HTTPException as err:
            self.bot.log.error(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")

    @commands.guild_only()
    @commands.has_permissions(manage_guild=True)
    @autopublish.command(aliases=["value"], slash_command=False)
    async def autopublish_value(self, ctx, channel: discord.TextChannel, value: str):
        """Sets the value qualifier for the setting chosen.

        Usually a Role or User ID such as if you only want a specific user/bot or those who send messages that have a specific role on them.

        Example:

        config autopublish value #channel user_id
        config autopublish value #twitter role_id

        Requires Permission: Manage Guild

        Parameters
        -----------
        ctx: context
            The context message involved.
        channel: discord.TextChannel
            The channel the message is being published from.
        value: int
            The value for the qualifier set, such as a Role or User ID
        """

        # Check if command is allowed to run here
        is_blocked = await self.bot.helpers.check_if_command_blocked(ctx)
        if is_blocked:
            self.bot.log.debug(
                f"CMD {ctx.command}: Blocked from running in gid:{ctx.message.guild.id}.cid:{ctx.message.channel.id}"
            )
            return

        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")
            # Checks if the value is an integer aka a Role or User ID
            try:
                value = int(value)
            except ValueError:
                return await ctx.send(
                    f"That does not look like a proper User ID or Role ID. Please try again.\n"
                    f"**Option Value:** {value}"
                )
            # Set the setting in Redis
            result = self.bot.helpers.redis.set(
                f"discord.settings.autopub.gid:{ctx.message.guild.id}.cid:{channel.id}.value_id",
                value,
            )
            if result:
                return await ctx.send(
                    f"Successfully set AutoPublish. Settings:\n"
                    f"**Channel:** {channel.mention}\n"
                    f"**Option Value:** {value}"
                )

        except discord.HTTPException as err:
            self.bot.log.error(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")

    # Command Restriction Configuration
    @commands.guild_only()
    @commands.has_permissions(manage_guild=True)
    @config.group(aliases=["cmdres"], invoke_without_command=False, slash_command=False)
    async def cmd_restriction(self, ctx):
        """Base for the Command Restriction configuration."""
        pass

    @commands.guild_only()
    @commands.has_permissions(manage_guild=True)
    @cmd_restriction.command(aliases=["modify"], slash_command=False)
    async def cmd_res_modify(
        self,
        ctx,
        allow_or_block: str,
        add_or_remove: str,
        channels: str,
        *,
        command_name: str,
    ):
        """Sets what channels a command is allowed to run in. Channel_ID takes either the ID itself, list of ID's, or the word 'all' for all channels at the time.

        Example:
        config cmdres modify allow/block add/remove channel_id,channel_id2 command_name
        config cmdres modify allow/block add/remove all command_name
        config cmdres modify allow add 1234567890 ping
        config cmdres modify allow remove 1234567890 ping
        config cmdres modify block add all ping

        Requires Permission: Manage Guild

        Parameters
        -----------
        ctx: context
            The context message involved.
        allow_or_block: str
            Either the words 'allow' or 'block' to indicate if the list of channel(s) are being allowed or blocked for the command_name
        add_or_remove: str
            Either the words 'add' or 'remove' to indicate adding or removing channel(s) from the list
        channels: str
            Either a single or comma separated list of channels, or 'all' for all presently known
        command_name: str
            The command name the allow/block is being applied to
        """

        # Check if command is allowed to run here
        # We actually won't run the check here, otherwise you can block yourself from being able to manage the command blocks. Bad idea.
        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")
            # Verify the command name exists
            cmd = self.bot.get_command(command_name)
            if not cmd:
                return ctx.send(f"Sorry, unable to find a command named `{command_name}`")

            # Determine if we're allowing or blocking
            if allow_or_block.lower() == "allow":
                redis_key = f"discord.command_mgmt.allowed.gid:{ctx.guild.id}.cmd_name:{str(cmd).lower()}"
                allow = True
            elif allow_or_block.lower() == "block":
                redis_key = f"discord.command_mgmt.blocked.gid:{ctx.guild.id}.cmd_name:{str(cmd).lower()}"
                allow = False
            else:
                return ctx.send(
                    f"Sorry, you inputted `{allow_or_block}` and the expected values are only `allow` or `block`."
                )

            # Determine the channels we're applying this to
            if channels.lower() == "all":
                channels_list = ctx.guild.text_channels
            else:
                channels_list = []
                tmp_channels_list = channels.split(",")
                for chan in tmp_channels_list:
                    try:
                        tmp_chan = self.bot.get_channel(int(chan))
                        channels_list.append(tmp_chan)
                    except ValueError:
                        pass

            # Add the channels to the redis set based on the command name
            # Determine if we're adding or removing from the list
            results = None
            if add_or_remove.lower() == "add":
                adding = True
                # Start the pipeline
                pipeline = self.bot.helpers.redis.pipeline()
                for chan in channels_list:
                    pipeline.sadd(redis_key, chan.id)
                # All done, execute the pipeline
                results = pipeline.execute()
            elif add_or_remove.lower() == "remove":
                adding = False
                # Start the pipeline
                pipeline = self.bot.helpers.redis.pipeline()
                for chan in channels_list:
                    pipeline.srem(redis_key, chan.id)
                # All done, execute the pipeline
                results = pipeline.execute()
            else:
                return ctx.send(
                    f"Sorry, you inputted `{add_or_remove}` and the expected values are only `add` or `remove`."
                )

            if results:
                await ctx.reply(
                    f"Completed modification.\n"
                    f"**Action:** {'Added' if adding else 'Removed'} below channels on the {'allowed' if allow else 'blocked'} list for command **{str(cmd).lower()}**\n"
                    f"**Channels:** {[chan.mention for chan in channels_list]}"[:2000]
                )

        except discord.HTTPException as err:
            self.bot.log.error(f"Discord HTTP Error responding to {ctx.command}. {sys.exc_info()[0].__name__}: {err}")
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except Exception as err:
            self.bot.log.exception(f"Error responding to {ctx.command}. {sys.exc_info()[0].__name__}: {err}")
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")

    @commands.guild_only()
    @commands.has_permissions(manage_messages=True)
    @cmd_restriction.command(aliases=["showres"], slash_command=False)
    async def cmd_res_show_cmd_res(
        self,
        ctx,
        *,
        command_name: str,
    ):
        """Show the command restrictions per command.

        Example:
        config cmdres showres command_name
        config cmdres showres ping

        Requires Permission: Manage Messages

        Parameters
        -----------
        ctx: context
            The context message involved.
        command_name: str
            The command name the allow/block is being shown for
        """

        # Check if command is allowed to run here
        # We actually won't run the check here, otherwise you can block yourself from being able to manage the command blocks. Bad idea.
        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")
            # Verify the command name exists
            cmd = self.bot.get_command(command_name)
            if not cmd:
                return ctx.send(f"Sorry, unable to find a command named `{command_name}`")
            # Get the Redis data
            block_key = f"discord.command_mgmt.blocked.gid:{ctx.guild.id}.cmd_name:{str(cmd).lower()}"
            allow_key = f"discord.command_mgmt.allowed.gid:{ctx.guild.id}.cmd_name:{str(cmd).lower()}"

            channel_summary = []
            block_result = self.bot.helpers.redis.smembers(block_key)
            allow_result = self.bot.helpers.redis.smembers(allow_key)
            # Process the blocks
            for chanid in block_result:
                # Add the blocks to the list
                channel_summary.append(f"<#{chanid}> \N{CROSS MARK}")
            # Process the allows
            for chanid in allow_result:
                # Try to remove the block if it's in the allow, so we only have one entry
                try:
                    channel_summary.remove(f"<#{chanid}> \N{CROSS MARK}")
                except ValueError:
                    pass
                # Now add the allow
                channel_summary.append(f"<#{chanid}> \N{WHITE HEAVY CHECK MARK}")

            # If there are no overrides
            if len(channel_summary) == 0:
                channel_summary.append(f"No Restrictions Set")

            embed_result_entries = []
            for chan_status in channel_summary:
                # Format the embed
                data_title = f"Channel Status"
                data_value = chan_status
                embed_result_entries.append([data_title, data_value])

            # The results
            p = FieldPages(
                ctx,
                per_page=10,
                entries=embed_result_entries,
                mm_channel=ctx.message.channel,
            )
            p.embed.color = 0x8E7CC3
            p.embed.set_author(
                name=f"Channel Status for Command: {str(cmd).lower()}",
                icon_url=ctx.bot.user.display_avatar.url,
            )
            p.embed.set_footer(text="Channels not listed are allowed by default")
            await p.paginate()

        except discord.HTTPException as err:
            self.bot.log.error(f"Discord HTTP Error responding to {ctx.command}. {sys.exc_info()[0].__name__}: {err}")
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except Exception as err:
            self.bot.log.exception(f"Error responding to {ctx.command}. {sys.exc_info()[0].__name__}: {err}")
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")

    @commands.guild_only()
    @commands.has_permissions(manage_messages=True)
    @cmd_restriction.command(aliases=["listcmd"], slash_command=False)
    async def cmd_res_list_cmds(
        self,
        ctx,
    ):
        """Lists all of the proper command names.

        Example:
        config cmdres listcmd

        Requires Permission: Manage Messages

        Parameters
        -----------
        ctx: context
            The context message involved.
        """

        # Check if command is allowed to run here
        # We actually won't run the check here, otherwise you can block yourself from being able to manage the command blocks. Bad idea.
        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")
            # Create list for pagination
            embed_result_entries = []
            footer_text = "List of all top level commands."
            for cmd in ctx.bot.commands:
                # Format the embed
                data_title = f"Command Name"
                data_value = f"{str(cmd).lower()}"
                embed_result_entries.append([data_title, data_value])

            # Sort
            embed_result_entries.sort(key=lambda x: x[1])
            # The results
            p = FieldPages(
                ctx,
                per_page=10,
                entries=embed_result_entries,
                mm_channel=ctx.message.channel,
            )
            p.embed.color = 0x8E7CC3
            p.embed.set_author(
                name=f"All top level commands",
                icon_url=ctx.bot.user.display_avatar.url,
            )
            p.embed.set_footer(text=footer_text)
            await p.paginate()

        except discord.HTTPException as err:
            self.bot.log.error(f"Discord HTTP Error responding to {ctx.command}. {sys.exc_info()[0].__name__}: {err}")
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except Exception as err:
            self.bot.log.exception(f"Error responding to {ctx.command}. {sys.exc_info()[0].__name__}: {err}")
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")

    # Activity Role Configuration
    @commands.guild_only()
    @commands.has_permissions(manage_guild=True)
    @config.group(aliases=["ar"], invoke_without_command=False, slash_command=False)
    async def activityrole(self, ctx):
        """Base for the activity role configuration.

        See:
        """
        pass

    @commands.guild_only()
    @commands.has_permissions(manage_guild=True)
    @activityrole.command(aliases=["enable"], slash_command=False)
    async def activityrole_feature_enabled(self, ctx, enabled: bool):
        """Sets whether the feature is enabled or not. When enabled, a role is granted when user goes live. See other config for what role to grant and any game filters.

        Example:

        config activityrole enable true
        config activityrole enable false

        Requires Permission: Manage Guild

        Parameters
        -----------
        ctx: context
            The context message involved.
        enabled: bool
            Whether to enable or disable the feature
        """

        # Check if command is allowed to run here
        is_blocked = await self.bot.helpers.check_if_command_blocked(ctx)
        if is_blocked:
            self.bot.log.debug(
                f"CMD {ctx.command}: Blocked from running in gid:{ctx.message.guild.id}.cid:{ctx.message.channel.id}"
            )
            return

        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")
            # If we're enabling or disabling the feature
            # Adjust key in redis
            enabled_result = self.bot.helpers.redis.set(
                f"discord.settings.activityrole.gid:{ctx.message.guild.id}.feature_status",
                enabled,
            )
            # Display the results
            if enabled_result:
                return await ctx.send(
                    f"Successfully set Streamer Activity Role Feature to {'Enabled' if enabled else 'Disabled'}"
                )

        except discord.HTTPException as err:
            self.bot.log.error(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")

    @commands.guild_only()
    @commands.has_permissions(manage_guild=True)
    @activityrole.command(aliases=["anyrole"], slash_command=False)
    async def activityrole_anyrole_enabled(self, ctx, enabled: bool, role_id: typing.Optional[int] = None):
        """Sets whether a role is granted for any activity being streamed and that role, or if the feature is disabled.

        Example:

        config activityrole anyrole {enabled} {role_id}
        config activityrole anyrole true 123456
        config activityrole anyrole false

        Requires Permission: Manage Guild

        Parameters
        -----------
        ctx: context
            The context message involved.
        enabled: bool
            Whether a role is granted for any activity being streamed.
        role_id: typing.Optional[int]
            The role ID to grant to the user. Not needed if you are disabling the feature.
        """

        # Check if command is allowed to run here
        is_blocked = await self.bot.helpers.check_if_command_blocked(ctx)
        if is_blocked:
            self.bot.log.debug(
                f"CMD {ctx.command}: Blocked from running in gid:{ctx.message.guild.id}.cid:{ctx.message.channel.id}"
            )
            return

        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")

            # If we're enabling the feature
            enabled_result = None
            if enabled:
                # Enable key in redis
                enabled_result = self.bot.helpers.redis.set(
                    f"discord.settings.activityrole.gid:{ctx.message.guild.id}.any_activity",
                    enabled,
                )

            # If we're setting a role
            role_result = None
            if role_id:
                role = ctx.message.guild.get_role(role_id)
                if not role:
                    await ctx.send(f"Sorry, unable to find a role by the ID of {role_id}")
                else:
                    # Set the key in redis
                    role_result = self.bot.helpers.redis.set(
                        f"discord.settings.activityrole.gid:{ctx.message.guild.id}.any_activity_role",
                        role.id,
                    )

            return await ctx.send(
                f"**__Configured Streamer Role:__**\n"
                f"**Game:** Any Activity\n"
                f"**Status:** {'Enabled' if enabled_result else 'Disabled'}\n"
                f"**Role:** {role.name if role_result else 'N/A'}"
            )

        except discord.HTTPException as err:
            self.bot.log.error(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")

    @commands.guild_only()
    @commands.has_permissions(manage_guild=True)
    @activityrole.command(aliases=["specific"], slash_command=False)
    async def activityrole_specific_enabled(
        self,
        ctx,
        enabled: bool,
        role_id: typing.Optional[int] = None,
        *,
        activity_name: str,
    ):
        """Sets whether a role is granted for a specific activity being streamed and that role, or if the feature is disabled.

        Activity Name can be retrieved from the User Stats command, and is taken exactly as written, case-insensitive.

        Example:

        config activityrole specific {enabled} {role_id} {activity_name}
        config activityrole specific true 123456 Just Chatting
        config activityrole specific false

        Requires Permission: Manage Guild

        Parameters
        -----------
        ctx: context
            The context message involved.
        enabled: bool
            Whether a role is granted for any activity being streamed.
        role_id: typing.Optional[int]
            The role ID to grant to the user. Not needed if disabling the feature.
        activity_name: str
            The name of the activity that a role is granted for when that specific activity is being streamed.
        """

        # Check if command is allowed to run here
        is_blocked = await self.bot.helpers.check_if_command_blocked(ctx)
        if is_blocked:
            self.bot.log.debug(
                f"CMD {ctx.command}: Blocked from running in gid:{ctx.message.guild.id}.cid:{ctx.message.channel.id}"
            )
            return

        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")

            # If we're enabling the feature
            enabled_result = None
            if enabled:
                # Enable key in redis
                enabled_result = self.bot.helpers.redis.set(
                    f"discord.settings.activityrole.gid:{ctx.message.guild.id}.any_activity",
                    enabled,
                )

            # If we're setting a role
            role_result = None
            if role_id:
                role = ctx.message.guild.get_role(role_id)
                if not role:
                    await ctx.send(f"Sorry, unable to find a role by the ID of {role_id}")
                else:
                    # Set the key in redis
                    role_result = self.bot.helpers.redis.set(
                        f"discord.settings.activityrole.gid:{ctx.message.guild.id}.any_activity_role",
                        role.id,
                    )

                return await ctx.send(
                    f"**__Configured Streamer Role:__**\n"
                    f"**Game:** Any Activity\n"
                    f"**Status:** {'Enabled' if enabled_result else 'Disabled'}\n"
                    f"**Role:** {role.name if role_result else 'N/A'}"
                )

        except discord.HTTPException as err:
            self.bot.log.error(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")


async def setup(bot):
    await bot.add_cog(Config(bot))
