import sys

import discord
from discord.ext import commands


class MMConfig(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.has_permissions(manage_guild=True)
    @commands.guild_only()
    @commands.group(
        name="Config.ModMail",
        aliases=["mmconfig", "mmconfigure"],
        case_insensitive=True,
        invoke_without_command=False,
        slash_command=False,
    )
    async def mmconfig(self, ctx):
        """Allows for setting various mod mail configuration."""
        pass

    @commands.guild_only()
    @commands.is_owner()
    @mmconfig.command(aliases=["serverid"], slash_command=False)
    async def server(self, ctx, modmail_server_id: int):
        """Sets the mod mail server ID.

        Example:

        mmconfig server serverID
        mmconfig server 123456

        Requires Permission: Bot Owner

        Parameters
        -----------
        ctx: context
            The context message involved.
        modmail_server_id: int
            Server ID of the Mod Mail server.
        """

        # Check if command is allowed to run here
        is_blocked = await self.bot.helpers.check_if_command_blocked(ctx)
        if is_blocked:
            self.bot.log.debug(
                f"CMD {ctx.command}: Blocked from running in gid:{ctx.message.guild.id}.cid:{ctx.message.channel.id}"
            )
            return

        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")

            mm_guild = self.bot.get_guild(modmail_server_id)
            result = self.bot.helpers.redis.set(
                f"discord.settings.modmail.bid:{self.bot.user.id}.modmail_server_id",
                mm_guild.id,
            )
            if result:
                return await ctx.send(f"Successfully set the mod mail server to: {mm_guild.name} ({mm_guild.id}).")
            else:
                return await ctx.send(
                    f"There was an error setting the mod mail server to: {mm_guild.name} ({mm_guild.id})."
                )

        except discord.HTTPException as err:
            self.bot.log.error(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")

    @commands.guild_only()
    @commands.is_owner()
    @mmconfig.command(slash_command=False)
    async def unanswered(self, ctx, category_id: int):
        """Sets the mod mail unanswered category ID.

        Example:

        mmconfig unanswered categoryID
        mmconfig unanswered 123456

        Requires Permission: Bot Owner

        Parameters
        -----------
        ctx: context
            The context message involved.
        category_id: int
            Category ID for the Unanswered category in the Mod Mail server.
        """

        # Check if command is allowed to run here
        is_blocked = await self.bot.helpers.check_if_command_blocked(ctx)
        if is_blocked:
            self.bot.log.debug(
                f"CMD {ctx.command}: Blocked from running in gid:{ctx.message.guild.id}.cid:{ctx.message.channel.id}"
            )
            return

        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")

            mm_category = self.bot.get_channel(category_id)
            result = self.bot.helpers.redis.set(
                f"discord.settings.modmail.bid:{self.bot.user.id}.modmail_unanswered_cat_id",
                mm_category.id,
            )

            if result:
                return await ctx.send(
                    f"Successfully set the mod mail unanswered category to: {mm_category.name} ({mm_category.id})."
                )
            else:
                return await ctx.send(
                    f"There was an error setting the mod mail unanswered category to: {mm_category.name} ({mm_category.id})."
                )

        except discord.HTTPException as err:
            self.bot.log.error(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")

    @commands.guild_only()
    @commands.is_owner()
    @mmconfig.command(slash_command=False)
    async def inprogress(self, ctx, category_id: int):
        """Sets the mod mail in progress category ID.

        Example:

        mmconfig inprogress categoryID
        mmconfig inprogress 123456

        Requires Permission: Bot Owner

        Parameters
        -----------
        ctx: context
            The context message involved.
        category_id: int
            Category ID for the In Progress category in the Mod Mail server.
        """

        # Check if command is allowed to run here
        is_blocked = await self.bot.helpers.check_if_command_blocked(ctx)
        if is_blocked:
            self.bot.log.debug(
                f"CMD {ctx.command}: Blocked from running in gid:{ctx.message.guild.id}.cid:{ctx.message.channel.id}"
            )
            return

        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")

            mm_category = self.bot.get_channel(category_id)
            result = self.bot.helpers.redis.set(
                f"discord.settings.modmail.bid:{self.bot.user.id}.modmail_in_progress_cat_id",
                mm_category.id,
            )

            if result:
                return await ctx.send(
                    f"Successfully set the mod mail In Progress category to: {mm_category.name} ({mm_category.id})."
                )
            else:
                return await ctx.send(
                    f"There was an error setting the mod mail In Progress category to: {mm_category.name} ({mm_category.id})."
                )

        except discord.HTTPException as err:
            self.bot.log.error(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")

    @commands.guild_only()
    @commands.is_owner()
    @mmconfig.command(aliases=["main.server"], slash_command=False)
    async def mainserver(self, ctx, main_server_id: int):
        """Sets the server ID of the Main Server.

        Example:

        mmconfig mainserver serverID
        mmconfig mainserver 123456

        Requires Permission: Bot Owner

        Parameters
        -----------
        ctx: context
            The context message involved.
        main_server_id: int
            Server ID that represents the main server, to pull user History, etc.
        """

        # Check if command is allowed to run here
        is_blocked = await self.bot.helpers.check_if_command_blocked(ctx)
        if is_blocked:
            self.bot.log.debug(
                f"CMD {ctx.command}: Blocked from running in gid:{ctx.message.guild.id}.cid:{ctx.message.channel.id}"
            )
            return

        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")

            main_guild = self.bot.get_guild(main_server_id)
            result = self.bot.helpers.redis.set(
                f"discord.settings.modmail.bid:{self.bot.user.id}.main_server_id",
                main_guild.id,
            )

            if result:
                return await ctx.send(f"Successfully set the Main Server to: {main_guild.name} ({main_guild.id}).")
            else:
                return await ctx.send(
                    f"There was an error setting the Main Server to: {main_guild.name} ({main_guild.id})."
                )

        except discord.HTTPException as err:
            self.bot.log.error(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")

    @commands.guild_only()
    @commands.is_owner()
    @mmconfig.command(slash_command=False)
    async def reload_modmail(self, ctx):
        """Reloads the Mod Mail extension. This is needed after first time mod mail setup, or if you change the config ID's.

        Example:

        mmconfig reload_modmail

        Requires Permission: Bot Owner

        Parameters
        -----------
        ctx: context
            The context message involved.
        """

        # Check if command is allowed to run here
        is_blocked = await self.bot.helpers.check_if_command_blocked(ctx)
        if is_blocked:
            self.bot.log.debug(
                f"CMD {ctx.command}: Blocked from running in gid:{ctx.message.guild.id}.cid:{ctx.message.channel.id}"
            )
            return

        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")

            extension_name = "utilities.modmail"
            try:
                self.bot.reload_extension(extension_name)
                self.bot.log.info(f"Reloaded extension {extension_name}")
                await ctx.send(f"Successfully reloaded extension: {extension_name}")
            except commands.ExtensionAlreadyLoaded:
                pass

        except discord.HTTPException as err:
            self.bot.log.error(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")


async def setup(bot):
    await bot.add_cog(MMConfig(bot))
