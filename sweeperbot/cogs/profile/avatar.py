import sys
from typing import Optional

import discord
from discord.ext import commands


class Avatar(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.hybrid_command(aliases=["avi", "pfp"])
    @discord.app_commands.describe(user_id="The User ID to get the profile picture for")
    @commands.has_permissions(send_messages=True)
    @commands.bot_has_permissions(attach_files=True, send_messages=True)
    @commands.guild_only()
    async def avatar(self, ctx, user_id: Optional[str]):
        """Shows the profile picture of the requested user.

        Example:
        pfp userID
        """

        # Check if command is allowed to run here
        is_blocked = await self.bot.helpers.check_if_command_blocked(ctx)
        if is_blocked:
            self.bot.log.debug(
                f"CMD {ctx.command}: Blocked from running in gid:{ctx.message.guild.id}.cid:{ctx.message.channel.id}"
            )
            return await ctx.send("Sorry, this command is blocked.", ephemeral=True)

        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")
            # Check if user is blacklisted, if so, ignore.
            if await self.bot.helpers.check_if_blacklisted(ctx.message.author.id, ctx.message.guild.id):
                self.bot.log.debug(
                    f"User {ctx.message.author} ({ctx.message.author.id}) Blacklisted, unable to use command {ctx.command}"
                )
                return await ctx.send("Sorry, you're not allowed to use this command", ephemeral=True)

            user = None
            # If a user is provided, then get their profile
            if user_id:
                user = await self.bot.helpers.get_member_or_user(user_id, ctx.message.guild)
                if not user:
                    return await ctx.send(
                        f"Unable to find the requested user. Please make sure the user ID or @ mention is valid."
                    )
            # If no user ID then pull user info from the command caller
            if not user:
                user = ctx.message.author
            # Get all the possible avatar assets
            # If this is a Member, see if they have a Guild Avatar and get that, and get the User avatar
            guild_asset = None
            user_asset = None
            tags = ["avatar_command"]
            context = {"userid": f"{user.id}"}
            if isinstance(user, discord.Member):
                if user.guild_avatar:
                    gavatar = await self.bot.helpers.upload_image(user.guild_avatar, tags=tags, context=context)
                    if gavatar.secure_url:
                        await ctx.send(f"{gavatar.secure_url}")
            if user.avatar:
                uavatar = await self.bot.helpers.upload_image(user.avatar, tags=tags, context=context)
                if uavatar.secure_url:
                    await ctx.send(f"{uavatar.secure_url}")

        except discord.HTTPException as err:
            self.bot.log.error(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")


async def setup(bot):
    await bot.add_cog(Avatar(bot))
