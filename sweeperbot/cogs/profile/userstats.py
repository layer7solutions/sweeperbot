import sys
import typing
from datetime import datetime, timezone
from distutils.util import strtobool

import discord
from discord.ext import commands
from utilities.helpers import set_sentry_scope


class UserStats(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.hybrid_command(aliases=["us", "user"], with_app_command=True)
    @discord.app_commands.describe(
        user_id="The User ID to get the stats for",
        ephemeral="Default: No | If message should only be visible to you",
    )
    @commands.guild_only()
    async def userstats(
        self,
        ctx,
        user_id: typing.Optional[str],
        ephemeral: typing.Literal["Yes", "No"] = "No",
    ):
        """Shows info about the user.

        Requires Permission: Send Messages

        Parameters
        -----------
        ctx: context
            The context message involved.
        user_id: Optional[str]
            The Discord ID the user stats should be retrieved for.
        ephemeral: Optional[bool]
            If message should only be visible to you. Default No.
        """
        # Check if command is allowed to run here
        is_blocked = await self.bot.helpers.check_if_command_blocked(ctx)
        if is_blocked:
            self.bot.log.debug(
                f"CMD {ctx.command}: Blocked from running in gid:{ctx.message.guild.id}.cid:{ctx.message.channel.id}"
            )
            return await ctx.send("Sorry, this command is blocked.", ephemeral=True)
        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")
            # Check if user is blacklisted, if so, ignore.
            if await self.bot.helpers.check_if_blacklisted(ctx.message.author.id, ctx.message.guild.id):
                self.bot.log.debug(
                    f"User {ctx.message.author} ({ctx.message.author.id}) Blacklisted, unable to use command {ctx.command}"
                )
                return await ctx.send("Sorry, you're not allowed to use this command", ephemeral=True)

            member = None
            modmail_bypass = False
            # Set ephemeral to boolean value
            ephemeral = bool(strtobool(ephemeral)) if ephemeral else False
            # If we were provided an ID, let's try and use it
            if user_id:
                member = await self.bot.helpers.get_member_or_user(user_id, ctx.message.guild)
                if not member:
                    return await ctx.send(
                        f"Unable to find the requested user `{user_id}`. For best results, please use the User ID."
                    )

            # If no user ID then pull user stats from the command caller
            if not member:
                member = ctx.message.author

            embed = discord.Embed(color=0xFF8C00, timestamp=datetime.now(timezone.utc))
            embed.set_author(name=f"{member.name} ({member.id})", icon_url=member.display_avatar.url)

            # If it's a User meaning we get limited info
            if isinstance(member, discord.User):
                # Discord join date
                discord_join_date = self.bot.helpers.relative_time(
                    datetime.now(timezone.utc), member.created_at, brief=True
                )
                discord_join_date = (
                    f"{member.created_at.replace(tzinfo=None, microsecond=0)}\n*{discord_join_date} ago*"
                )
                embed.add_field(name="Joined Discord", value=f"{discord_join_date}")
            # If it's a Member meaning we get guild specific and detailed info
            elif isinstance(member, discord.Member):
                if ctx.interaction:
                    # get the member to get activities, status, and banner as it doesn't come normally via interaction
                    account_id = member.id
                    member = ctx.message.guild.get_member(account_id)
                    if member is None:
                        self.bot.log.debug(f"Had to fetch member for {account_id}")
                        member = ctx.message.guild.fetch_member(account_id)
                        if member is None:
                            return

                member_names = (
                    f"__Active Display:__ {member.display_name}\n"
                    f"__Server Nick:__ {member.nick}\n"
                    f"__Global Display:__ {member.global_name}\n\n"
                )

                # If they have an activity status (Playing, Watching, Custom), add that
                tmp_activities = []
                activities = ""
                for activity in member.activities:
                    if activity.type == discord.ActivityType.custom:
                        tmp_activities.append(
                            f"\N{SPEECH BALLOON} **{self.bot.constants.activity_enum[activity.type.value]}:** {activity}"
                        )
                    elif activity.type == discord.ActivityType.playing:
                        tmp_activities.append(
                            f"{self.bot.constants.statuses['playing']} **{self.bot.constants.activity_enum[activity.type.value]}:** {activity.name}"
                        )
                    elif activity.type == discord.ActivityType.streaming:
                        tmp_activities.append(
                            f"{self.bot.constants.statuses['twitch']} **{self.bot.constants.activity_enum[activity.type.value]}:**\n"
                            f"<:spacer:328352361569583105> **Game:** {activity.game}\n"
                            f"<:spacer:328352361569583105> **Title:** {activity.name}"
                        )
                    elif activity.type == discord.ActivityType.listening:
                        # Because a listening activity can sometimes not have the title attribute, going to error handle and use details as a default
                        try:
                            activity_title = activity.title
                        except NameError:
                            activity_title = activity.details

                        tmp_activities.append(
                            f"{self.bot.constants.statuses['listening']} **{self.bot.constants.activity_enum[activity.type.value]}:** {activity_title}"
                        )
                    else:
                        tmp_activities.append(
                            f"**{self.bot.constants.activity_enum[activity.type.value]}:** {activity.name}"
                        )

                # Put activities together
                if tmp_activities:
                    activities = "\n".join(tmp_activities)
                    activities = f"**__Activities__**\n{activities}\n\n"

                # If they are in a Voice Channel, add that
                voice_info = ""
                if member.voice and member.voice.channel:
                    voice_info = f"**__Voice Channel__**\n**Name:** {member.voice.channel.name}"

                # Put the Member Names, Activities, and Voice Info together
                embed.title = f"Names"
                embed.description = f"{member_names}{activities}{voice_info}"

                # User Status
                discord_status = str(member.status)
                embed.add_field(
                    name="User Status",
                    value=f"{self.bot.constants.statuses[discord_status]} {self.bot.constants.statuses['mobile'] if member.is_on_mobile() else ''}\n{member.mention}",
                )
                # Guild join date
                guild_join_date = self.bot.helpers.relative_time(
                    datetime.now(timezone.utc), member.joined_at, brief=True
                )
                guild_member_join_date = (
                    member.joined_at.replace(tzinfo=None, microsecond=0) if member.joined_at else "Unknown"
                )
                guild_join_date = f"{guild_member_join_date} UTC\n*{guild_join_date} ago*"
                embed.add_field(name="Joined Guild", value=f"{guild_join_date}")

                # Discord join date
                discord_member_join_date = (
                    member.created_at.replace(tzinfo=None, microsecond=0) if member.created_at else "Unknown"
                )
                discord_join_date = self.bot.helpers.relative_time(
                    datetime.now(timezone.utc), member.created_at, brief=True
                )
                discord_join_date = f"{discord_member_join_date} UTC\n*{discord_join_date} ago*"
                embed.add_field(name="Joined Discord", value=f"{discord_join_date}")

                # Roles
                roles = list(reversed(member.roles))
                roles_temp = []
                if modmail_bypass:
                    for role in roles:
                        roles_temp.append(role.name)
                else:
                    for role in roles:
                        roles_temp.append(role.mention)

                # little logic to split into embeds with 1000 characters max
                out = ""
                for line in roles_temp:
                    out += ", " + line
                    if len(out) > 950:
                        embed.add_field(name="Roles", value=out.lstrip(", "))
                        out = ""

                if len(out) != 0:
                    embed.add_field(name="Roles", value=out.lstrip(", "))

            # Done, send the response
            await ctx.send(f"{member} ({member.id})", embed=embed, ephemeral=ephemeral)

        except discord.HTTPException as err:
            set_sentry_scope(ctx)
            self.bot.log.exception(
                f"Discord HTTP Error responding to {ctx.command}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except Exception as err:
            set_sentry_scope(ctx)
            self.bot.log.exception(f"Error responding to {ctx.command}. {sys.exc_info()[0].__name__}: {err}")
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")


async def setup(bot):
    await bot.add_cog(UserStats(bot))
