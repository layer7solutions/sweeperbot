import sys

import discord
from discord.ext import commands


class GuildStats(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.hybrid_command(aliases=["stats"], with_app_command=True)
    @commands.has_permissions(send_messages=True)
    @commands.guild_only()
    async def guildstats(self, ctx):
        """Lists guild statistics including: member, presence, channel & booster counts"""

        # Check if command is allowed to run here
        is_blocked = await self.bot.helpers.check_if_command_blocked(ctx)
        if is_blocked:
            self.bot.log.debug(
                f"CMD {ctx.command}: Blocked from running in gid:{ctx.message.guild.id}.cid:{ctx.message.channel.id}"
            )
            return await ctx.send("Sorry, this command is blocked.", ephemeral=True)

        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")
            # Check if user is blacklisted, if so, ignore.
            if await self.bot.helpers.check_if_blacklisted(ctx.message.author.id, ctx.message.guild.id):
                self.bot.log.debug(
                    f"User {ctx.message.author} ({ctx.message.author.id}) Blacklisted, unable to use command {ctx.command}"
                )
                return await ctx.send("Sorry, you're not allowed to use this command", ephemeral=True)

            guild = ctx.message.guild
            text_channels = guild.text_channels or []

            # In order to get a more accurate Member Count and Presence Count, we need to pull from an invite code
            member_count = int(guild.member_count)
            presence_count = 0
            # Get guild invite
            invite = await self.bot.helpers.get_guild_invite(guild)

            if invite:
                member_count = int(invite.approximate_member_count)
                presence_count = invite.approximate_presence_count

            # Create the embed
            embed = discord.Embed(
                title=f"Guild Owner: {guild.owner if guild.owner else 'Unavailable'} ({guild.owner_id})"
            )
            embed.set_author(
                name=f"Statistics for {ctx.message.guild.name} ({ctx.message.guild.id})",
                icon_url=guild.icon.url if guild.icon else None,
            )
            # This is number of members currently in the guild
            # We're going to calculate how many max members there can be. So far the tiers are 100k, 250k, 500k
            # If we can get the value Discord provides us, use that
            if guild.max_members:
                max_members = guild.max_members
            # If less than or equal to 100,000 then use 100k
            elif member_count <= 100000:
                max_members = 100000
            # If less than or equal to 250,000 then use 250,
            elif member_count <= 250000:
                max_members = 250000
            # If less than or equal to 500,000 then use 500k
            elif member_count <= 500000:
                max_members = 500000
            # Otherwise, give it 0 to indicate that we don't know
            else:
                max_members = 0

            embed.add_field(
                name="Member Count",
                value=f"{member_count:,} / {max_members:,}",
                inline=True,
            )
            # This is maximum number of presence members, or people currently connected to the guild
            # Max presence count most of the time is equal to max members due to new Relay tech that
            # allows the limit to no longer be needed for all but the largest guilds.
            max_presences = guild.max_presences if guild.max_presences else max_members
            embed.add_field(
                name="Presence Count",
                value=f"{presence_count:,} / {max_presences:,}",
                inline=True,
            )
            # Number of text channels
            embed.add_field(name="Text Channels", value=f"{len(text_channels)}", inline=True)
            # Number of voice users
            voice_channels = guild.voice_channels + guild.stage_channels
            total_voice_users = 0
            total_voice_channels = 0
            used_voice_channels = 0
            for vc in voice_channels:
                try:
                    total_voice_users += len(vc.members)
                    total_voice_channels += 1
                    if len(vc.members) > 0:
                        used_voice_channels += 1
                except AttributeError as err:
                    # Likely a broken channel/new channel the Library can't handle, pass to ignore
                    pass
                except Exception as err:
                    self.bot.log.exception(f"Error getting VC Details. {sys.exc_info()[0].__name__}: {err}")

            embed.add_field(
                name="Active Voice Users",
                value=f"{total_voice_users}",
                inline=True,
            )
            # Number of voice channels
            embed.add_field(
                name="Active Voice Channels",
                value=f"{used_voice_channels} / {total_voice_channels}",
                inline=True,
            )
            # Total number of boosts
            embed.add_field(
                name="Total Boosts",
                value=f"{guild.premium_subscription_count}",
                inline=True,
            )
            # Total unique boosters
            embed.add_field(
                name="Unique Boosters",
                value=f"{len(guild.premium_subscribers)}",
                inline=True,
            )
            await ctx.send(embed=embed)

        except discord.HTTPException as err:
            self.bot.log.exception(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")


async def setup(bot):
    await bot.add_cog(GuildStats(bot))
