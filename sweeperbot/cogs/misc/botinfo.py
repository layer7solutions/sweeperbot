import asyncio
import os
import sys
from datetime import datetime, timezone

import discord
import psutil
from discord.ext import commands


class BotInfo(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.hybrid_command(aliases=["invite"], with_app_command=True)
    @commands.has_permissions(send_messages=True)
    @commands.guild_only()
    async def botinfo(self, ctx):
        """Provides info about bot inc: invite link, source code, memory & cpu usage, etc."""

        # Check if command is allowed to run here
        is_blocked = await self.bot.helpers.check_if_command_blocked(ctx)
        if is_blocked:
            self.bot.log.debug(
                f"CMD {ctx.command}: Blocked from running in gid:{ctx.message.guild.id}.cid:{ctx.message.channel.id}"
            )
            return
        try:
            self.bot.log.info(f"CMD {ctx.invoked_with} called by {ctx.message.author} ({ctx.message.author.id})")
            # Check if user is blacklisted, if so, ignore.
            if await self.bot.helpers.check_if_blacklisted(ctx.message.author.id, ctx.message.guild.id):
                self.bot.log.debug(
                    f"User {ctx.message.author} ({ctx.message.author.id}) Blacklisted, unable to use command {ctx.command}"
                )
                return

            # Get bot uptime
            uptime = self.bot.helpers.relative_time(datetime.now(timezone.utc), self.bot.started_time, brief=True)

            # Get system usage
            process = psutil.Process(os.getpid())
            # Needs to be called twice to get accurate values
            # https://psutil.readthedocs.io/en/latest/#psutil.Process.cpu_percent
            cpu_usage = process.cpu_percent(interval=None)
            await asyncio.sleep(0.5)
            cpu_usage = process.cpu_percent(interval=None)
            try:
                memory_usage = process.memory_full_info().uss / 1024 ** 2
            except psutil.AccessDenied:
                memory_usage = 0.00

            # Create the embed of info
            embed = discord.Embed(
                color=discord.Color.blurple(),
                title="Bot Invite Link & Info",
                timestamp=datetime.now(timezone.utc),
            )
            # Gets the bot owner info.
            if isinstance(self.bot.owner, list):
                owners = [f"{temp_user.mention} ({temp_user.id})" for temp_user in self.bot.owner]
            else:
                owners = [f"{self.bot.owner.mention} ({self.bot.owner.id})"]
            owners_string = ", ".join(owners)
            embed.add_field(name="Bot Owner", value=f"{owners_string}", inline=False)

            # Generate Invite Permissions Link
            perms = discord.Permissions(
                # General Permissions
                administrator=False,  # Never needed
                view_audit_log=True,  # Watching ban/kicks done outside the bot
                view_guild_insights=False,
                manage_guild=True,  # Used for getting list of guild invites
                manage_roles=True,  # For adding/removing roles during mutes
                manage_channels=True,
                kick_members=True,  # To use the kick users command
                ban_members=True,  # To use the ban users command
                create_instant_invite=True,  # To allow more accurate server stats
                change_nickname=False,
                manage_nicknames=True,
                manage_emojis_and_stickers=False,
                manage_webhooks=False,
                read_messages=True,  # To view server messages
                manage_events=False,
                moderate_members=True,
                # Text Permissions
                send_messages=True,  # To send messages
                send_messages_in_threads=True,
                create_public_threads=False,
                create_private_threads=False,
                manage_threads=True,
                send_tts_messages=False,
                manage_messages=True,
                embed_links=True,
                attach_files=True,
                read_message_history=True,
                mention_everyone=False,
                external_emojis=True,
                add_reactions=True,
                # Voice
                connect=False,
                speak=False,
                # video = False,
                mute_members=True,
                deafen_members=True,
                move_members=True,  # Used to disconnect users from voice when muted
                use_voice_activation=False,
                priority_speaker=False,
            )
            invite_url = discord.utils.oauth_url(
                client_id=self.bot.user.id, permissions=perms, scopes=["bot", "applications.commands"]
            )
            embed.add_field(
                name="Invite Link",
                value=f"[Click here.]({invite_url})",
                inline=True,
            )
            embed.add_field(
                name="Source Code",
                value=f"[Click here.]({self.bot.constants.repository_link})",
                inline=True,
            )
            embed.add_field(
                name="Support Server",
                value=f"[Click here.]({self.bot.constants.support_invite_link})",
                inline=True,
            )
            embed.add_field(name="Bot Version", value=f"{self.bot.version}", inline=True)
            embed.add_field(name="# of Guilds", value=f"{len(self.bot.guilds)}", inline=True)
            embed.add_field(name="Memory Usage", value=f"{memory_usage:.2f} MiB", inline=True)
            embed.add_field(name="CPU Usage", value=f"{cpu_usage}%", inline=True)
            embed.add_field(name="Bot Uptime", value=f"{uptime}", inline=True)
            # API Versions
            # Get the HTTP API Version
            http_ver = discord.http.Route.BASE.split("/")[-1]
            # Get the Websocket Version
            wss_ver = await ctx.bot.http.get_bot_gateway()
            wss_ver = wss_ver[1].split("&")[1].replace("=", "")
            embed.add_field(name="API Versions", value=f"HTTP: {http_ver}\nWebsocket: {wss_ver}", inline=True)
            embed.add_field(
                name="Donation Link",
                value=f"Appreciate the project and want to donate? [Please click here.]({self.bot.constants.patreon_link})",
                inline=False,
            )

            await ctx.send(embed=embed)
        except discord.HTTPException as err:
            self.bot.log.exception(
                f"Discord HTTP Error responding to {ctx.command} request via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")


async def setup(bot):
    await bot.add_cog(BotInfo(bot))
