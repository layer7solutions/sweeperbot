import sys
from datetime import datetime, timezone

import discord
from discord.ext import commands


class Ping(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.hybrid_command(with_app_command=True)
    @commands.has_permissions(manage_messages=True)
    @commands.guild_only()
    @commands.cooldown(6, 60, commands.BucketType.guild)
    async def ping(self, ctx):
        """Shows the bots Websocket, Redis & Message latency"""

        # This variable we want as soon as possible
        response_time = round((datetime.now(timezone.utc) - ctx.message.created_at).total_seconds() * 1000, 2)
        # Check if command is allowed to run here
        is_blocked = await self.bot.helpers.check_if_command_blocked(ctx)
        if is_blocked:
            return self.bot.log.debug(
                f"CMD {ctx.command}: Blocked from running in gid:{ctx.message.guild.id}.cid:{ctx.message.channel.id}"
            )
        try:
            self.bot.log.info(f"CMD {ctx.command} called by {ctx.message.author} ({ctx.message.author.id})")
            # Check if user is blacklisted, if so, ignore.
            if await self.bot.helpers.check_if_blacklisted(ctx.message.author.id, ctx.message.guild.id):
                self.bot.log.debug(
                    f"User {ctx.message.author} ({ctx.message.author.id}) Blacklisted, unable to use command {ctx.command}"
                )
                return
            msg = await ctx.send(f"Pong! 🏓")
            msg_diff = round((msg.created_at - ctx.message.created_at).total_seconds() * 1000, 2)
            api_latency = round(self.bot.latency * 1000, 2)
            redis_latency = round(self.bot.helpers.redis_latency(), 2)
            await msg.edit(
                content=f"Pong! 🏓"
                f"\n"
                f"\n**API Latency:** {api_latency}ms"
                f"\n- This is the Websocket latency"
                f"\n**Initial Message Delivery Latency:** {response_time}ms"
                f"\n- This is the time taken from your message, to when the bot received the event to process it"
                f"\n**Message Response Latency:** {msg_diff}ms"
                f"\n- This is the time from your message, to time to send a response"
                f"\n**Redis Latency:** {redis_latency}ms"
                f"\n- This is the latency for our Redis service"
            )
        except discord.HTTPException as err:
            self.bot.log.exception(
                f"Discord HTTP Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")
        except Exception as err:
            self.bot.log.exception(
                f"Error responding to {ctx.command} via Msg ID {ctx.message.id}. {sys.exc_info()[0].__name__}: {err}"
            )
            await ctx.send(f"Error processing {ctx.command}. Error has already been reported to my developers.")


async def setup(bot):
    await bot.add_cog(Ping(bot))
