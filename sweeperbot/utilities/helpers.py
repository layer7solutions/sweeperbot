import asyncio
import datetime
import math
import re
import sys
from random import SystemRandom
from time import sleep
from typing import Optional, Union

import aiohttp
import discord
from cogs.utils.timer import Timer
from db import models
from discord.ext import commands
from redis import TimeoutError as rTimeoutError
from rediscluster import RedisCluster, exceptions
from sentry_sdk import configure_scope
from sqlalchemy import desc
from sqlalchemy.exc import DBAPIError, IntegrityError
from sqlalchemy.orm import aliased
from sqlalchemy.sql import func
from sqlalchemy.sql.expression import literal_column

from .utility_classes import Image


class Helpers:
    def __init__(self, bot):
        self.bot = bot
        self.reminds_already_loaded = None
        self.redis = None
        self.sysrand = SystemRandom()
        # Create the cooldown mapping
        self.cd_AutoVC: commands.CooldownMapping = commands.CooldownMapping.from_cooldown(
            5, 300, commands.BucketType.user
        )
        # Create the redis connection
        redis_hosts = self.bot.botconfig.get("Redis", "HOST").split(",")
        redis_password = self.bot.botconfig.get("Redis", "PASSWORD")
        redis_socket_timeout = self.bot.botconfig.getint("Redis", "SOCKET_TIMEOUT")
        node_list = []
        for node in redis_hosts:
            temp_d = {"host": node, "port": "6379"}
            node_list.append(temp_d)

        try:
            self.redis = RedisCluster(
                startup_nodes=node_list,
                decode_responses=True,
                password=redis_password,
                socket_timeout=redis_socket_timeout,
                # skip_full_coverage_check=True,
            )
        except exceptions.RedisClusterException as err:
            self.bot.log.exception(f"Redis Cluster Unavailable. {sys.exc_info()[0].__name__}: {err}")
            sleep(60 * 5)
        except Exception as err:
            self.bot.log.exception(f"Unknown Error pinging Redis. {sys.exc_info()[0].__name__}: {err}")

        try:
            if self.redis:
                result_ping = self.redis.ping()
                self.bot.log.info(f"Redis connection setup. PING: {result_ping}")
        except rTimeoutError as err:
            self.bot.log.warning(f"TimeoutError pinging Redis")
        except Exception as err:
            self.bot.log.exception(f"Unknown Error pinging Redis. {sys.exc_info()[0].__name__}: {err}")

    def redis_latency(self) -> float:
        """Calculates the Redis Latency for a SET>>GET>>DEL request"""
        # Set the key to current timestamp
        REDIS_KEY = f"SWEEPERBOT_LATENCY_{self.bot.user.id}"
        self.redis.set(REDIS_KEY, f"{datetime.datetime.now(datetime.timezone.utc).timestamp()}")
        # Get the get as start time, convert to float (from string) into timestamp, TZ aware
        start_time = datetime.datetime.fromtimestamp(float(self.redis.get(REDIS_KEY)), datetime.timezone.utc)
        # Delete the key
        self.redis.delete(REDIS_KEY)
        # Record done time
        end_time = datetime.datetime.now(datetime.timezone.utc)
        # Calculate the difference
        diff = end_time - start_time
        diff_milliseconds = diff.total_seconds() * 1000
        return diff_milliseconds

    def add_to_redis_ban_cache(self, userid: int, guildid: int) -> int:
        redis_key = f"discord.gid:{guildid}.ban_cache"
        result = int(self.bot.helpers.redis.sadd(redis_key, f"{userid}"))
        return result

    def remove_from_redis_ban_cache(self, userid: int, guildid: int) -> int:
        redis_key = f"discord.gid:{guildid}.ban_cache"
        result = int(self.bot.helpers.redis.srem(redis_key, f"{userid}"))
        return result

    def is_in_redis_ban_cache(self, userid: int, guildid: int) -> bool:
        redis_key = f"discord.gid:{guildid}.ban_cache"
        result = self.bot.helpers.redis.sismember(redis_key, f"{userid}")
        return True if result else False

    async def get_member_or_user(self, input_str: str, guild: discord.Guild = None):
        # input_str could be: <@!82942340309716992> | D0cR3d#0001 | 82942340309716992
        # First, let's clean it (non-harmful) and see if we get a User ID. This searches both straight User ID and an @Mention
        user_id = self.clean_mention(input_str)
        # If no User ID, then let's assume it's a username#discrim that we need to search
        if user_id is None:
            try:
                # split by the hash symbol
                input_str_split = input_str.split("#")
                if input_str_split and len(input_str_split) == 2:
                    username = input_str_split[0]
                    discrim = input_str_split[1]
                    # This does an exact name search, case sensitive
                    tmp_member = discord.utils.get(self.bot.get_all_members(), name=username, discriminator=discrim)
                    if tmp_member:
                        user_id = tmp_member.id
                    else:
                        return None
            except Exception as err:
                # Really, not much we can do
                return None

        # Ok, assuming we have a User ID finally

        # Let's try to get a member on the server
        if guild:
            # Get user from internal cache
            member = guild.get_member(user_id)
            if member:
                return member
            else:
                # Fetch with an API call
                try:
                    member = await guild.fetch_member(user_id)
                    if member:
                        return member
                except (discord.NotFound, discord.HTTPException):
                    # No user found, pass
                    pass

        # Try to get a user from local cache first
        user = self.bot.get_user(user_id)
        if user:
            return user
        # If no user, fetch with an API call
        else:
            try:
                user = await self.bot.fetch_user(user_id)
                if user:
                    return user
            except (discord.NotFound, discord.HTTPException):
                # No user found, pass
                pass

        # If we make it this far, return None
        return None

    def clean_mention(self, mention: str) -> Optional[int]:
        """Takes a mention, and tries to return the underlying Discord ID"""

        mention_temp = mention
        if type(mention_temp) == str:
            # Clean the mention to be just an ID
            for char in ["<", "#", "&", "@", "!", ">"]:
                mention_temp = mention_temp.replace(char, "")

        try:
            return int(mention_temp)
        except ValueError:
            return None

    def relative_time(self, start_time, end_time, brief=False) -> str:
        if start_time is None or end_time is None:
            return "Unknown"
        seconds_in_year = 365 * 24 * 60 * 60  # days*hours*min*sec
        seconds_in_day = 24 * 60 * 60  # hours*min*sec
        seconds_in_hour = 60 * 60  # min*sec
        seconds_in_min = 60.0

        delta = start_time - end_time
        total_seconds = delta.total_seconds()
        # Get the years and remainder
        years, remainder = divmod(total_seconds, seconds_in_year)
        # Get the days and remainder
        days, remainder = divmod(remainder, seconds_in_day)
        # Get the hours and remainder
        hours, remainder = divmod(remainder, seconds_in_hour)
        # Get the minutes and remainder
        minutes, seconds = divmod(remainder, seconds_in_min)

        if brief:
            # Always show Days, Hours, Minutes, Seconds
            fmt = "{d:.0f}d {h:.0f}h {m:.0f}m {s:.0f}s"
            # But if there's Years, show that too
            if years:
                fmt = "{y:.0f}y " + fmt
        else:
            # Always show Days, Hours, Minutes, Seconds
            fmt = "{d:.0f} days, {h:.0f} hours, {m:.0f} minutes, and {s:.0f} seconds"
            # But if there's Years, show that too
            if years:
                fmt = "{y:.0f} years, " + fmt

        return fmt.format(y=years, d=days, h=hours, m=minutes, s=seconds)

    async def process_ban(self, session, member, mod, guild, action_timestamp, action_text):
        db_logged = False
        chan_logged = False
        try:
            # Try and log to the database
            new_ban = None
            try:
                self.add_to_redis_ban_cache(member.id, guild.id)
                # Get mod's DB profile
                db_mod = await self.bot.helpers.db_get_user(session, mod.id)
                # Get the DB profile for the guild
                db_guild = await self.bot.helpers.db_get_guild(session, guild.id)
                # Get the DB profile for the user
                db_user = await self.bot.helpers.db_get_user(session, member.id)

                # Log the action to the database
                logged_action = models.Action(mod=db_mod, server=db_guild)
                new_ban = models.Ban(
                    text_encrypted=action_text,
                    user=db_user,
                    server=db_guild,
                    action=logged_action,
                )
                session.add(new_ban)
                session.commit()
                db_logged = True
            except DBAPIError as err:
                self.bot.log.exception(
                    f"Error logging ban to database for: ({member}). {sys.exc_info()[0].__name__}: {err}"
                )
                session.rollback()
                db_logged = False

            # Try and log to the logs channel
            try:
                # Create the embed of info
                description = (
                    f"**Member:** {member} ({member.id})\n"
                    f"**Moderator:** {mod} ({mod.id})\n"
                    f"**Reason:** {action_text}"
                )

                embed = discord.Embed(
                    color=0xE50000,
                    timestamp=action_timestamp,
                    title=f"A user was banned | *#{new_ban.id if new_ban else 'n/a'}*",
                    description=description[: self.bot.constants.embed_description_limit],
                )
                embed.set_author(name=f"{member} ({member.id})", icon_url=member.display_avatar.url)
                # Get the logs channel
                all_channels = (
                    [channel for channel in guild.text_channels]
                    + [channel for channel in guild.threads]
                    + [channel for channel in guild.forums]
                )
                logs = discord.utils.get(all_channels, name="bot-logs")
                if not logs or not isinstance(logs, (discord.TextChannel, discord.Thread, discord.ForumChannel)):
                    return

                # Checks if the bot can even send messages in that channel
                if (
                    logs.permissions_for(logs.guild.me).send_messages
                    and logs.permissions_for(logs.guild.me).embed_links
                ):
                    await logs.send(embed=embed)
                    chan_logged = True
            except Exception as err:
                self.bot.log.exception(
                    f"Error logging ban in channel for user: '{member.id}' in Guild: '{guild.id}'. {sys.exc_info()[0].__name__}: {err}"
                )
        except Exception as err:
            self.bot.log.exception(
                f"Error processing ban for user: '{member.id}' in Guild: '{guild.id}'. {sys.exc_info()[0].__name__}: {err}"
            )
            raise
        finally:
            return db_logged, chan_logged

    async def process_unban(self, session, member, mod, guild, action_timestamp, action_text="None"):
        db_logged = False
        chan_logged = False
        try:
            # Try and log to the database
            new_note = None
            try:
                self.remove_from_redis_ban_cache(member.id, guild.id)
                # Get mod's DB profile
                db_mod = await self.bot.helpers.db_get_user(session, mod.id)
                # Get the DB profile for the guild
                db_guild = await self.bot.helpers.db_get_guild(session, guild.id)
                # Get the DB profile for the user
                db_user = await self.bot.helpers.db_get_user(session, member.id)

                logged_action = models.Action(mod=db_mod, server=db_guild)
                new_note = models.Note(
                    text_encrypted=f"(Unban) {action_text}",
                    user=db_user,
                    server=db_guild,
                    action=logged_action,
                )
                session.add(new_note)
                session.commit()
                db_logged = True
            except DBAPIError as err:
                self.bot.log.exception(
                    f"Error logging unban to database for: ({member}). {sys.exc_info()[0].__name__}: {err}"
                )
                session.rollback()
                db_logged = False

            # Create the embed of info
            description = (
                f"**Member:** {member} ({member.id})\n"
                f"**Moderator:** {mod} ({mod.id})\n"
                f"**Reason:** {action_text}"
            )

            embed = discord.Embed(
                color=0xBDBDBD,
                timestamp=action_timestamp,
                title=f"A users ban was removed | *#{new_note.id if new_note else 'n/a'}*",
                description=description[: self.bot.constants.embed_description_limit],
            )
            embed.set_author(name=f"{member} ({member.id})", icon_url=member.display_avatar.url)
            # Get the logs channel
            all_channels = (
                [channel for channel in guild.text_channels]
                + [channel for channel in guild.threads]
                + [channel for channel in guild.forums]
            )
            logs = discord.utils.get(all_channels, name="bot-logs")
            if not logs or not isinstance(logs, (discord.TextChannel, discord.Thread, discord.ForumChannel)):
                return

            # Checks if the bot can even send messages in that channel
            if logs.permissions_for(logs.guild.me).send_messages and logs.permissions_for(logs.guild.me).embed_links:
                await logs.send(embed=embed)
                chan_logged = True

        except Exception as err:
            self.bot.log.exception(
                f"Error processing ban for user: '{member.id}' in Guild: '{guild.id}'. {sys.exc_info()[0].__name__}: {err}"
            )
            raise
        finally:
            return db_logged, chan_logged

    def normal_name(self, input_text) -> Union[str, bytes]:
        return re.sub(self.bot.constants.normalize_regex, "", input_text.lower())

    async def get_voice_channel_members(self, member, members_list, include_self):
        # Create a mention list for the embed
        tmp_members_mention = [tmp_user.mention for tmp_user in members_list]
        if include_self:
            tmp_members_mention.append(member.mention)
        channel_members = ", ".join(tmp_members_mention)
        # Create a id list for the database logging
        channel_members_ids = [tmp_user.id for tmp_user in members_list]
        if include_self:
            channel_members_ids.append(member.id)
        # Return the data
        return channel_members, channel_members_ids

    async def get_guild_invite(self, guild):
        invite = None
        try:
            all_invites = await guild.invites()
            # If the server doesn't have any invites, let's try and create one
            if all_invites is None:
                # Let's try and create one.. by iterating through every channel until we find the perms
                for channel in guild.text_channels:
                    try:
                        # Create invite to last 60 seconds, we don't need it long.
                        tmp_invite = await channel.create_invite(max_age=60, reason="Getting Guild Stats")
                        # Now we need to fetch it so we get counts
                        invite = await self.bot.fetch_invite(tmp_invite.code, with_counts=True)
                        # Once we find a good candidate then break out
                        break
                    except Exception:
                        pass

            else:
                # If the server has an invite, let's use that instead
                for tmp_invite in all_invites:
                    try:
                        invite = await self.bot.fetch_invite(tmp_invite.code, with_counts=True)
                        break
                    except discord.HTTPException:
                        # Discord HTTP Error, possible outage
                        pass
        except discord.errors.HTTPException:
            pass
        except asyncio.TimeoutError as err:
            self.bot.log.warning(f"TimeoutError getting Guild Invite. {sys.exc_info()[0].__name__}: {err}")
        except Exception as err:
            self.bot.log.exception(f"Generic Error getting Guild Invite. {sys.exc_info()[0].__name__}: {err}")
        finally:
            return invite

    # Database Functions
    def get_db_session(self, db_config="Database"):
        try:
            return self.bot.database.get_session(db_config)
        except Exception as err:
            self.bot.log.exception(f"Error getting session. Error: {err}")
            return None

    def get_all_guild_settings(self) -> None:
        session = self.get_db_session()
        try:
            self.bot.log.info(f"Getting all guild settings")
            # Get all guild settings where the bot is in that guild
            guild_settings = session.query(models.ServerSetting).filter().all()

            all_settings = {}
            # TO DO - This could probably be combined with above query using a join
            if guild_settings:
                for guild in guild_settings:
                    guild_id = (
                        session.query(models.Server).filter(models.Server.id == guild.server_id).first()
                    ).discord_id

                    all_settings[guild_id] = guild

            self.bot.guild_settings = all_settings
            self.bot.log.info(f"Done getting all guild settings")
        except Exception as err:
            self.bot.log.exception(f"Error getting all guild settings. {sys.exc_info()[0].__name__}: {err}")
            session.rollback()
        finally:
            session.close()

    def db_get_cooldown_settings(self) -> None:
        session = self.get_db_session()
        try:
            self.bot.log.info(f"Getting all cooldown settings")
            # Get all guild settings where the bot is in that guild
            cooldown_settings = session.query(models.Cooldowns).filter().all()

            all_settings = {}
            for setting in cooldown_settings:
                all_settings[setting.name] = setting

            self.bot.cooldown_settings = all_settings
            self.bot.log.info(f"Done getting all cooldown settings")
        except Exception as err:
            self.bot.log.exception(f"Error getting all cooldown settings. {sys.exc_info()[0].__name__}: {err}")
            session.rollback()
        finally:
            session.close()

    async def get_one_guild_settings(self, session, guild_id):
        try:
            # Get guild settings
            guild_settings = (
                session.query(models.ServerSetting)
                .join(models.Server)
                .filter(models.Server.discord_id == guild_id)
                .first()
            )
            if not guild_settings:
                # Check if there is a guild in the database already
                guild = session.query(models.Server).filter(models.Server.discord_id == guild_id).first()
                # If not, add one
                if not guild:
                    guild = await self.db_add_new_guild(session, guild_id)
                # Now that we have a guild, let's finally create the settings
                guild_settings = await self.db_add_new_guild_settings(session, guild)

            return guild_settings
        except Exception as err:
            self.bot.log.exception(f"Error getting guild setting for {guild_id}. {sys.exc_info()[0].__name__}: {err}")
            session.rollback()

    async def db_add_new_guild(self, session, guild_id, guild=None):
        try:
            if guild:
                new_guild = models.Server(discord_id=guild.id, name=guild.name)
            else:
                new_guild = models.Server(discord_id=guild_id)
            session.add(new_guild)
            session.commit()
            self.bot.log.debug(f"Added new guild to DB: {guild_id}")
            return new_guild
        except Exception as err:
            self.bot.log.exception(
                f"Error adding new guild '{guild_id}' to database. {sys.exc_info()[0].__name__}: {err}"
            )
            session.rollback()

    async def db_add_new_guild_settings(self, session, guild):
        try:
            new_settings = models.ServerSetting(server=guild)
            session.add(new_settings)
            session.commit()
            self.bot.log.debug(f"Added new guild setting to DB for guild: {guild.discord_id}")
            return new_settings
        except Exception as err:
            self.bot.log.exception(
                f"Error adding new guild settings '{guild.discord_id}' to database. {sys.exc_info()[0].__name__}: {err}"
            )
            session.rollback()
            raise

    async def db_get_user(self, session, discord_id):
        try:
            # Try and get record from database
            db_user = session.query(models.User).filter(models.User.discord_id == discord_id).first()
            # If no DB record for the user then create one
            if not db_user:
                db_user = models.User(discord_id=discord_id)
                session.add(db_user)
                session.commit()
                self.bot.log.debug(f"Added new user to DB: {discord_id}")
            return db_user
        except (DBAPIError, IntegrityError) as err:
            self.bot.log.exception(
                f"Database Error getting / adding user to database. {sys.exc_info()[0].__name__}: {err}"
            )
            session.rollback()
        except Exception as err:
            self.bot.log.exception(
                f"Generic Error getting / adding user to database. {sys.exc_info()[0].__name__}: {err}"
            )

    async def db_get_guild(self, session, discord_id):
        try:
            # Try and get record from database
            db_guild = session.query(models.Server).filter(models.Server.discord_id == discord_id).first()
            # If no DB record for the guild, create one
            if not db_guild:
                self.bot.log.debug(f"No guild in DB found for {discord_id} - creating a new record")
                db_guild = await self.bot.helpers.db_add_new_guild(session, discord_id)
            return db_guild
        except Exception as err:
            self.bot.log.exception(
                f"Error getting / adding guild to database: '{discord_id}'. {sys.exc_info()[0].__name__}: {err}"
            )

    async def get_action_history(self, session, user, guild):
        try:
            # Creates an alias to the User table specifically to be used for the user.
            # Otherwise using models.User.discord_id == user.id will match against the Mod
            # due to prior join to the User table
            user_alias = aliased(models.User)

            # Note Query
            note_query = (
                session.query(
                    models.Note.id,
                    models.Note.created.label("created"),
                    models.Note.text_encrypted,
                    models.User,
                    literal_column("'Note'").label("type"),
                    models.Note.created.label("expires"),
                )
                # Links the Note and Action table (used for next 2 joins)
                .join(models.Action, models.Action.id == models.Note.action_id)
                # Links the User and Action table to get the Mod
                .join(models.User, models.User.id == models.Action.mod_id)
                # Links the Server and Note table to get the Guild
                .join(models.Server, models.Server.id == models.Note.server_id)
                # Links the User and Note table to get the User
                .join(user_alias, user_alias.id == models.Note.user_id)
                # Filters on the user alias where the users Discord ID matches
                # And where the guilds Discord ID matches
                .filter(
                    user_alias.discord_id == user.id,
                    models.Server.discord_id == guild.id,
                )
            )
            # Mute Query
            mute_query = (
                session.query(
                    models.Mute.id,
                    models.Mute.created.label("created"),
                    models.Mute.text_encrypted,
                    models.User,
                    literal_column("'Mute'").label("type"),
                    models.Mute.expires,
                )
                # Links the Mute and Action table (used for next 2 joins)
                .join(models.Action, models.Action.id == models.Mute.action_id)
                # Links the User and Action table to get the Mod
                .join(models.User, models.User.id == models.Action.mod_id)
                # Links the Server and Mute table to get the Guild
                .join(models.Server, models.Server.id == models.Mute.server_id)
                # Links the User and Mute table to get the User
                .join(user_alias, user_alias.id == models.Mute.user_id)
                # Filters on the user alias where the users Discord ID matches
                # And where the guilds Discord ID matches
                .filter(
                    user_alias.discord_id == user.id,
                    models.Server.discord_id == guild.id,
                )
            )
            # Warn Query
            warn_query = (
                session.query(
                    models.Warn.id,
                    models.Warn.created.label("created"),
                    models.Warn.text_encrypted,
                    models.User,
                    literal_column("'Warn'").label("type"),
                    models.Warn.created.label("expires"),
                )
                # Links the Warn and Action table (used for next 2 joins)
                .join(models.Action, models.Action.id == models.Warn.action_id)
                # Links the User and Action table to get the Mod
                .join(models.User, models.User.id == models.Action.mod_id)
                # Links the Server and Warn table to get the Guild
                .join(models.Server, models.Server.id == models.Warn.server_id)
                # Links the User and Warn table to get the User
                .join(user_alias, user_alias.id == models.Warn.user_id)
                # Filters on the user alias where the users Discord ID matches
                # And where the guilds Discord ID matches
                .filter(
                    user_alias.discord_id == user.id,
                    models.Server.discord_id == guild.id,
                )
            )
            # Warn Query
            kick_query = (
                session.query(
                    models.Kick.id,
                    models.Kick.created.label("created"),
                    models.Kick.text_encrypted,
                    models.User,
                    literal_column("'Kick'").label("type"),
                    models.Kick.created.label("expires"),
                )
                # Links the Kick and Action table (used for next 2 joins)
                .join(models.Action, models.Action.id == models.Kick.action_id)
                # Links the User and Action table to get the Mod
                .join(models.User, models.User.id == models.Action.mod_id)
                # Links the Server and Kick table to get the Guild
                .join(models.Server, models.Server.id == models.Kick.server_id)
                # Links the User and Kick table to get the User
                .join(user_alias, user_alias.id == models.Kick.user_id)
                # Filters on the user alias where the users Discord ID matches
                # And where the guilds Discord ID matches
                .filter(
                    user_alias.discord_id == user.id,
                    models.Server.discord_id == guild.id,
                )
            )
            # Ban Query
            ban_query = (
                session.query(
                    models.Ban.id,
                    models.Ban.created.label("created"),
                    models.Ban.text_encrypted,
                    models.User,
                    literal_column("'Ban'").label("type"),
                    models.Ban.created.label("expires"),
                )
                # Links the Ban and Action table (used for next 2 joins)
                .join(models.Action, models.Action.id == models.Ban.action_id)
                # Links the User and Action table to get the Mod
                .join(models.User, models.User.id == models.Action.mod_id)
                # Links the Server and Ban table to get the Guild
                .join(models.Server, models.Server.id == models.Ban.server_id)
                # Links the User and Ban table to get the User
                .join(user_alias, user_alias.id == models.Ban.user_id)
                # Filters on the user alias where the users Discord ID matches
                # And where the guilds Discord ID matches
                .filter(
                    user_alias.discord_id == user.id,
                    models.Server.discord_id == guild.id,
                )
            )
            # Get the results from the database
            results = note_query.union(warn_query, kick_query, mute_query, ban_query).order_by(desc("created"))
            # Format the results
            note_count = 0
            warn_count = 0
            mute_count = 0
            kick_count = 0
            ban_count = 0
            total_count = 0
            embed_result_entries = []
            for result in results:
                total_count += 1
                # Set variables to make readability easier
                action_dbid = result[0]
                action_date = result[1]
                action_date_friendly = result[1].strftime("%b %d, %Y %I:%M %p") + " UTC"
                action_text = result[2]
                action_mod = result[3]
                action_type = result[4]
                action_expires = result[5]
                action_length_friendly = ""
                # Count how many of each action
                if action_type == "Note":
                    note_count += 1
                elif action_type == "Warn":
                    warn_count += 1
                elif action_type == "Kick":
                    kick_count += 1
                elif action_type == "Mute":
                    mute_count += 1
                    action_length = self.bot.helpers.relative_time(
                        start_time=action_expires, end_time=action_date, brief=True
                    )
                    action_length_friendly = f" [{action_length}]"
                elif action_type == "Ban":
                    ban_count += 1

                # Truncate the embed action text to avoid long messages
                if action_text and len(action_text) > 750:
                    action_text = f"{action_text[:750]}..."
                # Format the embed
                data_title = f"{action_type}{action_length_friendly} - {action_date_friendly} | *#{action_dbid}*"
                data_value = f"{action_text} - <@{action_mod.discord_id}>"

                embed_result_entries.append([data_title, data_value])
            # Handle if there were no records returned so the users know there's nothing vs a bot error
            if total_count == 0:
                embed_result_entries.append(["History Data:", "None"])
            # Set the footer
            footer_text = (
                f"Note: {note_count}, Warn: {warn_count}, Mute: {mute_count}, Kick: {kick_count}, Ban: {ban_count}"
            )
            # Return the results
            return embed_result_entries, footer_text
        except Exception as err:
            self.bot.log.exception(
                f"Error getting history from database for User: '{user.id}' in Guild: '{guild.id}'. {sys.exc_info()[0].__name__}: {err}"
            )
            raise

    async def db_process_admin_relationship(self, member, session, server_admin):
        # Get the DB profile for the guild
        db_guild = await self.bot.helpers.db_get_guild(session, member.guild.id)
        # Get the DB profile for the user
        db_user = await self.bot.helpers.db_get_user(session, member.id)
        # If server_admin is true, then we need to add a relationship
        if server_admin:
            db_serveradminrels = models.ServerAdminRels(user=db_user, server=db_guild)
            session.add(db_serveradminrels)
            self.bot.log.debug(
                f"Server Admin Relationship Added to database: Guild: {member.guild.id}, User: {member.id}"
            )
        # Otherwise if it's false we need to remove the relationship
        else:
            session.query(models.ServerAdminRels).filter(
                models.ServerAdminRels.server_id == db_guild.id,
                models.ServerAdminRels.user_id == db_user.id,
            ).delete(synchronize_session=False)
            self.bot.log.debug(f"Removed admin relationship for: Guild: {member.guild.id}, User: {member.id}")

    async def db_process_mod_relationship(self, member, session, server_mod):
        # Get the DB profile for the guild
        db_guild = await self.bot.helpers.db_get_guild(session, member.guild.id)
        # Get the DB profile for the user
        db_user = await self.bot.helpers.db_get_user(session, member.id)
        # If server_mod is true, then we need to add a relationship
        if server_mod:
            db_serverrels = models.ServerModRels(user=db_user, server=db_guild)
            session.add(db_serverrels)
            self.bot.log.debug(
                f"Server Mod Relationship Added to database: Guild: {member.guild.id}, User: {member.id}"
            )
        # Otherwise if it's false we need to remove the relationship
        else:
            session.query(models.ServerModRels).filter(
                models.ServerModRels.server_id == db_guild.id,
                models.ServerModRels.user_id == db_user.id,
            ).delete(synchronize_session=False)
            self.bot.log.debug(f"Removed mod relationship for: Guild: {member.guild.id}, User: {member.id}")

    async def check_if_blacklisted(self, user_id: int, guild_id: int) -> bool:
        session = self.bot.helpers.get_db_session()
        try:
            # Get the DB ID for the user
            db_user = await self.bot.helpers.db_get_user(session, user_id)
            # Get the DB ID for the guild
            db_guild = await self.bot.helpers.db_get_guild(session, guild_id)
            # Check if user is blacklisted
            user_status = (
                session.query(models.Blacklist)
                .filter(models.Blacklist.server_id == db_guild.id)
                .filter(models.Blacklist.user_id == db_user.id)
                .first()
            )

            self.bot.log.debug(
                f"Blacklist for DiscoID: {user_id}: DBID: {user_status.user_id if user_status else None} | BLStatus: {user_status.blacklisted if user_status else None}"
            )
            if not user_status or user_status.blacklisted is False:
                self.bot.log.debug(f"Blacklist for {user_id}: Allowed")
                return False
            else:
                self.bot.log.debug(f"Blacklist for {user_id}: Denied")
                return True

        except DBAPIError as err:
            self.bot.log.exception(
                f"Error processing database query for getting blacklist info. {sys.exc_info()[0].__name__}: {err}"
            )
            session.rollback()
            self.bot.log.debug(f"Blacklist for {user_id} - using default: Allowed")
            return False
        except Exception as err:
            self.bot.log.exception(f"Unknown exception getting blacklist info. {sys.exc_info()[0].__name__}: {err}")
            self.bot.log.debug(f"Blacklist for {user_id} - using default: Allowed")
            return False
        finally:
            session.close()

    async def check_autopublish(self, msg) -> None:
        try:
            # While a Guild and Channel should be set, let's make sure
            if msg.guild is None or msg.channel is None:
                return
            # Get the setting type, and the setting value. Returns None if not set
            autopub_option = self.redis.get(
                f"discord.settings.autopub.gid:{msg.guild.id}.cid:{msg.channel.id}.setting_options"
            )
            if autopub_option:
                autopub_option = autopub_option.lower()
            autopub_value = self.redis.get(f"discord.settings.autopub.gid:{msg.guild.id}.cid:{msg.channel.id}.value_id")
            # If the option is None, return
            if autopub_option is None:
                return

            # Now check if the message is eligible for publishing
            # Set the publish flag to False by default
            publish_msg = False
            # If disabled, move on
            if autopub_option in ["disable", "disabled"]:
                publish_msg = False
            # Publish every message
            elif autopub_option in ["any", "all"]:
                publish_msg = True
            # Only if the user ID is specific
            elif autopub_option == "specific_id" and autopub_value:
                if msg.author.id == int(autopub_value):
                    publish_msg = True
            # Only if the user/bot has a specific role
            elif autopub_option == "specific_role" and autopub_value:
                if int(autopub_value) in [role.id for role in msg.author.roles if msg.author.roles]:
                    publish_msg = True
            # Only if it's a human aka not a bot or webhook
            elif autopub_option == "all_human":
                if not msg.author.bot:
                    publish_msg = True
            # Only if it's a bot or a webhook
            elif autopub_option == "all_bot":
                if msg.author.bot:
                    publish_msg = True

            # Try to actually publish the message
            try:
                if publish_msg:
                    await msg.publish()
                    self.bot.log.debug(f"AutoPublished message: {msg.id}")
            except discord.Forbidden:
                self.bot.log.warning(
                    f"AutoPublish: Missing permissions for Guild: {msg.guild.id} | Channel: {msg.channel.id}"
                )
            except Exception as err:
                self.bot.log.exception(f"Unknown exception trying to AutoPublish. {sys.exc_info()[0].__name__}: {err}")

        except Exception as err:
            self.bot.log.exception(
                f"Unknown exception performing AutoPublish check. {sys.exc_info()[0].__name__}: {err}"
            )

    async def redis_clean_most_freq_used_and_channel_key(self, redis_channel_key) -> None:
        try:
            self.redis.delete(redis_channel_key)
            # And also delete from the most_freq_used key
            most_freq_used_key_name = f"discord.messages.channels.most_freq_used"
            # First is the name of the Redis Key, Second is the member of the set to remove
            self.redis.zrem(most_freq_used_key_name, redis_channel_key)
        except Exception as err:
            self.bot.log.exception(
                f"Unknown exception performing redis_clean_most_freq_used_and_channel_key. {sys.exc_info()[0].__name__}: {err}"
            )

    async def activityrole_adjust_role(
        self,
        member: discord.Member,
        action: str,
        role_id: int,
    ):
        try:
            # Get the role from the ID
            role = member.guild.get_role(role_id)
            # If we have a role, either add or remove it
            if role:
                if action == "add" and role not in member.roles:
                    await member.add_roles(role)
                elif action == "remove" and role in member.roles:
                    await member.remove_roles(role)
        except Exception as err:
            # Nothing we can do, just ignore the error
            self.bot.log.exception(f"{sys.exc_info()[0].__name__}: {err}")
            pass

    async def load_reminders(self):
        # Don't load reminders again..
        if self.reminds_already_loaded:
            return
        # Actually load since this is first time
        session = self.bot.helpers.get_db_session()
        try:
            reminders = (
                session.query(
                    func.coalesce(models.Reminder.updated, models.Reminder.created).label("created"),
                    models.Reminder.id,
                    models.Reminder.creator_user_id,
                    models.User,
                    models.Server,
                    models.Reminder.expires,
                    models.Reminder.text_encrypted,
                )
                .join(models.Server, models.Server.id == models.Reminder.server_id)
                .join(models.User, models.User.id == models.Reminder.remind_user_id)
                .filter(models.Reminder.expires > datetime.datetime.now(datetime.timezone.utc))
                .all()
            )

            counter = 0
            temp_guild_list = [tmp_guild.id for tmp_guild in self.bot.guilds]
            for item in reminders:
                # Filter out reminders where the bot isn't in them
                if item.Server.discord_id in temp_guild_list:
                    counter += 1
                    # Add timer to send the reminder
                    timer = Timer.temporary(
                        item.Server.discord_id,
                        item.User.discord_id,
                        item.creator_user_id,
                        item.text_encrypted,
                        event=self._remind,
                        expires=item.expires,
                        created=item.created,
                    )
                    timer.start(self.bot.loop)

            # Log number of reminders loaded
            self.bot.log.info(
                f"Helpers-Reminders: Loaded {counter} reminders | Bot " f"Startup: {self.bot.started_time.timestamp()}"
            )

        except DBAPIError as err:
            self.bot.log.exception(
                f"Error processing database query loading reminders. {sys.exc_info()[0].__name__}: {err}"
            )
            session.rollback()
        except Exception as err:
            self.bot.log.exception(f"Error loading reminders. {sys.exc_info()[0].__name__}: {err}")
        finally:
            self.reminds_already_loaded = True
            session.close()

    def _remind(self, guild_id: int, remind_user_id: int, creator_user_id: int, remind_text: str):

        try:
            guild = self.bot.get_guild(guild_id)
            if not guild:
                raise ValueError(f"Bot can't find guild {guild_id}")
            member = guild.get_member(remind_user_id)
            if not member:
                raise ValueError(f"Bot can't find member {remind_user_id} in guild {guild} ({guild.id})")
            creator_user = self.bot.get_user(creator_user_id)
        except ValueError:
            self.bot.log.exception(f"Can't execute reminder")
            raise

        settings = self.bot.guild_settings.get(member.guild.id)
        has_modmail_server = settings.modmail_server_id

        footer_text = (
            self.bot.constants.footer_with_modmail.format(guild=member.guild)
            if has_modmail_server
            else self.bot.constants.footer_no_modmail.format(guild=member.guild)
        )

        try:
            self.bot.loop.create_task(
                member.send(
                    f"Here is the reminder requested by **{creator_user}** from the **{guild}** server.\n\n'{remind_text}'{footer_text}"
                )
            )

            self.bot.log.info(f"Sent reminder to: {member} ({member.id}) in guild {member.guild}")
        except Exception as e:
            if not (type(e) == discord.errors.Forbidden and e.code == 50007):
                self.bot.log.exception(
                    f"There was an error while informing {member} ({member.id}) about their reminder. Unable to send messages to this user."
                )
            return False

        return True

    async def redis_set_sticky_role(self, member: discord.Member, roles: list, guild: discord.Guild):
        redis_key = f"discord.stickyroles.gid:{guild.id}.mid:{member.id}"

        # Going to expire the key if the user has not been in the server for 1 year
        # This'll help keep excess keys clean that aren't used in a while
        # Redis Expire format: seconds = seconds * minutes * hours * days * months
        redis_expire_time = 60 * 60 * 24 * 30 * 12
        # Create a pipeline to batch the requests
        pipeline = self.redis.pipeline()
        # Add the user message to the channel key
        pipeline.set(name=channel_key, mapping={user_msg_value: redis_score_epoch})
        # Set key expiration
        pipeline.expire(channel_key, redis_expire_time)

        # Now let's update the Most Frequently Used sorted set so we know what has the most values in the key
        # so we can target those for cleaning
        most_freq_used_key = f"discord.messages.channels.most_freq_used"
        redis_score_incr = 1
        # Increase the message count in most_freq_used key
        pipeline.zadd(
            name=most_freq_used_key,
            mapping={channel_key: redis_score_incr},
            incr=True,
        )
        # All done, execute the pipeline
        results = pipeline.execute()

    async def check_if_command_blocked(self, ctx) -> bool:
        """Checks if a command is blocked in the channel and guild. Default is False (allowed) and Allowed overrides Blocked"""
        guild = ctx.message.guild
        channel = ctx.message.channel
        command = ctx.command

        command_name = str(command).split(" ")[0]
        # Is a set of channel ID's that the command is NOT allowed to run in, grouped by command name
        blocked_key = f"discord.command_mgmt.blocked.gid:{guild.id}.cmd_name:{command_name.lower()}"
        allowed_key = f"discord.command_mgmt.allowed.gid:{guild.id}.cmd_name:{command_name.lower()}"

        is_blocked = self.redis.sismember(blocked_key, f"{channel.id}")
        # If it's blocked, now check if it's allowed to override the block
        if is_blocked:
            is_allowed = self.redis.sismember(allowed_key, f"{channel.id}")
            if is_allowed:
                # It's in allow list
                return False
            else:
                # It's in the block list, attempt to delete the calling message
                try:
                    if ctx.interaction is None:
                        await ctx.message.delete()
                # Don't care about any errors, nothing we can do
                except Exception as err:
                    pass
                # Regardless, return that it's blocked
                finally:
                    return True
        # Otherwise return, nothing else to check
        else:
            return False

    async def clean_guild_invites(
        self,
        guild,
        less_than_eq_uses,
        older_than_hours,
        only_temporary,
        total_invites_above,
        ctx=None,
    ):
        count_cleaned = 0
        count_exceptions = 0
        now_dt = datetime.datetime.now(datetime.timezone.utc)
        older_than_hours_dt = now_dt - datetime.timedelta(hours=older_than_hours)
        # Get all invites
        invites = await guild.invites()
        if invites:
            if ctx:
                await ctx.reply(f"Starting invite pruning.")
            # If number of guild invites is less than total_invites_above, then skip cleaning
            total_guild_invites = len(invites)
            if total_guild_invites < total_invites_above:
                if ctx:
                    await ctx.reply(
                        f"Skipping cleaning, total guild invites: {total_guild_invites}, which is less than the limit of {total_invites_above} to clean."
                    )
                return
            for inv in invites:
                try:
                    # If the invite has less than or equal to uses, or older than x hours
                    if inv.uses <= less_than_eq_uses and inv.created_at < older_than_hours_dt:
                        # If removing only temporary is false (so remove all)
                        # OR only temporary is True AND the invite is temporary
                        if only_temporary is False or (only_temporary is True and inv.temporary is True):
                            await inv.delete(reason=f"Pruning | Uses: {inv.uses} | CreDat: {inv.created_at}")
                    count_cleaned += 1
                except Exception as err:
                    count_exceptions += 1
                    # If we have more than 10 exceptions, it could be something major
                    if count_exceptions > 10:
                        # so let's log it
                        self.bot.log.exception(
                            f"Excessive exceptions purging invites. {sys.exc_info()[0].__name__}: {err}"
                        )
                        # Let the mods know
                        if ctx:
                            await ctx.reply(
                                f"We've reached an excessive number of errors purging invites, so we're stopping the purge.\n\nPossible causes include: Discord Outage, the invites being deleted prior to the bot removing them, or an unknown issue.\n\nYou can try again, but if this keeps happening, please be patient.",
                                mention_author=True,
                            )
                        # and let's break out
                        break

            self.bot.log.debug(f"Completed purging invites for ({guild.id}). Removed {count_cleaned:,}")
            if ctx:
                await ctx.send(f"Completed purging invites. Removed {count_cleaned:,}")

    async def upload_image(self, file_input, tags=None, context=None):
        image = Image()
        try:
            if isinstance(file_input, discord.Asset):
                file = await file_input.read()
                filename = file_input.url.split("/")[-1].split("?")[0]
                async with aiohttp.ClientSession() as session:
                    data = aiohttp.FormData()
                    data.add_field("myFile", file, filename=f"{filename}", content_type="multipart/form-data")
                    async with session.post("https://uploads.layer7.solutions/upload", data=data) as resp:
                        if 200 <= resp.status < 300:
                            data = await resp.json()
                            image = Image(
                                public_id=data["filename"],
                                secure_url=data["uri"],
                                size=int(data["size"]),
                            )
                            self.bot.log.debug(f"Image Uploader - Orig File_Input: {file_input} | Image Data: {image}")
                        else:
                            self.bot.log.exception(
                                f"Image Uploader: Resp Status: {resp.status} | Resp: {await resp.text()}"
                            )
        except discord.NotFound:
            # File not found, purged from CDN cache
            pass
        except Exception as err:
            self.bot.log.exception(
                f"Image Uploader - Orig File_Input: {file_input} | {sys.exc_info()[0].__name__}: {err}"
            )
        finally:
            return image

    async def CheckIfVCBanned(self, member: discord.Member):
        result = False
        try:
            result = self.CheckIfAutoVCBanned(member.id)
            if result is True:
                self.bot.log.warning(f"User is AutoVC Banned: {member} ({member.id})")
                # Tell user they can't use the service
                try:
                    await member.send(
                        f"You are not permitted to use the AutoVC Creation Service due to being blacklisted for abusing the service."
                    )
                except (discord.Forbidden):
                    pass

                # Kick from VC
                try:
                    await member.move_to(channel=None, reason="Global Banned From Using AutoVC Service; Kicking")
                except Exception as err:
                    pass
        except Exception as err:
            self.bot.log.warning(f"Exception checking if user is AutoVC Banned: {err}")
        finally:
            return result

    async def CreateVC(self, member: discord.Member, voice_state: discord.VoiceState):
        try:
            channel = voice_state.channel
            # Sweeper Dev, GTFO, Project Breach server, GTFO-BH
            if channel.guild.id in [305133671776649216, 408196129470152705, 872128969938452540, 635874586365526026]:
                # If user joined the "create team" channel
                if channel.name.lower().startswith("create team"):
                    # Check if AutoVC Baned, and if so, skip
                    result = await self.CheckIfVCBanned(member)
                    if result is True:
                        return

                    # Check if user has exceeded the cooldown
                    # 1. Create a generic fake message object
                    gen_msg = discord.Object(id=member.id, type=discord.Message)
                    # 1.5. Set the fake msg obj to include the member/author
                    gen_msg.author = member
                    # 2. Pass the gen msg to check the cooldown
                    retry_after = self.cd_AutoVC.update_rate_limit(gen_msg)

                    # 3. Check if ratelimit exceeded
                    if retry_after:
                        self.bot.log.warning(
                            f"AutoVC Ratelimit Exceeded for {member} ({member.id}) | {self.cd_AutoVC._cooldown.__repr__()}"
                        )
                        # Kick from VC
                        try:
                            await member.move_to(channel=None, reason="Exceeded AutoVC Ratelimit; Kicking")
                        except Exception as err:
                            pass
                        # Tell user they have exceeded the ratelimit
                        try:
                            await member.send(
                                f"You have exceeded the number of Auto VC's you can create. Please wait {math.ceil(retry_after)} seconds."
                            )
                        except Exception as err:
                            pass
                        finally:
                            return

                    # Check if bot has permissions
                    if channel.permissions_for(channel.guild.me).manage_channels:
                        # Check if this category is at the max channel limit
                        if len(channel.category.channels) == 50:
                            await member.move_to(channel=None, reason="Max Channel Limit Reached in Cat; Kicking")
                            try:
                                await member.send(
                                    f"Sorry for the inconvenience. This bot is unable to create more Voice Channels in the 'Create Team' category. As a result you have been kicked from the 'Create Team' channel. Please use another 'Create Team' category, or let the server staff know."
                                )
                            except Exception as err:
                                pass
                        else:
                            max_users: int = channel.user_limit

                            numb: int = await self.GetRandomTeamNumber(1, 999, channel.guild)
                            name: str = f"Team #{numb}"
                            # Clone the channel
                            new_channel: discord.VoiceChannel = await channel.clone(name=name)
                            # Sync permissions with Category
                            await new_channel.edit(sync_permissions=True)
                            self.AddAutoVCTeamNumbRedis(channel.guild, numb)
                            self.bot.log.info(f"AutoVC: Created VC '{new_channel.name}' ({new_channel.id})")
                            # Move the user to the new channel
                            try:
                                await member.move_to(new_channel)
                                self.bot.log.info(
                                    f"AutoVC: Moved {member.name} ({member.id}) to '{new_channel.name}' ({new_channel.id})"
                                )
                            except Exception as err:
                                self.bot.log.exception(f"AutoVC:Exception moving user: {err}")

        except discord.HTTPException as err:
            self.bot.log.exception(f"Overall discord HTTPException creating VC: {err}")
        except Exception as err:
            self.bot.log.exception(f"Exception creating VC: {err}")

    async def GetRandomTeamNumber(self, start: int, end: int, guild: discord.Guild) -> int:
        numb: int = 0
        while True:
            numb: int = self.sysrand.randint(start, end)
            result = self.CheckIfAutoVCTeamNumbExistsRedis(guild, numb)
            if not result:
                self.bot.log.debug(f"Found unused team number: {numb}")
                break
            else:
                self.bot.log.debug(f"Team number already in use: {numb}, searching for another.")

        return numb

    def CheckIfAutoVCTeamNumbExistsRedis(self, guild: discord.Guild, numb: int) -> bool:
        key = f"discord.AutoVC.gid:{guild.id}.TeamNumb"
        result = self.redis.sismember(key, numb)
        return result

    def AddAutoVCTeamNumbRedis(self, guild: discord.Guild, numb: int):
        key = f"discord.AutoVC.gid:{guild.id}.TeamNumb"
        result = self.redis.sadd(key, numb)

    def RemoveAutoVCTeamNumbRedis(self, guild: discord.Guild, numb: int):
        key = f"discord.AutoVC.gid:{guild.id}.TeamNumb"
        result = self.redis.srem(key, numb)

    async def RemoveAutoVC(self, channel: discord.VoiceChannel):
        try:
            # Sweeper Dev, GTFO, Project Breach server, GTFO-BH
            if channel.guild.id in [305133671776649216, 408196129470152705, 872128969938452540, 635874586365526026]:
                # If it's an AutoVC channel
                if channel.name.lower().startswith("team #"):
                    # Check if this category is at the max channel limit
                    if channel.members is not None and int(len(channel.members)) == 0:
                        if channel.permissions_for(channel.guild.me).manage_channels:
                            delta = datetime.datetime.now(tz=datetime.timezone.utc) - channel.created_at
                            # Only delete if channel is older than 60 seconds.
                            # This prevents race condition where auto cleanup tries to delete when someone just created it
                            if delta.total_seconds() > 30:
                                tmp = channel.name.lower().split("team #")
                                numb = int(tmp[1])
                                await channel.delete()
                                self.RemoveAutoVCTeamNumbRedis(channel.guild, numb)
                                self.bot.log.info(f"AutoVC: Deleted empty VC '{channel.name}' ({channel.id})")

        except discord.Forbidden:
            pass
        except discord.NotFound:
            self.bot.log.debug("VC channel not found, unable to delete (probably already deleted)")
        except Exception as err:
            self.bot.log.exception(f"Exception deleting VC: {err}")

    def CheckIfAutoVCBanned(self, member_id: int) -> bool:
        key = f"discord.AutoVC.GlobalBlacklist"
        result = self.redis.sismember(key, member_id)
        return result

    def AddAutoVCBan(self, member_id: int) -> int:
        result = 0
        if not self.CheckIfAutoVCBanned(member_id):
            key = f"discord.AutoVC.GlobalBlacklist"
            result = self.redis.sadd(key, member_id)
        return result

    def RemoveAutoVCBan(self, member_id: int) -> int:
        result = 0
        if self.CheckIfAutoVCBanned(member_id):
            key = f"discord.AutoVC.GlobalBlacklist"
            result = self.redis.srem(key, member_id)
        return result


def has_guild_permissions(**perms):
    def predicate(ctx):
        author = ctx.message.author
        permissions = author.guild_permissions

        missing = [perm for perm, value in perms.items() if getattr(permissions, perm, None) != value]

        if not missing:
            return True

        raise commands.MissingPermissions(missing)

    return commands.check(predicate)


def set_sentry_scope(ctx):
    with configure_scope() as scope:
        scope.set_extra("guild_id", f"{ctx.message.guild.id if ctx.message.guild else None}")
        scope.set_extra("message_id", f"{ctx.message.id if ctx.message else None}")
        scope.set_extra("command", f"{ctx.command if ctx.command else None}")
        scope.user = {
            "id": f"{ctx.message.author.id if ctx.message.author else None}",
            "username": f"{ctx.message.author if ctx.message.author else None}",
        }
