import asyncio
import sys
from datetime import datetime, timezone
from distutils.util import strtobool

import discord
import requests
from PIL import Image as PILImage
from db import models
from discord.ext import commands
from imagehash import phash, hex_to_hash
from sqlalchemy.exc import DBAPIError, IntegrityError


class Events(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.redis = bot.helpers.redis

    @commands.Cog.listener()
    async def on_member_join(self, member):
        session = self.bot.helpers.get_db_session()
        try:
            # If the member joining is this bot then skip trying to log
            if self.bot.user.id == member.id or member.bot:
                return

            # Get guild settings
            guild = member.guild
            settings = await self.bot.helpers.get_one_guild_settings(session, guild.id)
            # If welcome message enabled, send DM to user
            if settings.welcome_msg_enabled and settings.welcome_msg:
                try:
                    # Format the welcome message
                    action_type = "Message"
                    message = self.bot.constants.infraction_header.format(action_type=action_type.lower(), guild=guild)
                    # Reduces the text to 1,800 characters to leave enough buffer for header and footer text
                    message += f"{settings.welcome_msg[:1800]}"
                    # Set footer based on if the server has modmail or not
                    if settings.modmail_server_id:
                        message += self.bot.constants.footer_with_modmail.format(guild=guild)
                    else:
                        message += self.bot.constants.footer_no_modmail.format(guild=guild)
                    # Send the message
                    await member.send(message)
                except discord.Forbidden:
                    # Unable to DM user, likely privacy settings disallows, we can't do anything about it so ignore
                    pass
                except Exception as err:
                    self.bot.log.error(
                        f"Error sending welcome message to user {member} ({member.iod}). {sys.exc_info()[0].__name__}: {err}"
                    )
            # If join role set, add to user
            if settings.on_join_role_enabled and settings.on_join_role_id:
                try:
                    role = member.guild.get_role(settings.on_join_role_id)
                    if role:
                        await member.add_roles(role)
                except Exception as err:
                    self.bot.log.error(
                        f"Error adding on join role to user {member} ({member.iod}). {sys.exc_info()[0].__name__}: {err}"
                    )
            # Log new user to the database
            # First check if they exist in our DB already
            dbuser = session.query(models.User).filter(models.User.discord_id == member.id).first()
            # If they don't exist in the database
            if not dbuser:
                # Add them to the database
                dbuser = models.User(discord_id=member.id)
                session.add(dbuser)

            all_channels = (
                [channel for channel in guild.text_channels]
                + [channel for channel in guild.threads]
                + [channel for channel in guild.forums]
            )
            logs = discord.utils.get(all_channels, name="member-logs")
            if not logs or not isinstance(logs, (discord.TextChannel, discord.Thread, discord.ForumChannel)):
                return

            # Checks if the bot can even send messages in that channel
            if not (
                logs.permissions_for(logs.guild.me).send_messages and logs.permissions_for(logs.guild.me).embed_links
            ):
                return self.bot.log.debug(
                    f"Missing Permissions to log Member Join in Guild: {logs.guild.id} | Channel: {logs.id}"
                )

            # Guild join date
            guild_join_date = self.bot.helpers.relative_time(datetime.now(timezone.utc), member.joined_at, brief=True)
            guild_join_date = f"{member.joined_at.replace(microsecond=0)} UTC\n*{guild_join_date} ago*"

            # Discord join date
            discord_join_date = self.bot.helpers.relative_time(
                datetime.now(timezone.utc), member.created_at, brief=True
            )
            discord_join_date = f"{member.created_at.replace(microsecond=0)} UTC\n*{discord_join_date} ago*"

            # Create the embed of info
            embed = discord.Embed(color=0x80F31F, timestamp=datetime.now(timezone.utc))
            embed.set_author(name=f"{member} ({member.id})", icon_url=member.display_avatar.url)
            embed.set_thumbnail(url=member.display_avatar.url)
            embed.add_field(name="Joined Guild", value=f"{guild_join_date}")
            embed.add_field(name="Joined Discord", value=f"{discord_join_date}")
            embed.set_footer(text="User Joined")

            try:
                await logs.send(f"{member} ({member.id})", embed=embed)
            except discord.Forbidden:
                self.bot.log.warning(
                    f"Missing permissions to send in {logs.name} ({logs.id}) in guild {logs.guild.name} ({logs.guild.id})"
                )

            # process anti-spam checks
            # TO DO - Create a "Beta Feature Opt-In" feature
            redis_key = "discord.naughty_data.global.bad_pfp_hashes"
            # Hash the file
            image_data = PILImage.open(requests.get(member.display_avatar.url, stream=True).raw)
            pfp_filehash = phash(image_data)
            # get all the bad pfp hashes
            bad_pfp_hash_members = self.redis.smembers(redis_key)
            # Create an ImageHash file from a hex
            for redis_hash in bad_pfp_hash_members:
                redis_filehash = hex_to_hash(redis_hash)
                # Compare the Hamming distance by subtracting the hashes
                hamming_distance = pfp_filehash - redis_filehash
                # http://hackerfactor.com/blog/index.php%3F/archives/432-Looks-Like-It.html
                # A distance of zero indicates that it is likely a very similar picture
                # (or a variation of the same picture). A distance of 5 means a few things may be different,
                # but they are probably still close enough to be similar. But a distance of 10 or more?
                # That's probably a very different picture.
                if hamming_distance <= 5:
                    # TO DO - Add debugging to show the pfp image URL and the matching redis filehash url so we can visually compare in case there's an error
                    self.bot.log.info(
                        f"on_member_join antispam hamming_distance: {hamming_distance} | member: {member} ({member.id}) | PFPHash: {pfp_filehash} | RedisHash: {redis_filehash}"
                    )
                    # Initiate a kick, just like we do for an auto-mute

        except discord.HTTPException as err:
            self.bot.log.exception(
                f"Discord HTTP Error processing Member Join Event. {sys.exc_info()[0].__name__}: {err}"
            )
        except Exception as err:
            self.bot.log.exception(f"Error processing Member Join Event. {sys.exc_info()[ 0].__name__}: {err}")
        except DBAPIError as err:
            self.bot.log.exception(f"Error logging Member Join to database. {sys.exc_info()[0].__name__}: {err}")
            session.rollback()
        finally:
            session.close()

    @commands.Cog.listener()
    async def on_member_remove(self, member):
        try:
            # If the member leaving is themselves then skip trying to log
            if self.bot.user.id == member.id:
                return
            # Remove any Mod or Admin relationship on member leaving the server
            session = self.bot.helpers.get_db_session()
            try:
                await self.bot.helpers.db_process_mod_relationship(member, session, server_mod=False)
                await self.bot.helpers.db_process_admin_relationship(member, session, server_admin=False)
                # Commit the changes to the database
                session.commit()
            except IntegrityError:
                self.bot.log.debug(
                    f"Duplicate Server Relationship database record: Guild: {member.guild.id} | User: {member.id}"
                )
                session.rollback()
            except DBAPIError as err:
                self.bot.log.exception(
                    f"Database error logging Server Relationship removal to database. {sys.exc_info()[0].__name__}: {err}"
                )
                session.rollback()
            except Exception as err:
                self.bot.log.exception(
                    f"Generic Error logging Server Relationship removal to database. {sys.exc_info()[0].__name__}: {err}"
                )
                session.rollback()
            finally:
                session.close()

            try:
                guild = member.guild
                all_channels = (
                    [channel for channel in guild.text_channels]
                    + [channel for channel in guild.threads]
                    + [channel for channel in guild.forums]
                )
                logs = discord.utils.get(all_channels, name="member-logs")
                if not logs or not isinstance(logs, (discord.TextChannel, discord.Thread, discord.ForumChannel)):
                    return
            except Exception as err:
                return self.bot.log.exception(f"Error getting logs channel. {sys.exc_info()[0].__name__}: {err}")

            # Checks if the bot can even send messages in that channel
            if not (
                logs.permissions_for(logs.guild.me).send_messages and logs.permissions_for(logs.guild.me).embed_links
            ):
                return self.bot.log.debug(
                    f"Missing Permissions to log Member Part in Guild: {logs.guild.id} | Channel: {logs.id}"
                )

            # Guild join date
            guild_join_date = "Unknown"
            if member.joined_at:
                guild_join_date = self.bot.helpers.relative_time(
                    datetime.now(timezone.utc), member.joined_at, brief=True
                )

            guild_join_date = f"{member.joined_at.replace(microsecond=0) if member.joined_at else 'Unknown'} UTC\n*{guild_join_date} ago*"

            # Discord join date
            discord_join_date = "Unknown"
            if member.created_at:
                discord_join_date = self.bot.helpers.relative_time(
                    datetime.now(timezone.utc), member.created_at, brief=True
                )
            discord_join_date = f"{member.created_at.replace(microsecond=0) if member.created_at else 'Unknown'} UTC\n*{discord_join_date} ago*"

            # Create the embed of info
            embed = discord.Embed(color=0xC70000, timestamp=datetime.now(timezone.utc))
            embed.set_author(name=f"{member} ({member.id})", icon_url=member.display_avatar.url)
            embed.set_thumbnail(url=member.display_avatar.url)
            embed.add_field(name="Joined Guild", value=f"{guild_join_date}")
            embed.add_field(name="Joined Discord", value=f"{discord_join_date}")
            embed.set_footer(text="User Left")

            try:
                await logs.send(f"{member} ({member.id})", embed=embed)
            except discord.Forbidden:
                self.bot.log.warning(
                    f"Missing permissions to send in {logs.name} ({logs.id}) in guild {logs.guild.name} ({logs.guild.id})"
                )
        except discord.HTTPException as err:
            self.bot.log.exception(
                f"Discord HTTP Error processing Member Part Event. {sys.exc_info()[0].__name__}: {err}"
            )
        except Exception as err:
            self.bot.log.exception(f"Error processing Member Part Event. {sys.exc_info()[ 0].__name__}: {err}")

    @commands.Cog.listener()
    async def on_message(self, msg):
        # Processing for AutoMod messages
        # Need to get the message type - AutoMod is 24
        message = msg
        if message.type.value == 24:
            try:
                automod_ch = message.channel
                # Even though in the Discord UI it shows the Author of the message as
                #   being posted by AutoMod, the actual raw data has the author as the
                #   person who sent the message that triggered AutoMod
                orig_message_author = f"{message.author} ({message.author.id})"
                # AutoMod posts the original message as an embed
                orig_message_content = message.embeds[0].description
                embed_color = discord.Color.from_rgb(0, 0, 0)  # Black
                embed_header = "A Message Was Actioned by AutoMod"
                embed_category_data = ""
                embed_channel_data = ""
                embed_rule_name = ""
                for field in message.embeds[0].fields:
                    if field.name == "rule_name":
                        embed_rule_name = f"**Rule Name:** {field.value}\n"
                    elif field.name == "channel_id":
                        orig_channel = message.guild.get_channel(int(field.value))
                        if orig_channel.category:
                            embed_category_data = (
                                f"**Category:** {orig_channel.category} ({orig_channel.category.id})\n"
                            )
                        embed_channel_data = f"**Channel:** #{orig_channel} ({orig_channel.id})\n"

                # TO DO - Is #deleted-logs really the best place? Depends if we can separate blocked from alerted automod
                #   Or maybe we just post this same message in the automod_ch ?
                all_channels = (
                    [channel for channel in message.guild.text_channels]
                    + [channel for channel in message.guild.threads]
                    + [channel for channel in message.guild.forums]
                )
                logs = discord.utils.get(all_channels, name="deleted-logs")
                if logs and isinstance(logs, (discord.Thread, discord.TextChannel, discord.ForumChannel)):
                    # Checks if the bot can even send messages in that channel
                    if (
                        logs.permissions_for(logs.guild.me).send_messages
                        and logs.permissions_for(logs.guild.me).embed_links
                    ):
                        # Create the embed of info
                        description = ""
                        description += embed_rule_name
                        description += embed_category_data
                        description += embed_channel_data
                        description += f"AutoMod [Msg Link]({message.jump_url})\n"
                        # This isn't the actual timestamp of the original message, but close enough
                        description += f"**Message Timestamp:** {message.created_at.replace(microsecond=0)} UTC\n"
                        description += f"**Message:** (ID Not Available)\n\n"
                        description += f"{orig_message_content}"

                        embed = discord.Embed(
                            color=embed_color,
                            title=embed_header,
                            timestamp=datetime.now(timezone.utc),
                            description=description[: self.bot.constants.embed_description_limit],
                        )
                        embed.set_author(name=orig_message_author, icon_url=message.author.display_avatar)

                        if message.attachments:
                            for file in message.attachments:
                                embed.add_field(name="Attachment:", value=file.proxy_url)

                        try:
                            await logs.send(orig_message_author, embed=embed)
                        except discord.Forbidden:
                            self.bot.log.warning(
                                f"Missing permissions to send in {logs.name} ({logs.id}) in guild {logs.guild.name} ({logs.guild.id})"
                            )
            except Exception as err:
                self.bot.log.exception(f"Error processing AutoMod message  {sys.exc_info()[0].__name__}: {err}")

        # Check if it's from a DM, then ignore it since we're processing this in the mod mail module
        if isinstance(msg.channel, discord.DMChannel):
            pass

        if isinstance(msg.channel, (discord.TextChannel, discord.Thread, discord.ForumChannel)):
            # Log each message to the database
            # Unless it's an AutoMod message, then skip it
            if msg.type == discord.MessageType.auto_moderation_action:
                return

            session = self.bot.helpers.get_db_session()
            try:
                # Check if there is a user in the database already
                db_user = session.query(models.User).filter(models.User.discord_id == msg.author.id).first()
                # If no DB record for the user then create one
                if not db_user:
                    db_user = models.User(discord_id=msg.author.id)
                    session.add(db_user)
                # Check if there is a guild in the database already
                db_guild = session.query(models.Server).filter(models.Server.discord_id == msg.guild.id).first()
                if not db_guild:
                    db_guild = await self.bot.helpers.db_add_new_guild(session, msg.guild.id, msg.guild)

                message_details = {
                    "message_id": msg.id,
                    "channel_id": msg.channel.id,
                    "channel_name": msg.channel.name,
                }
                new_message = models.Message(user=db_user, server=db_guild, **message_details)
                session.add(new_message)
                session.commit()
            except IntegrityError as err:
                # We don't need to log when there is a duplicate message in the database as it won't be helpful.
                # self.bot.log.debug(
                #     f"Duplicate database record: '/{msg.guild.id}/{msg.channel.id}/{msg.id}' to database. {sys.exc_info()[0].__name__}: {err}"
                # )
                session.rollback()
            except DBAPIError as err:
                self.bot.log.exception(
                    f"Generic Error logging message '/{msg.guild.id}/{msg.channel.id}/{msg.id}' to database. {sys.exc_info()[0].__name__}: {err}"
                )
                session.rollback()
            finally:
                session.close()

            try:
                channel_key = f"discord.messages.gid:{msg.guild.id}.cid:{msg.channel.id}"
                user_msg_value = f"uid:{msg.author.id}.mid:{msg.id}"
                redis_score_epoch = msg.created_at.timestamp()
                # However we can't expire the key (channel.id) normally, otherwise all messages are lost below it in the set
                # but we can set a TTL on the channel key, and use pipelines to update the expire time whenever we add new elements to it
                # which would mean that as we add new stuff, it extends the expire time indefinitely, but if we don't extend it, then everything
                # would be over 2 days old, which is when we'd remove by pruning anyways.
                #
                # So what we also do is set a task to use ZREMRANGEBYSCORE and purge old items by epoch (score), which keeps the overall key small
                # because if it's an active channel, the TTL expire would never be reached, so pruning still needed
                # Redis Expire format: seconds | seconds * minutes * hours * days
                redis_expire_time = 60 * 60 * 24 * 2
                # Create a pipeline to batch the requests
                pipeline = self.redis.pipeline()
                # Add the user message to the channel key
                pipeline.zadd(name=channel_key, mapping={user_msg_value: redis_score_epoch})
                # Set key expiration
                pipeline.expire(channel_key, redis_expire_time)

                # Now let's update the Most Frequently Used sorted set so we know what has the most values in the key
                # so we can target those for cleaning
                most_freq_used_key = f"discord.messages.channels.most_freq_used"
                redis_score_incr = 1
                # Increase the message count in most_freq_used key
                pipeline.zadd(
                    name=most_freq_used_key,
                    mapping={channel_key: redis_score_incr},
                    incr=True,
                )
                # All done, execute the pipeline
                results = pipeline.execute()

            except Exception as err:
                self.bot.log.exception(
                    f"Generic Error logging message '/{msg.guild.id}/{msg.channel.id}/{msg.id}' to Redis. {sys.exc_info()[0].__name__}: {err}"
                )

            # Perform AntiSpam checks
            self.bot.log.debug(f"About to start AntiSpam Checks for Msg ID: {msg.id}")
            await self.bot.antispam.antispam_process_message(msg)

            # Perform AutoPublish checks
            self.bot.log.debug(f"About to start AutoPublish Checks for Msg ID: {msg.id}")
            if msg.channel.is_news():
                await self.bot.helpers.check_autopublish(msg)

    @commands.Cog.listener()
    async def on_message_delete(self, message):
        pass

    @commands.Cog.listener()
    async def on_raw_message_delete(self, payload):
        try:
            # Get guild. Could be none if not a guild message
            if payload.guild_id is None:
                return
            guild = self.bot.get_guild(payload.guild_id)
            channel = guild.get_channel_or_thread(payload.channel_id)

            # If the message is cached, use that
            if payload.cached_message:
                message = payload.cached_message
            # Otherwise create a generic object
            else:
                message = discord.Object(id=payload.message_id)

            # Send to processor to log
            await self.process_deleted_message(message, channel, guild)
        except discord.HTTPException as err:
            self.bot.log.exception(
                f"Discord HTTP Error processing Message Delete Event. {sys.exc_info()[0].__name__}: {err}"
            )
        except asyncio.CancelledError as err:
            pass
        except Exception as err:
            self.bot.log.exception(f"Error processing Message Delete Event. {sys.exc_info()[ 0].__name__}: {err}")

    @commands.Cog.listener()
    async def on_raw_bulk_message_delete(self, payload):
        try:
            # Get guild. Could be none if not a guild message
            if payload.guild_id is None:
                return
            guild = self.bot.get_guild(payload.guild_id)
            channel = guild.get_channel_or_thread(payload.channel_id)

            messages_deleted_ids = payload.message_ids
            messages_deleted_obj = []
            # Loop through all deleted message objects, add to the obj list.
            # what remains is the id's not in cache
            for message in payload.cached_messages:
                messages_deleted_ids.remove(message.id)
                messages_deleted_obj.append(message)

            # Now loop through all id's not in cache, make a generic object
            for messageid in messages_deleted_ids:
                message = discord.Object(id=messageid)
                messages_deleted_obj.append(message)

            # Send to processor to log
            for message in messages_deleted_obj:
                await self.process_deleted_message(message, channel, guild)

        except discord.HTTPException as err:
            self.bot.log.warning(
                f"Discord HTTP Error processing Bulk Message Delete Event. {sys.exc_info()[0].__name__}: {err}"
            )
        except asyncio.CancelledError as err:
            pass
        except Exception as err:
            self.bot.log.exception(f"Error processing Bulk Message Delete Event. {sys.exc_info()[ 0].__name__}: {err}")

    async def process_deleted_message(self, message, channel: discord.abc.GuildChannel, guild: discord.Guild):
        # Remove from the Redis cache
        try:
            redis_guild_channel_key = f"discord.messages.gid:{guild.id}.cid:{channel.id}"
            result = self.redis.zrem(
                redis_guild_channel_key,
                *[f"uid:*.mid:{message.id}"],
            )

            # Decrease the "discord.messages.channels.most_freq_used"
            amount_to_decrease = -1
            self.redis.zincrby(
                name="discord.messages.channels.most_freq_used",
                amount=amount_to_decrease,
                value=redis_guild_channel_key,
            )
        except Exception as err:
            self.bot.log.exception(f"Exception deleting single message from redis cache")

        all_channels = [ch for ch in guild.text_channels] + [ch for ch in guild.threads] + [ch for ch in guild.forums]
        logs = discord.utils.get(all_channels, name="deleted-logs")
        if not logs or not isinstance(logs, (discord.TextChannel, discord.Thread, discord.ForumChannel)):
            return

        # Checks if the bot can even send messages in that channel
        if logs.permissions_for(logs.guild.me).send_messages and logs.permissions_for(logs.guild.me).embed_links:
            member = None
            member_info = None
            msg_content = ""
            description = ""
            if isinstance(message, discord.Message):
                member = message.author
                member_info = f"{member} ({member.id})"
                msg_content = message.clean_content
            else:
                description += (
                    f"\N{WARNING SIGN} __[MESSAGE NOT FOUND IN INTERNAL CACHE]__ \N{WARNING SIGN}\n"
                    f"\N{WARNING SIGN} __[MESSAGE CONTENT & AUTHOR UNAVAILABLE]__ \N{WARNING SIGN}\n\n"
                )

            # Create the embed of info
            if isinstance(channel, discord.abc.GuildChannel) and channel.category:
                description += f"**Category:** {channel.category.name} ({channel.category.id})\n"
            if isinstance(channel, discord.Thread) and channel.parent:
                description += f"**Parent:** {channel.parent.name} ({channel.parent.id})\n"

            description += f"**Channel:** #{channel.name} ({channel.id})\n"
            description += f"**Msg Timestamp:** {message.created_at.replace(microsecond=0)} UTC\n"
            description += f"**Msg:** ({message.id})\n\n"
            description += msg_content

            embed = discord.Embed(
                color=0x5C28C2,
                title="A Message Was Deleted",
                timestamp=datetime.now(timezone.utc),
                description=description[: self.bot.constants.embed_description_limit],
            )
            if member:
                embed.set_author(name=member_info, icon_url=member.display_avatar.url)

            if isinstance(message, discord.Message) and message.attachments:
                for file in message.attachments:
                    embed.add_field(name="Attachment:", value=file.proxy_url)

            try:
                await logs.send(member_info, embed=embed)
            except discord.Forbidden:
                self.bot.log.warning(
                    f"Missing permissions to send in {logs.name} ({logs.id}) in guild {logs.guild.name} ({logs.guild.id})"
                )

    @commands.Cog.listener()
    async def on_message_edit(self, message_before, message_after):
        try:
            # Try and get the guild
            if not message_after.guild or message_after.guild.unavailable:
                return
            else:
                guild = message_after.guild

            # Sometimes we get a discord.Object for normal channels, so let's try and fix that
            if isinstance(message_after.channel, discord.Object):
                self.bot.log.debug(f"Edited message.channel is a discord.Object, trying to fix")
                message_after_channel = guild.get_channel_or_thread(message_after.channel.id)
            else:
                message_after_channel = message_after.channel

            # If contains embeds, ignore (known to cause excessive "edit" message events)
            if message_before.embeds or message_after.embeds:
                return
            # If content is the same thing, ignore (Can be caused by publishing a message)
            if message_before.content == message_after.content:
                return

            # Get logs channel
            all_channels = (
                [channel for channel in guild.text_channels]
                + [channel for channel in guild.threads]
                + [channel for channel in guild.forums]
            )
            logs = discord.utils.get(all_channels, name="edited-logs")
            if not logs or not isinstance(logs, (discord.TextChannel, discord.Thread, discord.ForumChannel)):
                return

            # Checks if the bot can even send messages in that channel
            if not logs.permissions_for(logs.guild.me).send_messages:
                return self.bot.log.debug(
                    f"Missing Permissions to log Message Delete in Guild: {logs.guild.id} | Channel: {logs.id}"
                )

            member = message_after.author

            # Create the embed of info
            embed = discord.Embed(
                color=0x5C28C2,
                title="A Message Was Edited",
                timestamp=datetime.now(timezone.utc),
            )

            description = ""
            if isinstance(message_after_channel, discord.abc.GuildChannel) and message_after_channel.category:
                description += (
                    f"**Category:** {message_after_channel.category.name} ({message_after_channel.category.id})\n"
                )
            if isinstance(message_after_channel, discord.Thread) and message_after_channel.parent:
                description += f"**Parent:** {message_after_channel.parent.name} ({message_after_channel.parent.id})\n"
            description += f"**Channel:** #{message_after_channel.name} ({message_after.channel.id})\n"
            description += f"**Created Timestamp:** {message_after.created_at.replace(microsecond=0)} UTC\n"
            description += f"**Edited Timestamp:** {message_after.edited_at.replace(microsecond=0) if message_after.edited_at else None} UTC\n"
            description += f"**Before:** ({message_before.id})\n\n"
            description += f"{message_before.clean_content[:1750]}"
            embed.description = description[:2048]

            embed.add_field(name="**After:**", value=f"_ _\n{message_after.clean_content}"[:1024])
            embed.set_footer(text="Note: Before/After text may be trimmed due to limitations.")

            embed.set_author(name=f"{member} ({member.id})", icon_url=member.display_avatar.url)

            try:
                await logs.send(f"{member} ({member.id})", embed=embed)
            except discord.Forbidden:
                self.bot.log.warning(
                    f"Missing permissions to send in {logs.name} ({logs.id}) in guild {logs.guild.name} ({logs.guild.id})"
                )
        except discord.HTTPException as err:
            self.bot.log.exception(
                f"Discord HTTP Error processing Message Edit Event. {sys.exc_info()[0].__name__}: {err}"
            )
        except Exception as err:
            self.bot.log.exception(f"Error processing Message Edit Event. {sys.exc_info()[ 0].__name__}: {err}")

    @commands.Cog.listener()
    async def on_guild_join(self, newguild):
        session = self.bot.helpers.get_db_session()
        try:
            # Check if there is a guild in the database already
            db_guild = session.query(models.Server).filter(models.Server.discord_id == newguild.id).first()

            # If not, add one
            if not db_guild:
                db_guild = await self.bot.helpers.db_add_new_guild(session, newguild.id, newguild)

            # Check if there are guild settings
            guild_settings = (
                session.query(models.ServerSetting).filter(models.ServerSetting.server_id == db_guild.id).first()
            )

            # If there is no guild settings, create them
            if not guild_settings:
                await self.bot.helpers.db_add_new_guild_settings(session, db_guild)

            # Update local cache with new guild settings
            await self.bot.helpers.get_one_guild_settings(session, newguild.id)
            self.bot.log.info(f"Joined new guild: {newguild.name} ({newguild.id})")
            # Get guild owner.. because guild.owner can be None we get member from owner_id
            guild_owner = await self.bot.helpers.get_member_or_user(newguild.owner_id, newguild)

            # Send a welcome message to the new guild's owner
            try:
                # Gets the bot owner info.
                if isinstance(self.bot.owner, list):
                    owners = [f"{temp_user.mention} ({temp_user.id})" for temp_user in self.bot.owner]
                else:
                    owners = [f"{self.bot.owner.mention} ({self.bot.owner.id})"]
                bot_owners_string = ", ".join(owners)

                if guild_owner:
                    welcome_message = self.bot.helpers.redis.get(f"discord.bid:{self.bot.user.id}.welcome_message")
                    if welcome_message:
                        await guild_owner.send(welcome_message.replace("{{bot_owners_string}}", bot_owners_string))
            except discord.Forbidden:
                # If there's an error sending to the Guild Owner, ignore it. Nothing we can do.
                pass
            except Exception as err:
                self.bot.log.exception(
                    f"Error sending Welcome message to new guild. {sys.exc_info()[0].__name__}: {err}"
                )

            # Logs in the bots support server
            support_guild = self.bot.get_guild(
                self.bot.botconfig.getint("BotConfig", "ON_GUILD_JOIN_GUILD_ID", fallback=547_549_495_563_517_953)
            )
            if support_guild:
                # Get logs channel
                all_channels = (
                    [channel for channel in support_guild.text_channels]
                    + [channel for channel in support_guild.threads]
                    + [channel for channel in support_guild.forums]
                )
                logs = discord.utils.get(all_channels, name="guild-logs")
                if logs:
                    # Create the embed of info
                    embed = discord.Embed(
                        color=0x0B9CB4,
                        timestamp=datetime.now(timezone.utc),
                        description=f"**Guild Description:** {newguild.description}",
                    )
                    embed.set_author(
                        name=f"Joined: '{newguild}' ({newguild.id})",
                        icon_url=newguild.icon.url if newguild.icon else None,
                    )
                    embed.add_field(
                        name="Guild Owner",
                        value=f"{guild_owner} ({guild_owner.id})",
                        inline=False,
                    )
                    # Guild features
                    if newguild.features:
                        features_list = newguild.features
                        features_list.sort()
                        features = ""
                        for item in features_list:
                            feature = str(item).replace("_", " ").title()
                            features += f"• {feature}\n"
                        embed.add_field(name="Features", value=features, inline=False)

                    bots = len([member for member in newguild.members if member.bot])
                    humans = len(newguild.members) - bots
                    embed.add_field(
                        name="Bots : Humans : Total",
                        value=f"{bots} : {humans} : {newguild.member_count}",
                        inline=False,
                    )
                    # Set the thumbnail at top right to the banner that shows below the channel list
                    if newguild.banner:
                        embed.set_thumbnail(url=newguild.banner.url)
                    try:
                        await logs.send(embed=embed)
                    except discord.Forbidden:
                        self.bot.log.warning(
                            f"Missing permissions to send in {logs.name} ({logs.id}) in guild {logs.guild.name} ({logs.guild.id})"
                        )

        except discord.HTTPException as err:
            self.bot.log.exception(
                f"Discord HTTP Error processing Guild Join Event. {sys.exc_info()[0].__name__}: {err}"
            )
        except DBAPIError as err:
            self.bot.log.exception(f"Error with Guild Join Event database calls. {sys.exc_info()[0].__name__}: {err}")
            session.rollback()
        except Exception as err:
            self.bot.log.exception(f"Error processing Guild Join Event. {sys.exc_info()[0].__name__}: {err}")
        finally:
            session.close()

    @commands.Cog.listener()
    async def on_guild_update(self, guild_before, guild_after):
        session = self.bot.helpers.get_db_session()
        try:
            # If no change in guild name, return
            if guild_before.name == guild_after.name:
                return
            # Check if there is a guild in the database already
            guild = session.query(models.Server).filter(models.Server.discord_id == guild_after.id).first()

            # If not, add one which will set the guild name, then we can return
            if not guild:
                guild = await self.bot.helpers.db_add_new_guild(session, guild_after.id, guild_after)
                # Check if there are guild settings
                guild_settings = (
                    session.query(models.ServerSetting).filter(models.ServerSetting.server_id == guild.id).first()
                )
                # If there is no guild settings, create them
                if not guild_settings:
                    await self.bot.helpers.db_add_new_guild_settings(session, guild)
                # Update local cache with new guild settings
                await self.bot.helpers.get_one_guild_settings(session, guild_after.id)
                # Now we're done processing new guild and updating the settings, return
                return
            else:
                guild.name = guild_after.name
                session.commit()
                return self.bot.log.info(f"Updated guild name in DB for: {guild_after.name} ({guild_after.id})")

        except discord.HTTPException as err:
            self.bot.log.exception(
                f"Discord HTTP Error processing Guild Update Event. {sys.exc_info()[0].__name__}: {err}"
            )
        except DBAPIError as err:
            self.bot.log.exception(f"Error with Guild Update Event database calls. {sys.exc_info()[0].__name__}: {err}")
            session.rollback()
        except Exception as err:
            self.bot.log.exception(f"Error processing Guild Update Event. {sys.exc_info()[0].__name__}: {err}")
        finally:
            session.close()

    @commands.Cog.listener()
    async def on_guild_remove(self, oldguild):
        try:
            self.bot.log.info(f"Parted guild: {oldguild.name} ({oldguild.id})")

            try:
                # Get all the keys that match the guild:
                redis_key = f"discord.messages.gid:{oldguild.id}.cid:*"
                # This will scan all keys in Redis, 20 at a time, matching the key. It'll return the results in a
                # generator. It's best to have a reasonable count otherwise too high and it blocks commands, too low
                # and it'll take too long
                guild_channel_keys = self.redis.scan_iter(match=redis_key, count=20)
                # Delete the Redis keys for that guild
                for redis_channel_key in guild_channel_keys:
                    await self.bot.helpers.redis_clean_most_freq_used_and_channel_key(redis_channel_key)

            except Exception as err:
                self.bot.log.exception(
                    f"Error deleting redis key during Guild Part. {sys.exc_info()[0].__name__}: {err}"
                )

            # Get guild owner.. because guild.owner can be None we get member from owner_id
            guild_owner = await self.bot.helpers.get_member_or_user(oldguild.owner_id, oldguild)

            # Logs in the bots support server
            support_guild = self.bot.get_guild(
                self.bot.botconfig.getint("BotConfig", "ON_GUILD_JOIN_GUILD_ID", fallback=547_549_495_563_517_953)
            )
            if support_guild:
                # Get logs channel
                all_channels = (
                    [channel for channel in support_guild.text_channels]
                    + [channel for channel in support_guild.threads]
                    + [channel for channel in support_guild.forums]
                )
                logs = discord.utils.get(all_channels, name="guild-logs")
                if logs:
                    # Create the embed of info
                    embed = discord.Embed(
                        color=0x06505C,
                        timestamp=datetime.now(timezone.utc),
                        description=f"**Guild Description:** {oldguild.description}",
                    )
                    embed.set_author(
                        name=f"Parted: '{oldguild}' ({oldguild.id})",
                        icon_url=oldguild.icon.url if oldguild.icon else None,
                    )
                    embed.add_field(
                        name="Guild Owner",
                        value=f"{guild_owner} ({guild_owner.id})",
                        inline=False,
                    )
                    features_list = oldguild.features if oldguild.features else []
                    features_list.sort()
                    if features_list:
                        features = ""
                        for item in features_list:
                            feature = str(item).replace("_", " ").title()
                            features += f"• {feature}\n"
                        embed.add_field(name="Features", value=f"{features}", inline=False)

                    if oldguild.members:
                        bots = len([member for member in oldguild.members if member.bot])
                        humans = len(oldguild.members) - bots
                        embed.add_field(
                            name="Bots : Humans : Total",
                            value=f"{bots} : {humans} : {oldguild.member_count}",
                            inline=False,
                        )
                    embed.set_thumbnail(url=oldguild.icon.url if oldguild.icon else None)
                    try:
                        await logs.send(embed=embed)
                    except discord.Forbidden:
                        self.bot.log.warning(
                            f"Missing permissions to send in {logs.name} ({logs.id}) in guild {logs.guild.name} ({logs.guild.id})"
                        )
        except discord.HTTPException as err:
            self.bot.log.exception(
                f"Discord HTTP Error processing Guild Part Event. {sys.exc_info()[0].__name__}: {err}"
            )
        except Exception as err:
            self.bot.log.exception(f"Error processing Guild Part Event. {sys.exc_info()[0].__name__}: {err}")

    @commands.Cog.listener()
    async def on_guild_channel_delete(self, channel):
        try:
            # While this shouldn't be necessary since this a guild channel delete event, doesn't hurt to validate
            # that this isn't a DM Channel
            if isinstance(channel, discord.DMChannel):
                return
            # If it's a Voice Channel
            if isinstance(channel, discord.VoiceChannel):
                await self.bot.helpers.RemoveAutoVC(channel)
            # and that it's only a Text Channel since those are all that messages log from
            if not isinstance(channel, discord.TextChannel):
                return

            # Delete the Redis key
            redis_channel_key = f"discord.messages.gid:{channel.guild.id}.cid:{channel.id}"
            await self.bot.helpers.redis_clean_most_freq_used_and_channel_key(redis_channel_key)

        except discord.HTTPException as err:
            self.bot.log.exception(
                f"Discord HTTP Error processing Guild Channel Delete Event. {sys.exc_info()[0].__name__}: {err}"
            )
        except Exception as err:
            self.bot.log.exception(f"Error processing Guild Channel Delete Event. {sys.exc_info()[ 0].__name__}: {err}")

    @commands.Cog.listener()
    async def on_user_update(self, userbefore, userafter):
        session = self.bot.helpers.get_db_session()
        try:
            # If the user is this bot then skip trying to log
            if self.bot.user.id == userafter.id:
                return

            # Get the database user
            dbuser = session.query(models.User).filter(models.User.discord_id == userafter.id).first()
            # If they don't exist in the database
            if not dbuser:
                # Add them to the database
                dbuser = models.User(discord_id=userafter.id)
                session.add(dbuser)

            # User can update multiple things like Alias and Avatar within one event
            embeds = []

            # User changed Avatar
            # use old user data as any other changes will show up as a second embed
            userbefore_asset = userbefore.avatar
            userafter_asset = userafter.avatar
            tags = ["avatar_change"]
            context = {"userid": f"{userbefore.id}"}
            if userbefore_asset != userafter_asset:

                self.bot.log.debug(f"Updated User avatar for {userbefore} ({userbefore.id})")

                if userbefore_asset:
                    # if they had a user avatar before, try and get that (most likely will fail due to discord caching/deleting it quickly)
                    old_avatar = await self.bot.helpers.upload_image(userbefore_asset)
                else:
                    # they didn't have a user avatar, and were using the default one so show the default avatar
                    old_avatar = await self.bot.helpers.upload_image(userbefore.default_avatar)
                if userafter_asset:
                    # If they set a new user avatar, get that
                    new_avatar = await self.bot.helpers.upload_image(userafter_asset)
                else:
                    # if they didn't set a new avatar, they are using the default one, show that
                    new_avatar = await self.bot.helpers.upload_image(userafter.display_avatar)

                # Create the embed for avatar change
                embed = discord.Embed(color=0x7289DA, timestamp=datetime.now(timezone.utc))
                description = "**Reason:** User Avatar Changed"
                if old_avatar.secure_url:
                    description += "\nOld Avatar shown on **right**"
                    description += f"\n\nNOTE: _Old avatar link may decay in the near future._"
                    embed.set_thumbnail(url=old_avatar.secure_url)

                embed.set_author(name=f"{userbefore} ({userbefore.id})", icon_url=old_avatar.secure_url)

                if new_avatar.secure_url:
                    description += "\n\nNew Avatar shown **below**"
                    embed.set_image(url=new_avatar.secure_url)
                embed.description = description

                embeds.append(embed)

            # User name changed
            if userbefore.name != userafter.name:
                # Create the embed of info for alias change
                alias_description = f"**Reason:** Username Changed\n"
                alias_description += f"**Previous Username:** {userbefore.name}\n"
                alias_description += f"**New Username:** {userafter.name}"

                embed = discord.Embed(
                    color=0x7289DA,
                    timestamp=datetime.now(timezone.utc),
                    description=alias_description,
                )
                embed.set_author(name=f"{userafter} ({userafter.id})", icon_url=userafter.display_avatar.url)

                embed.set_thumbnail(url=userafter.display_avatar.url)
                embeds.append(embed)

            # Global name changed
            if userbefore.global_name != userafter.global_name:
                # Create the embed of info for change
                alias_description = f"**Reason:** Global Name Changed\n"
                alias_description += f"**Previous Global Name:** {userbefore.global_name}\n"
                alias_description += f"**New Global Name:** {userafter.global_name}"

                embed = discord.Embed(
                    color=0x7289DA,
                    timestamp=datetime.now(timezone.utc),
                    description=alias_description,
                )
                embed.set_author(name=f"{userafter} ({userafter.id})", icon_url=userafter.display_avatar.url)

                embed.set_thumbnail(url=userafter.display_avatar.url)
                embeds.append(embed)

            # No change, do nothing
            if len(embeds) == 0:
                return

            # Send to all guilds log channel
            for guild in self.bot.guilds:
                member = discord.utils.find(lambda m: m.id == userafter.id, guild.members)
                if member:
                    # Get logs channel
                    all_channels = (
                        [channel for channel in guild.text_channels]
                        + [channel for channel in guild.threads]
                        + [channel for channel in guild.forums]
                    )

                    logs = discord.utils.get(all_channels, name="name-change-logs")
                    if logs and isinstance(logs, (discord.TextChannel, discord.Thread, discord.ForumChannel)):
                        try:
                            await logs.send(f"{userafter} ({userafter.id})", embeds=embeds)
                        except discord.Forbidden:
                            self.bot.log.warning(
                                f"Missing permissions to send in {logs.name} ({logs.id}) in guild {logs.guild.name} ({logs.guild.id})"
                            )

        except discord.HTTPException as err:
            self.bot.log.exception(
                f"Discord HTTP Error processing User Update Event. {sys.exc_info()[0].__name__}: {err}"
            )
        except Exception as err:
            self.bot.log.exception(f"Error processing User Update Event. {sys.exc_info()[0].__name__}: {err}")
        except DBAPIError as err:
            self.bot.log.exception(
                f"Error with database calls for User Update event. {sys.exc_info()[0].__name__}: {err}"
            )
            session.rollback()
        finally:
            session.close()

    @commands.Cog.listener()
    async def on_member_update(self, member_before, member_after):
        try:
            # If the member is this bot then skip trying to log
            if self.bot.user.id == member_after.id:
                return

            # If their activity changes, e.g. streaming a game, check if we need to
            # update their roles
            # 1. Check if Activity based roles feature is enabled
            try:
                feature_status = self.redis.get(
                    f"discord.settings.activityrole.gid:{member_after.guild.id}.feature_status"
                )
                feature_status_bool: bool = False
                if feature_status:
                    feature_status_bool = strtobool(feature_status)

                # Checks so the on_member_event isn't causing duplicate work
                try:
                    # Checks if the user is pending verification
                    member_pending_status: bool = member_after.pending
                except AttributeError:
                    member_pending_status: bool = False
                # Need to exclude everything but a activity change otherwise it won't check for activity change if activity name is the same stream name
                if (
                    feature_status_bool  # feature enabled
                    and (member_before.roles == member_after.roles)  # there are no role changes
                    and (member_pending_status is False)  # user isn't pending discord verification
                    and (member_before.nick == member_after.nick)  # and they haven't had a nickname change
                    and (
                        member_before.status == member_after.status
                    )  # and their status (online/offline/etc) hasn't changed
                ):
                    # 2. Loop through all activities for member_before
                    streaming_before = None
                    game_before = None
                    for activity in member_before.activities:
                        # If the activity is streaming
                        if activity.type == discord.ActivityType.streaming:
                            streaming_before = True
                            # Check if a role is granted for any activity
                            any_activity_bool = None
                            any_activity = self.redis.get(
                                f"discord.settings.activityrole.gid:{member_before.guild.id}.any_activity"
                            )
                            if any_activity:
                                any_activity_bool = bool(strtobool(any_activity))

                            # Set the activity name for getting the role from game key
                            if any_activity_bool:
                                game_before = "any_activity"
                            else:
                                game_before = activity.game.lower()
                            self.bot.log.debug(
                                f"ActivityRole: Before Activity Detected: {game_before} | Guild: {member_after.guild.id}"
                            )
                            break

                    # 3. Loop through all activities for member_after
                    streaming_after = None
                    game_after = None
                    for activity in member_after.activities:
                        # If the activity is streaming
                        if activity.type == discord.ActivityType.streaming:
                            # Check if a role is granted for any activity
                            any_activity_bool = None
                            any_activity = self.redis.get(
                                f"discord.settings.activityrole.gid:{member_after.guild.id}.any_activity"
                            )
                            if any_activity:
                                any_activity_bool = bool(strtobool(any_activity))

                            # Set the activity name for getting the role from game key
                            if any_activity_bool:
                                game_after = "any_activity"
                            else:
                                game_after = activity.game.lower()

                            self.bot.log.debug(
                                f"ActivityRole: After Activity Detected: {game_after} | Guild: {member_after.guild.id}"
                            )
                            # Break out, stop searching for activities
                            streaming_after = True
                            break

                    # 4. Perform checks to determine if we need to adjust the role or grant/remove a role
                    if streaming_before and streaming_after:
                        # If game is any activity, make sure they get the role
                        if game_after == "any_activity":
                            after_role_id = self.redis.get(
                                f"discord.settings.activityrole.gid:{member_after.guild.id}.{game_after}_role"
                            )
                            if after_role_id:
                                self.bot.log.debug(
                                    f"ActivityRole: After Activity Role: {game_after} | Guild: {member_after.guild.id} | Role ID: {after_role_id}"
                                )
                                after_role_id = int(after_role_id)
                                # Add the new role
                                await self.bot.helpers.activityrole_adjust_role(
                                    member=member_after,
                                    action="add",
                                    role_id=after_role_id,
                                )
                        else:
                            # Get both the roles for before game
                            before_role_id = self.redis.get(
                                f"discord.settings.activityrole.gid:{member_after.guild.id}.{game_before}_role"
                            )
                            if before_role_id:
                                before_role_id = int(before_role_id)
                            # And after game change
                            after_role_id = self.redis.get(
                                f"discord.settings.activityrole.gid:{member_after.guild.id}.{game_after}_role"
                            )
                            if after_role_id:
                                after_role_id = int(after_role_id)

                            if before_role_id:
                                self.bot.log.debug(
                                    f"ActivityRole: Before Activity Role: {game_before} | Guild: {member_after.guild.id} | Role ID: {before_role_id}"
                                )
                                # Remove the old role
                                await self.bot.helpers.activityrole_adjust_role(
                                    member=member_after,
                                    action="remove",
                                    role_id=before_role_id,
                                )
                            if after_role_id:
                                self.bot.log.debug(
                                    f"ActivityRole: After Activity Role: {game_after} | Guild: {member_after.guild.id} | Role ID: {after_role_id}"
                                )
                                # Add the new role
                                await self.bot.helpers.activityrole_adjust_role(
                                    member=member_after,
                                    action="add",
                                    role_id=after_role_id,
                                )
                    elif streaming_before and not streaming_after:
                        # remove role
                        role_id = self.redis.get(
                            f"discord.settings.activityrole.gid:{member_after.guild.id}.{game_before}_role"
                        )
                        if role_id:
                            self.bot.log.debug(
                                f"ActivityRole: Before Activity Role (and not after): {game_before} | Guild: {member_after.guild.id} | Role ID: {role_id}"
                            )
                            role_id = int(role_id)
                            await self.bot.helpers.activityrole_adjust_role(
                                member=member_after,
                                action="remove",
                                role_id=role_id,
                            )
                    elif not streaming_before and streaming_after:
                        # add role
                        role_id = self.redis.get(
                            f"discord.settings.activityrole.gid:{member_after.guild.id}.{game_after}_role"
                        )
                        if role_id:
                            self.bot.log.debug(
                                f"ActivityRole: After Activity Role (and not before): {game_after} | Guild: {member_after.guild.id} | Role ID: {role_id}"
                            )
                            role_id = int(role_id)
                            await self.bot.helpers.activityrole_adjust_role(
                                member=member_after,
                                action="add",
                                role_id=role_id,
                            )
            except Exception as err:
                self.bot.log.exception(f"{sys.exc_info()[0].__name__}: {err}")
                pass

            # Check if there is a role change, then process that for the servermodrels and serveradminrels tables
            if member_before.roles != member_after.roles:
                server_admin = None
                server_mod = None
                perms_before = member_before.guild_permissions
                perms_after = member_after.guild_permissions
                # User had administrator before, but doesn't now. Need to remove the admin relationship
                if perms_before.administrator and not perms_after.administrator:
                    server_admin = False
                # User did NOT have administrator before, but does have it now. Need to add admin relationship
                elif not perms_before.administrator and perms_after.administrator:
                    server_admin = True
                else:
                    settings = self.bot.guild_settings.get(member_after.guild.id)
                    # Get their roles before and after
                    before_role_ids = [role.id for role in member_before.roles]
                    after_role_ids = [role.id for role in member_after.roles]
                    # If an admin role is set, check if that role changed
                    if settings and settings.admin_role:
                        if settings.admin_role in before_role_ids and settings.admin_role not in after_role_ids:
                            server_admin = False
                        if settings.admin_role in after_role_ids and settings.admin_role not in before_role_ids:
                            server_admin = True
                    # If a mod role is set, check if that role changed
                    if settings and settings.mod_role:
                        if settings.mod_role in before_role_ids and settings.mod_role not in after_role_ids:
                            server_mod = False
                        if settings.mod_role in after_role_ids and settings.mod_role not in before_role_ids:
                            server_mod = True

                # If there is a server admin relationship change, log it
                if server_admin is not None:
                    # Log to the database
                    session = self.bot.helpers.get_db_session()
                    try:
                        await self.bot.helpers.db_process_admin_relationship(member_after, session, server_admin)
                        # Commit the changes to the database
                        session.commit()
                    except IntegrityError:
                        self.bot.log.debug(
                            f"Duplicate Server Admin Relationship database record: Guild: {member_after.guild.id} | User: {member_after.id}"
                        )
                        session.rollback()
                    except DBAPIError as err:
                        self.bot.log.exception(
                            f"Database error logging Server Admin Relationship to database. {sys.exc_info()[0].__name__}: {err}"
                        )
                        session.rollback()
                    except Exception as err:
                        self.bot.log.exception(
                            f"Generic Error logging Server Admin Relationship to database. {sys.exc_info()[0].__name__}: {err}"
                        )
                        session.rollback()
                    finally:
                        session.close()
                # If there is a server mod relationship change, log it
                if server_mod is not None:
                    # Log to the database
                    session = self.bot.helpers.get_db_session()
                    try:
                        await self.bot.helpers.db_process_mod_relationship(member_after, session, server_mod)
                        # Commit the changes to the database
                        session.commit()
                    except IntegrityError:
                        self.bot.log.debug(
                            f"Duplicate Server Mod Relationship database record: Guild: {member_after.guild.id} | User: {member_after.id}"
                        )
                        session.rollback()
                    except DBAPIError as err:
                        self.bot.log.exception(
                            f"Database error logging Server Mod Relationship to database. {sys.exc_info()[0].__name__}: {err}"
                        )
                        session.rollback()
                    except Exception as err:
                        self.bot.log.exception(
                            f"Generic Error logging Server Mod Relationship to database. {sys.exc_info()[0].__name__}: {err}"
                        )
                        session.rollback()
                    finally:
                        session.close()

            embeds = []
            # If the nickname changed
            if member_before.nick != member_after.nick:
                # Create the embed of info
                description = f"**Reason:** Nickname Changed\n"
                description += f"**Previous Nickname:** {member_before.nick}\n"
                description += f"**New Nickname:** {member_after.nick}"

                embed = discord.Embed(
                    color=0xDDE4E7,
                    timestamp=datetime.now(timezone.utc),
                    description=description[: self.bot.constants.embed_description_limit],
                )
                embed.set_author(
                    name=f"{member_after} ({member_after.id})",
                    icon_url=member_after.display_avatar.url,
                )
                embed.set_thumbnail(url=member_after.display_avatar.url)
                embeds.append(embed)

            # Member changed Guild Avatar
            member_before_asset = member_before.guild_avatar
            member_after_asset = member_after.guild_avatar
            if member_before_asset != member_after_asset:
                self.bot.log.debug(f"Updated Guild avatar for {member_before} ({member_before.id})")
                tags = ["avatar_change"]
                context = {"userid": f"{member_before.id}"}
                if member_after_asset:
                    # If they have a new pfp on the guild, get that
                    new_avatar = await self.bot.helpers.upload_image(member_after_asset)
                else:
                    # If they didn't set a new pfp, they are using their user avatar most likely, so show the display avatar
                    new_avatar = await self.bot.helpers.upload_image(member_after.display_avatar)

                if member_before_asset:
                    # If they had a guild pfp, try and get that (most likely will fail due to discord caching/deleting it quickly)
                    old_avatar = await self.bot.helpers.upload_image(member_before_asset)
                else:
                    # If they didn't have a guild pfp, they were using the display avatar likely from the user, so get that
                    old_avatar = await self.bot.helpers.upload_image(member_before.display_avatar)

                # Create the embed for avatar change
                embed = discord.Embed(color=0x7289DA, timestamp=datetime.now(timezone.utc))
                description = "**Reason:** Guild Avatar Changed"
                if old_avatar.secure_url:
                    description += "\nOld Avatar shown on **right**"
                    description += f"\n\nNOTE: _Old avatar link may decay in the near future._"
                    embed.set_thumbnail(url=old_avatar.secure_url)

                embed.set_author(name=f"{member_before} ({member_before.id})", icon_url=old_avatar.secure_url)

                if new_avatar.secure_url:
                    description += "\n\nNew Avatar shown **below**"
                    embed.set_image(url=new_avatar.secure_url)
                embed.description = description

                embeds.append(embed)

            # No change, do nothing
            if len(embeds) == 0:
                return

            # Get logs channel
            guild = member_after.guild
            all_channels = (
                [channel for channel in guild.text_channels]
                + [channel for channel in guild.threads]
                + [channel for channel in guild.forums]
            )

            logs = discord.utils.get(all_channels, name="name-change-logs")
            if logs and isinstance(logs, (discord.TextChannel, discord.Thread, discord.ForumChannel)):
                try:
                    await logs.send(f"{member_after} ({member_after.id})", embeds=embeds)
                except discord.Forbidden:
                    self.bot.log.warning(
                        f"Missing permissions to send in {logs.name} ({logs.id}) in guild {logs.guild.name} ({logs.guild.id})"
                    )

        except discord.HTTPException as err:
            self.bot.log.exception(
                f"Discord HTTP Error processing Member Update Event. {sys.exc_info()[0].__name__}: {err}"
            )
        except Exception as err:
            self.bot.log.exception(f"Error processing Member Update Event. {sys.exc_info()[0].__name__}: {err}")

    @commands.Cog.listener()
    async def on_member_ban(self, guild, member):
        """Watches audit log for ban events to log when it wasn't done via the bot."""

        # Sleep for some seconds to give audit log time to catch up
        await asyncio.sleep(60 * 0.25)
        session = self.bot.helpers.get_db_session()
        try:
            # The on member ban event only sends a guild and the member banned, but we don't know who did the banning.
            # We get the audit log, find the ban entry, and check if the person banning is same as bot, and if it is
            # then we skip logging, if not we want to log that, as it was a ban done outside the bot
            async for entry in guild.audit_logs(action=discord.AuditLogAction.ban, oldest_first=False):
                # Convert entry.user to mod variable, to avoid confusion
                mod = entry.user
                # If audit log user is not same as event member, skip
                if entry.target.id != member.id:
                    continue
                # If the user banned is by this bot then skip trying to log
                if entry.user.id == self.bot.user.id:
                    continue
                self.bot.log.info(
                    f"Found native ban entry in audit log from {mod} who banned {entry.target} in {guild}"
                )

                # Since the ban was done natively, we are assuming the user was not informed
                action_text = f"{entry.reason} | **Msg Delivered: No**"

                await self.bot.helpers.process_ban(session, member, mod, guild, entry.created_at, action_text)

                # Now that we found the audit log ban entry, break out so we don't process old entries
                break

        except discord.Forbidden as err:
            self.bot.log.warning(f"Missing permission to audit log for watching ban events for guild: {guild.id}.")
        except discord.HTTPException as err:
            self.bot.log.exception(
                f"Discord HTTP Error processing Member Ban Event. {sys.exc_info()[0].__name__}: {err}"
            )
        except Exception as err:
            self.bot.log.exception(f"Error processing Member Ban Event. {sys.exc_info()[0].__name__}: {err}")
        finally:
            session.close()

    @commands.Cog.listener()
    async def on_member_unban(self, guild, member):
        """Watches audit log for unban events to log when it wasn't done via the bot."""

        # Sleep for some seconds to give audit log time to catch up
        await asyncio.sleep(60 * 0.25)
        session = self.bot.helpers.get_db_session()
        try:
            # The on member ban event only sends a guild and the member banned, but we don't know who did the banning.
            # We get the audit log, find the ban entry, and check if the person banning is same as bot, and if it is
            # then we skip logging, if not we want to log that, as it was a ban done outside the bot
            async for entry in guild.audit_logs(action=discord.AuditLogAction.unban, oldest_first=False):
                # Convert entry.user to mod variable, to avoid confusion
                mod = entry.user
                # If audit log user is not same as event member, skip
                if entry.target.id != member.id:
                    continue
                # If the user banned is by this bot then skip trying to log
                if entry.user.id == self.bot.user.id:
                    continue
                self.bot.log.info(
                    f"Found native unban entry in audit log from {mod} who unbanned {entry.target} in {guild}"
                )

                # Since the unban was done natively, we are assuming the user was not informed
                action_text = f"{entry.reason} | **Msg Delivered: No**"

                await self.bot.helpers.process_unban(session, member, mod, guild, entry.created_at, action_text)

                # Now that we found the audit log ban entry, break out so we don't process old entries
                break

        except discord.Forbidden as err:
            self.bot.log.warning(f"Missing permission to audit log for watching unban events for guild: {guild.id}.")
        except discord.HTTPException as err:
            self.bot.log.exception(
                f"Discord HTTP Error processing Member Unban Event. {sys.exc_info()[0].__name__}: {err}"
            )
        except Exception as err:
            self.bot.log.exception(f"Error processing Member Unban Event. {sys.exc_info()[0].__name__}: {err}")
        finally:
            session.close()

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, raw_reaction_action_event):
        try:
            self.bot.log.debug(f"New Raw Reaction Event (Add), mID: {raw_reaction_action_event.message_id}")
            # If the member is this bot then skip trying to process
            if raw_reaction_action_event.user_id == self.bot.user.id:
                return

            # Set variables
            guild_id = raw_reaction_action_event.guild_id
            message_id = raw_reaction_action_event.message_id
            channel_id = raw_reaction_action_event.channel_id
            user_id = raw_reaction_action_event.user_id
            emoji = raw_reaction_action_event.emoji

            # Process/Check if need to for Role Assignment
            await self.bot.assignment.process_role(guild_id, channel_id, message_id, user_id, emoji)

            # Get channel ID's the command suggestion command goes to
            settings = self.bot.guild_settings.get(guild_id)
            try:
                suggestion_channel_id = settings.suggestion_channel
            except Exception as err:
                # If no suggestion_channel or settings returned from the DB settings then return
                return
            # Check if the reaction is on a suggestion, and if so adjust the voting:
            if channel_id == suggestion_channel_id:
                self.bot.log.debug(f"Reaction is in the suggestion channel. Suggestion mID: {message_id}")
                upvoted = False
                downvoted = False
                # Get the reactions
                upvote = self.bot.get_emoji(self.bot.constants.reactions["upvote"])
                downvote = self.bot.get_emoji(self.bot.constants.reactions["downvote"])
                if emoji.id == upvote.id:
                    upvoted = True
                elif emoji.id == downvote.id:
                    downvoted = True

                if upvoted or downvoted:
                    self.bot.log.debug(f"Suggestion was voted on: Suggestion mID: {message_id}")
                    # Get the database record
                    session = self.bot.helpers.get_db_session()
                    try:
                        suggestion_result = (
                            session.query(models.Suggestions).filter(models.Suggestions.message_id == message_id)
                        ).first()
                        if suggestion_result:
                            self.bot.log.debug(f"Found suggestion in the database. Suggestion mID: {message_id}")
                            if upvoted:
                                suggestion_result.upvotes = models.Suggestions.upvotes + 1
                                self.bot.log.debug(f"Suggestion Event: Upvote + 1. Suggestion mID: {message_id}")
                            elif downvoted:
                                suggestion_result.downvotes = models.Suggestions.downvotes + 1
                                self.bot.log.debug(f"Suggestion Event: Downvote + 1. Suggestion mID: {message_id}")
                            # Commit change to database
                            session.commit()
                            self.bot.log.debug(f"Committed change to database. Suggestion mID: {message_id}")
                        else:
                            self.bot.log.debug(f"Suggestion NOT in the database. Suggestion mID: {message_id}")
                    except DBAPIError as err:
                        self.bot.log.exception(
                            f"Error processing database query for adjusting suggestion votes. {sys.exc_info()[0].__name__}: {err}"
                        )
                        session.rollback()
                    except Exception as err:
                        self.bot.log.exception(
                            f"Unknown Error logging to database for adjusting suggestion votes. {sys.exc_info()[0].__name__}: {err}"
                        )
                    finally:
                        session.close()

        except discord.HTTPException as err:
            self.bot.log.exception(
                f"Discord HTTP Error processing Raw Reaction Add Event. {sys.exc_info()[0].__name__}: {err}"
            )
        except Exception as err:
            self.bot.log.exception(f"Error processing Raw Reaction Add Event. {sys.exc_info()[0].__name__}: {err}")

    @commands.Cog.listener()
    async def on_raw_reaction_remove(self, raw_reaction_action_event):
        try:
            self.bot.log.debug(f"New Raw Reaction Event (Remove), mID: {raw_reaction_action_event.message_id}")
            # If the member is this bot then skip trying to process
            if raw_reaction_action_event.user_id == self.bot.user.id:
                return

            # Set variables
            guild_id = raw_reaction_action_event.guild_id
            message_id = raw_reaction_action_event.message_id
            channel_id = raw_reaction_action_event.channel_id
            user_id = raw_reaction_action_event.user_id
            emoji = raw_reaction_action_event.emoji

            # Get channel ID's the command suggestion command goes to
            settings = self.bot.guild_settings.get(guild_id)
            try:
                suggestion_channel_id = settings.suggestion_channel
            except Exception as err:
                # If no suggestion_channel or settings returned from the DB settings then return
                return
            # Check if the reaction is on a suggestion, and if so adjust the voting:
            if channel_id == suggestion_channel_id:
                self.bot.log.debug(f"Reaction is in the suggestion channel. Suggestion mID: {message_id}")
                upvoted = False
                downvoted = False
                # Get the reactions
                upvote = self.bot.get_emoji(self.bot.constants.reactions["upvote"])
                downvote = self.bot.get_emoji(self.bot.constants.reactions["downvote"])
                if emoji.id == upvote.id:
                    upvoted = True
                elif emoji.id == downvote.id:
                    downvoted = True

                if upvoted or downvoted:
                    self.bot.log.debug(f"Suggestion was voted on: Suggestion mID: {message_id}")
                    # Get the database record
                    session = self.bot.helpers.get_db_session()
                    try:
                        suggestion_result = (
                            session.query(models.Suggestions).filter(models.Suggestions.message_id == message_id)
                        ).first()
                        if suggestion_result:
                            self.bot.log.debug(f"Found suggestion in the database. Suggestion mID: {message_id}")
                            if upvoted:
                                suggestion_result.upvotes = models.Suggestions.upvotes - 1
                                self.bot.log.debug(f"Suggestion Event: Upvote - 1. Suggestion mID: {message_id}")
                            elif downvoted:
                                suggestion_result.downvotes = models.Suggestions.downvotes - 1
                                self.bot.log.debug(f"Suggestion Event: Downvote - 1. Suggestion mID: {message_id}")
                            # Commit change to database
                            session.commit()
                            self.bot.log.debug(f"Committed change to database. Suggestion mID: {message_id}")
                        else:
                            self.bot.log.debug(f"Suggestion NOT in the database. Suggestion mID: {message_id}")
                    except DBAPIError as err:
                        self.bot.log.exception(
                            f"Error processing database query for adjusting suggestion votes. {sys.exc_info()[0].__name__}: {err}"
                        )
                        session.rollback()
                    except Exception as err:
                        self.bot.log.exception(
                            f"Unknown Error logging to database for adjusting suggestion votes. {sys.exc_info()[0].__name__}: {err}"
                        )
                    finally:
                        session.close()

        except discord.HTTPException as err:
            self.bot.log.exception(
                f"Discord HTTP Error processing Raw Reaction Remove Event. {sys.exc_info()[0].__name__}: {err}"
            )
        except Exception as err:
            self.bot.log.exception(f"Error processing Raw Reaction Remove Event. {sys.exc_info()[0].__name__}: {err}")

    @commands.Cog.listener()
    async def on_reaction_add(self, reaction, member):
        try:
            # If the member is this bot then skip trying to log
            if self.bot.user.id == member.id:
                return

            guild = reaction.message.guild
            if not guild:
                return

            if isinstance(reaction.message.channel, discord.Object):
                self.bot.log.debug(f"reaction message.channel is a discord.Object, trying to fix")
                channel = guild.get_channel_or_thread(reaction.message.channel.id)
            else:
                channel = reaction.message.channel

            # Get the logs channel
            all_channels = (
                [channel for channel in guild.text_channels]
                + [channel for channel in guild.threads]
                + [channel for channel in guild.forums]
            )
            logs = discord.utils.get(all_channels, name="reaction-logs")
            if not logs or not isinstance(logs, (discord.TextChannel, discord.Thread, discord.ForumChannel)):
                return

            # Create the embed of info
            description = f"**Reason:** A reaction was added\n"
            if isinstance(channel, discord.abc.GuildChannel) and channel.category:
                description += f"**Category:** {channel.category.name} ({channel.category.id})\n"
            if isinstance(channel, discord.Thread) and channel.parent:
                description += f"**Parent:** {channel.parent.name} ({channel.parent.id})\n"
            if isinstance(channel, discord.PartialMessageable):
                description += f"**Channel:** ({channel.id})\n"
            else:
                description += f"**Channel:** #{channel.name} ({channel.id})\n"
            description += f"**Message:** ({reaction.message.id})\n"
            # If the emoji is a unicode emoji then it is a string
            if type(reaction.emoji) is str:
                description += f"**Emoji:** {reaction.emoji}\n"
            else:
                description += f"**Emoji:** {reaction.emoji.name} ({reaction.emoji.id})\n"
            # If the bot isn't in the server the emoji is from, it gets a PartialEmoji type and we lack guild info
            if isinstance(reaction.emoji, discord.Emoji):
                description += f"**Source Guild:** {reaction.emoji.guild.name} ({reaction.emoji.guild.id})\n"
            description += f"**Message Link:** {reaction.message.jump_url}"

            embed = discord.Embed(
                color=0x64997D,
                timestamp=datetime.now(timezone.utc),
                description=description[: self.bot.constants.embed_description_limit],
            )
            embed.set_author(name=f"{member} ({member.id})", icon_url=member.display_avatar.url)
            # If the emoji is a unicode emoji then it is a string
            if type(reaction.emoji) is not str:
                embed.set_thumbnail(url=reaction.emoji.url)

            try:
                await logs.send(f"{member} ({member.id})", embed=embed)
            except discord.Forbidden:
                self.bot.log.warning(
                    f"Missing permissions to send in {logs.name} ({logs.id}) in guild {guild.name} ({guild.id})"
                )

        except discord.HTTPException as err:
            self.bot.log.warning(
                f"Discord HTTP Error processing Reaction Add Event. {sys.exc_info()[0].__name__}: {err}"
            )
        except Exception as err:
            self.bot.log.exception(f"Error processing Reaction Add Event. {sys.exc_info()[0].__name__}: {err}")

    @commands.Cog.listener()
    async def on_reaction_remove(self, reaction, member):
        try:
            # If the member is this bot then skip trying to log
            if self.bot.user.id == member.id:
                return

            guild = reaction.message.guild
            if not guild:
                return

            if isinstance(reaction.message.channel, discord.Object):
                self.bot.log.debug(f"reaction message.channel is a discord.Object, trying to fix")
                channel = guild.get_channel_or_thread(reaction.message.channel.id)
            else:
                channel = reaction.message.channel

            # Get the logs channel
            logs = discord.utils.get(guild.text_channels, name="reaction-logs")
            if not logs:
                return

            # Create the embed of info
            description = f"**Reason:** A reaction was removed\n"
            if isinstance(channel, discord.abc.GuildChannel) and channel.category:
                description += f"**Category:** {channel.category.name} ({channel.category.id})\n"
            if isinstance(channel, discord.Thread) and channel.parent:
                description += f"**Parent:** {channel.parent.name} ({channel.parent.id})\n"
            description += f"**Channel:** #{channel.name} ({channel.id})\n"
            description += f"**Message:** ({reaction.message.id})\n"
            # If the emoji is a unicode emoji then it is a string
            if type(reaction.emoji) is str:
                description += f"**Emoji:** {reaction.emoji}\n"
            else:
                description += f"**Emoji:** {reaction.emoji.name} ({reaction.emoji.id})\n"
            # If the bot isn't in the server the emoji is from, it gets a PartialEmoji type and we lack guild info
            if isinstance(reaction.emoji, discord.Emoji):
                description += f"**Source Guild:** {reaction.emoji.guild.name} ({reaction.emoji.guild.id})\n"
            description += f"**Message Link:** {reaction.message.jump_url}"

            embed = discord.Embed(
                color=0xED8F45,
                timestamp=datetime.now(timezone.utc),
                description=description[: self.bot.constants.embed_description_limit],
            )
            embed.set_author(name=f"{member} ({member.id})", icon_url=member.display_avatar.url)
            # If the emoji is a unicode emoji then it is a string
            if type(reaction.emoji) is not str:
                embed.set_thumbnail(url=reaction.emoji.url)

            try:
                await logs.send(f"{member} ({member.id})", embed=embed)
            except discord.Forbidden:
                self.bot.log.warning(
                    f"Missing permissions to send in {logs.name} ({logs.id}) in guild {guild.name} ({guild.id})"
                )

        except discord.HTTPException as err:
            self.bot.log.warning(
                f"Discord HTTP Error processing Reaction Remove Event. {sys.exc_info()[0].__name__}: {err}"
            )
        except Exception as err:
            self.bot.log.exception(f"Error processing Reaction Remove Event. {sys.exc_info()[0].__name__}: {err}")

    # TO DO - Rewrite this. The flow is overly messy.
    @commands.Cog.listener()
    async def on_voice_state_update(
        self, member, voice_state_before: discord.VoiceState, voice_state_after: discord.VoiceState
    ):
        try:
            # If the member is this bot then skip trying to log
            if self.bot.user.id == member.id:
                return

            # Get the guild of the before event
            before_guild = None
            try:
                temp = voice_state_before.channel.members
                before_guild = voice_state_before.channel.guild
            except AttributeError as err:
                # Likely a broken channel/new channel the Library can't handle, pass to ignore
                pass
            except Exception as err:
                self.bot.log.exception(f"Error getting VC Details. {sys.exc_info()[0].__name__}: {err}")

            # Get the guild of the after event
            after_guild = None
            try:
                temp = voice_state_after.channel.members
                after_guild = voice_state_after.channel.guild
            except AttributeError as err:
                # Likely a broken channel/new channel the Library can't handle, pass to ignore
                pass
            except Exception as err:
                self.bot.log.exception(f"Error getting VC Details. {sys.exc_info()[0].__name__}: {err}")

            # Set the event types
            reason = None
            event_details = None
            old_channel_members_ids = None
            new_channel_members_ids = None
            # If before_guild is none, but after_guild exists, they joined a channel
            if before_guild is None and after_guild:
                # New channel
                (new_channel_members, new_channel_members_ids,) = await self.bot.helpers.get_voice_channel_members(
                    member, voice_state_after.channel.members, include_self=False
                )

                reason = "Joined Voice Channel"
                event_details = f"**New Channel:** #{voice_state_after.channel.name} ({voice_state_after.channel.id})\n"
                event_details += f"**New Channel Users:** {new_channel_members}\n"

                # Auto VC Management
                await self.bot.helpers.CreateVC(member, voice_state_after)

            # If after_guild is none, but before_guild exists, they left a channel or stopped go live streaming
            elif after_guild is None and before_guild:
                # Left Voice Channel
                (old_channel_members, old_channel_members_ids,) = await self.bot.helpers.get_voice_channel_members(
                    member, voice_state_before.channel.members, include_self=True
                )

                reason = "Left Voice Channel"
                event_details = (
                    f"**Old Channel:** #{voice_state_before.channel.name} ({voice_state_before.channel.id})\n"
                )
                event_details += f"**Old Channel Users:** {old_channel_members}\n"

                # ~~~~~~~~~~~~~~~~~~~~~~~~
                # Stopped Go-Live Streaming
                golive_reason = "Stopped Go-Live Streaming"
                golive_description = f"**Reason:** {golive_reason}\n"
                golive_description += (
                    f"**Channel:** #{voice_state_before.channel.name} ({voice_state_before.channel.id})\n"
                )
                golive_description += f"**Channel Users:** {old_channel_members}\n"

                embed = await self._create_embed(member=member, description=golive_description)

                await self._send_log_to_channel(
                    guild=before_guild,
                    log_chan_name="activity-logs",
                    text=f"{member} ({member.id})",
                    embed=embed,
                )

                # Auto VC Management
                await self.bot.helpers.RemoveAutoVC(voice_state_before.channel)

            # If before_guild and after_guild exists.
            # Moved VC or started or maybe stopped Go Live streaming
            elif before_guild and after_guild:
                # Moved voice channels
                if voice_state_before.channel.id != voice_state_after.channel.id:
                    # Old channel
                    (old_channel_members, old_channel_members_ids,) = await self.bot.helpers.get_voice_channel_members(
                        member, voice_state_before.channel.members, include_self=True
                    )
                    # New channel
                    (new_channel_members, new_channel_members_ids,) = await self.bot.helpers.get_voice_channel_members(
                        member, voice_state_after.channel.members, include_self=False
                    )

                    reason = "Moved Voice Channel"
                    event_details = (
                        f"**Old Channel:** #{voice_state_before.channel.name} ({voice_state_before.channel.id})\n"
                    )
                    event_details += f"**Old Channel Users:** {old_channel_members}\n"
                    event_details += (
                        f"**New Channel:** #{voice_state_after.channel.name} ({voice_state_after.channel.id})\n"
                    )
                    event_details += f"**New Channel Users:** {new_channel_members}\n"

                    # Auto VC Management
                    await self.bot.helpers.CreateVC(member, voice_state_after)
                    await self.bot.helpers.RemoveAutoVC(voice_state_before.channel)

                # No Go Live stream before, but go live after = start stream
                if not voice_state_before.self_stream and voice_state_after.self_stream:
                    (channel_members, channel_members_ids,) = await self.bot.helpers.get_voice_channel_members(
                        member, voice_state_after.channel.members, include_self=False
                    )

                    reason = "Started Go-Live Streaming"
                    description = f"**Reason:** {reason}\n"
                    description += f"**Channel:** #{voice_state_after.channel.name} ({voice_state_after.channel.id})\n"
                    description += f"**Channel Users:** {channel_members}\n"

                    embed = await self._create_embed(
                        member=member, description=description[: self.bot.constants.embed_description_limit]
                    )

                    await self._send_log_to_channel(
                        guild=after_guild,
                        log_chan_name="activity-logs",
                        text=f"{member} ({member.id})",
                        embed=embed,
                    )

                # No Go Live stream after, but go live before = stop stream
                elif voice_state_before.self_stream and not voice_state_after.self_stream:
                    (channel_members, channel_members_ids,) = await self.bot.helpers.get_voice_channel_members(
                        member, voice_state_before.channel.members, include_self=False
                    )

                    reason = "Stopped Go-Live Streaming"
                    description = f"**Reason:** {reason}\n"
                    description += (
                        f"**Channel:** #{voice_state_before.channel.name} ({voice_state_before.channel.id})\n"
                    )
                    description += f"**Channel Users:** {channel_members}\n"

                    embed = await self._create_embed(
                        member=member, description=description[: self.bot.constants.embed_description_limit]
                    )

                    await self._send_log_to_channel(
                        guild=after_guild,
                        log_chan_name="activity-logs",
                        text=f"{member} ({member.id})",
                        embed=embed,
                    )
            else:
                return

            # If it's not a Voice Channel Join/Move/Part then don't continue w/ this logging
            if not event_details:
                return

            # Log to the database
            session = self.bot.helpers.get_db_session()
            try:
                # Get the DB profile for the guild
                db_guild = await self.bot.helpers.db_get_guild(session, member.guild.id)
                # Get the DB profile for the user
                db_user = await self.bot.helpers.db_get_user(session, member.id)

                old_chan = None
                if voice_state_before is not None and voice_state_before.channel is not None:
                    old_chan = voice_state_before.channel

                new_chan = None
                if voice_state_after is not None and voice_state_after.channel is not None:
                    new_chan = voice_state_after.channel

                db_voice = models.VoiceLog(
                    user=db_user,
                    server=db_guild,
                    event_type=reason,
                    # Old Channel
                    vc_old_name=old_chan.name if old_chan is not None else None,
                    vc_old_id=old_chan.id if old_chan is not None else None,
                    vc_old_users_ids=old_channel_members_ids if old_chan is not None else None,
                    # New channel
                    vc_new_name=new_chan.name if new_chan is not None else None,
                    vc_new_id=new_chan.id if new_chan is not None else None,
                    vc_new_users_ids=new_channel_members_ids if new_chan is not None else None,
                )
                session.add(db_voice)
                session.commit()
                self.bot.log.debug(f"Voice Log to database: Guild: {member.guild.id}, User: {member.id}")
            except DBAPIError as err:
                self.bot.log.exception(
                    f"Database error logging voice state update to database. {sys.exc_info()[0].__name__}: {err}"
                )
                session.rollback()
            except Exception as err:
                self.bot.log.exception(
                    f"Generic Error logging Voice State Update Event to database. {sys.exc_info()[0].__name__}: {err}"
                )
            finally:
                session.close()

            # Create the embed of info
            description = f"**Reason:** {reason}\n"
            description += event_details
            embed = await self._create_embed(
                member=member, description=description[: self.bot.constants.embed_description_limit]
            )

            if before_guild:
                await self._send_log_to_channel(
                    guild=before_guild,
                    log_chan_name="voice-logs",
                    text=f"{member} ({member.id})",
                    embed=embed,
                )

            elif after_guild:
                await self._send_log_to_channel(
                    guild=after_guild,
                    log_chan_name="voice-logs",
                    text=f"{member} ({member.id})",
                    embed=embed,
                )

        except discord.HTTPException as err:
            self.bot.log.warning(
                f"Discord HTTP Error processing Voice State Update Event. {sys.exc_info()[0].__name__}: {err}"
            )
        except Exception as err:
            self.bot.log.exception(f"Error processing Voice State Update Event. {sys.exc_info()[0].__name__}: {err}")

    async def _create_embed(self, description, member):
        embed = discord.Embed(
            color=0xFFC0CB,
            timestamp=datetime.now(timezone.utc),
            description=description[: self.bot.constants.embed_description_limit],
        )
        embed.set_author(name=f"{member} ({member.id})", icon_url=member.display_avatar.url)
        return embed

    async def _send_log_to_channel(self, guild, log_chan_name, text, embed):
        logs = None
        try:
            all_channels = (
                [channel for channel in guild.text_channels]
                + [channel for channel in guild.threads]
                + [channel for channel in guild.forums]
            )
            logs = discord.utils.get(all_channels, name=log_chan_name)
            if not logs or not isinstance(logs, (discord.TextChannel, discord.Thread, discord.ForumChannel)):
                return
            return await logs.send(text, embed=embed)
        except discord.Forbidden:
            self.bot.log.debug(
                f"Missing permissions to send in logs channel. Guild: {guild.id} | Channel: {logs.id if logs else log_chan_name}"
            )


async def setup(bot):
    await bot.add_cog(Events(bot))
