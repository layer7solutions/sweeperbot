class Image(object):
    def __init__(
        self,
        public_id: str = None,
        version: str = None,
        url: str = None,
        secure_url: str = None,
        width: int = None,
        height: int = None,
        size: int = 0,
        created_at: str = None,
        signature: str = None,
    ):
        """Represents an Image."""
        self.public_id: str = public_id
        self.version: str = version
        self.url: str = url
        self.secure_url: str = secure_url
        self.width: int = width
        self.height: int = height
        self.size: int = size
        self.created_at: str = created_at
        self.signature: str = signature

    def __str__(self):
        return f"public_id: {self.public_id} | version: {self.version} | secure_url: {self.secure_url} | width: {self.width} | height: {self.height} | size: {self.size} bytes | created_at: {self.created_at} | signature: {self.signature}"
