import asyncio
import os
import socket
import sys
from datetime import datetime, timezone

import psutil
from db import models
from discord import Game
from discord.ext import tasks
from sqlalchemy.exc import DBAPIError


class Tasks:
    def __init__(self, bot):
        self.bot = bot
        self.redis = self.bot.helpers.redis
        self.all_tasks = []
        self.loaded = None
        self.positive_inf = None
        self.negative_inf = None
        self.not_a_number = None

    async def start_tasks(self):
        # Internal check if we've already loaded and started the tasks
        if self.loaded:
            self.bot.log.debug(f"Tasks: Already loaded start_tasks, skipping")
            return

        # Log Server Stats
        self.log_server_stats.start()
        self.bot.log.debug(f"Tasks: Loaded: log_server_stats")

        # Change activity status
        self.change_activity_status.start()
        self.bot.log.debug(f"Tasks: Loaded: task_activity_status")

        # Load antispam services from db
        self.load_antispam_services_from_db.start()
        self.bot.log.debug(f"Tasks: Loaded: task_load_antispam_services")

        # Log System Health
        self.log_system_health.start()
        self.bot.log.debug(f"Tasks: Loaded: task_system_health")

        # Perform Redis discord.messages.most_freq_used Cleaning
        self.redis_most_freq_used_cleaning.start()
        self.bot.log.debug(f"Tasks: Loaded: task_most_freq_used")

        # Updates Mod Mail Category Counts
        self.modmail_update_cat_counts.start()
        self.bot.log.debug(f"Tasks: Loaded: modmail_update_cat_counts")

        # Update Owner Info
        self.update_owner_info.start()
        self.bot.log.debug(f"Tasks: Loaded: update_owner_info")

        # Clean Guild Invites
        self.clean_guild_invites.start()
        self.bot.log.debug(f"Tasks: Loaded: clean_guild_invites")

        # Clean Guild Auto VCs
        self.clean_auto_vcs.start()
        self.bot.log.debug(f"Tasks: Loaded: clean_auto_vcs")

        # All done
        self.bot.log.info(f"Tasks: Done loading all tasks | Bot Startup: {self.bot.started_time.timestamp()}")
        self.loaded = True

    @tasks.loop(minutes=5)
    async def log_server_stats(self):
        for guild in self.bot.guilds:
            # If the guild is unavailable such as during an outage, skip it
            if guild and guild.unavailable:
                continue
            # Get DB Session
            session = self.bot.helpers.get_db_session()
            try:
                # Check if there is a guild in the database already
                db_guild = session.query(models.Server).filter(models.Server.discord_id == guild.id).first()
                if not db_guild:
                    db_guild = await self.bot.helpers.db_add_new_guild(session, guild.id, guild)

                # Set default options in case the we can't get more accurate info:
                member_count = guild.member_count
                presence_count = 0  # Sadly only way to get any remotely accurate presence is via invite code
                voice_channels = guild.voice_channels + guild.stage_channels
                total_voice_users = 0
                total_voice_channels = 0
                used_voice_channels = 0
                for vc in voice_channels:
                    try:
                        total_voice_users += len(vc.members)
                        total_voice_channels += 1
                        if len(vc.members) > 0:
                            used_voice_channels += 1
                    except AttributeError as err:
                        # Likely a broken channel/new channel the Library can't handle, pass to ignore
                        pass
                    except Exception as err:
                        self.bot.log.exception(f"Error getting VC Details. {sys.exc_info()[0].__name__}: {err}")

                # Get guild invite
                invite = await self.bot.helpers.get_guild_invite(guild)

                if invite:
                    member_count = invite.approximate_member_count
                    presence_count = invite.approximate_presence_count

                data = {
                    "total_users": member_count,
                    "concurrent_users": presence_count,
                    "total_voice_users": total_voice_users,
                    "total_voice_channels": total_voice_channels,
                    "used_voice_channels": used_voice_channels,
                }
                new_stat = models.Statistic(server=db_guild, **data)
                session.add(new_stat)
                session.commit()
                self.bot.log.debug(f"Logged server stats for: {guild.name} ({guild.id}) | Data: {data}")

            except DBAPIError as err:
                self.bot.log.exception(f"Database Error logging Server Stats. {sys.exc_info()[0].__name__}: {err}")
                session.rollback()
            except asyncio.CancelledError as err:
                pass
            except Exception as err:
                self.bot.log.exception(f"Generic Error logging Server Stats. {sys.exc_info()[0].__name__}: {err}")
            finally:
                # Close this database session
                session.close()

    @log_server_stats.before_loop
    async def before_log_server_stats(self):
        # Wait until bot is ready
        await self.bot.wait_until_ready()

    @tasks.loop(minutes=20)
    async def change_activity_status(self):
        for guild in self.bot.guilds:
            # If the guild is unavailable such as during an outage, skip it
            if guild and guild.unavailable:
                continue

            try:
                settings = self.bot.guild_settings.get(guild.id)
                if not settings:
                    self.bot.log.debug(f"Tasks (activity status): No settings for guild: {guild.id}")
                    continue
                activity_status = settings.activity_status
                activity_status_enabled = settings.activity_status_enabled

                if not activity_status_enabled:
                    continue

                # If there is at least one activity status text added
                if len(activity_status) > 0:
                    # Get the max index id
                    as_max_idx = len(activity_status) - 1
                    # Get what index is last used
                    try:
                        as_last_idx = self.bot.activity_index[guild.id]
                    except KeyError:
                        # If nothing set, usually due to bot restart, set to one below 0, indexes start at 0
                        as_last_idx = -1
                    # If incrementing the index goes above max, then set index to 0
                    if as_last_idx + 1 > as_max_idx:
                        as_idx = 0
                    # Otherwise increase the index
                    else:
                        as_idx = as_last_idx + 1
                    # Get the activity status text
                    activity_text = str(activity_status[as_idx])
                    # If length of stripping the text is blank, skip trying to set the status, discord would reject
                    if len(activity_text.strip()) == 0:
                        print(f"{activity_text.strip()}")
                        continue
                    # Set the activity status
                    try:
                        await self.bot.change_presence(activity=Game(name=f"{activity_text}"))
                        self.bot.log.debug(f"Tasks: Set Activity Text to: '{activity_text}' from Guild: {guild.id}")
                        # Update the index
                        self.bot.activity_index[guild.id] = as_idx
                    except Exception as err:
                        self.bot.log.exception(
                            f"Tasks: Failed to set presence info. {sys.exc_info()[0].__name__}: {err}"
                        )

            except asyncio.CancelledError as err:
                pass
            except Exception as err:
                self.bot.log.exception(
                    f"Tasks: Generic Error setting presence info. {sys.exc_info()[0].__name__}: {err}"
                )

    @change_activity_status.before_loop
    async def before_change_activity_status(self):
        # Wait until bot is ready
        await self.bot.wait_until_ready()

    @tasks.loop(minutes=10)
    async def load_antispam_services_from_db(self):
        session = self.bot.helpers.get_db_session()
        try:
            self.bot.antispam.antispam_services = (
                session.query(models.AntiSpamServices).filter(models.AntiSpamServices.enabled.is_(True)).all()
            )
            self.bot.log.debug(f"Loaded AntiSpam Services from DB")
        except DBAPIError as err:
            self.bot.log.exception(f"Database Error loading AntiSpam Module. {sys.exc_info()[0].__name__}: {err}")
            session.rollback()
        except asyncio.CancelledError as err:
            pass
        finally:
            # Close this database session
            session.close()

    @load_antispam_services_from_db.before_loop
    async def before_load_antispam_services_from_db(self):
        # Wait until bot is ready
        await self.bot.wait_until_ready()

    @tasks.loop(seconds=10)
    async def log_system_health(self):
        # Set positive, negative and not a number values so we don't have to create them each time
        self.positive_inf = float("inf")
        self.negative_inf = float("-inf")
        self.not_a_number = float("nan")

        # Get DB Session
        session = self.bot.helpers.get_db_session()
        try:
            # Get the DB profile for the user
            db_user = await self.bot.helpers.db_get_user(session, self.bot.user.id)

            # API Latency (ms) & check if it's infinity and zero it ot
            api_latency = self.bot.latency * 1000
            if api_latency in (self.positive_inf, self.negative_inf, self.not_a_number):
                api_latency = 0
            # All shard's latencies
            # TO DO - Log all shard latencies
            # https://discordpy.readthedocs.io/en/latest/api.html#discord.AutoShardedClient.latencies
            all_shards_latencies = self.bot.latencies

            # Get cpu and memory usage
            process = psutil.Process(os.getpid())
            # Needs to be called twice to get accurate values
            # https://psutil.readthedocs.io/en/latest/#psutil.Process.cpu_percent
            cpu_usage = process.cpu_percent(interval=None)
            await asyncio.sleep(0.5)
            cpu_usage = process.cpu_percent(interval=None)
            try:
                memory_usage = process.memory_full_info().uss / 1024 ** 2
            except psutil.AccessDenied:
                memory_usage = 0.00

            # Get bot uptime in seconds
            now = datetime.now(timezone.utc).timestamp()
            uptime_sec = now - self.bot.started_time.timestamp()

            # Data being logged
            # TO DO - Add Redis latency? Not sure polling Redis that frequently is good idea or needed
            data = {
                "api_latency_ms": api_latency,
                "memory_mib": memory_usage,
                "cpu_usage_perc": cpu_usage,
                "total_guilds": len(self.bot.guilds),
                "total_shard_count": self.bot.shard_count,
                "uptime_sec": uptime_sec,
                "bot_version": self.bot.version,
                "hostname": socket.gethostname(),
            }
            new_stat = models.SystemHealth(bot=db_user, **data)
            session.add(new_stat)
            session.commit()
            # self.bot.log.debug(f"Logged System Health. Data: {data}")

        except DBAPIError as err:
            self.bot.log.exception(f"Database Error logging System Health Stats. {sys.exc_info()[0].__name__}: {err}")
            session.rollback()
        except asyncio.CancelledError as err:
            pass
        except Exception as err:
            self.bot.log.exception(f"Generic Error logging System Health Stats. {sys.exc_info()[0].__name__}: {err}")
        finally:
            # Close this database session
            session.close()

    @log_system_health.before_loop
    async def before_log_system_health(self):
        # Wait until bot is ready
        await self.bot.wait_until_ready()

    @tasks.loop(minutes=60)
    async def redis_most_freq_used_cleaning(self):
        try:
            # 1. Gets all of the channel keys we're tracking
            most_freq_used_key_name = f"discord.messages.channels.most_freq_used"
            all_most_freq_used_keys = self.redis.zrange(name=most_freq_used_key_name, start=0, end=-1)
            self.bot.log.info(f"redis_most_freq_used: About to clean {len(all_most_freq_used_keys)} total channel keys")
            # 2. Now select to remove elements in the Sorted Set that are older than x period (and get that TTL period)
            max_ttl_age_of_elements_sec = int(self.redis.get(f"discord.messages.channels.max_ttl_age_of_elements_sec"))
            utc_epoch_now = datetime.now(timezone.utc).timestamp()
            elements_older_than = utc_epoch_now - max_ttl_age_of_elements_sec
            # 3. Start a pipeline to tell it to remove elements from all keys older than TTL
            pipeline = self.redis.pipeline()
            for channel_key in all_most_freq_used_keys:
                pipeline.zremrangebyscore(name=channel_key, min="-inf", max=elements_older_than)

            # 4. All done
            results = pipeline.execute()
            cleaned = [item for item in results if item != 0]
            cleaned.sort(reverse=True)
            self.bot.log.info(
                f"redis_most_freq_used: Cleaned Redis Most Frequently Used data. Total cleaned: {len(cleaned)} | results: {cleaned}"
            )

        except asyncio.CancelledError as err:
            pass
        except Exception as err:
            self.bot.log.exception(
                f"Generic Error cleaning Redis Most Frequently Used Channel data. {sys.exc_info()[0].__name__}: {err}"
            )

    @redis_most_freq_used_cleaning.before_loop
    async def before_redis_most_freq_used_cleaning(self):
        # Wait until bot is ready
        await self.bot.wait_until_ready()

    @tasks.loop(minutes=11)
    async def modmail_update_cat_counts(self):
        try:
            # Get the ModMail Cog
            mmcog = self.bot.get_cog("ModMail")
            # If the cog exists and mod mail is loaded
            if mmcog and mmcog.modmail_loaded:
                # Get the ModMail Guild
                mm_guild = self.bot.get_guild(mmcog.modmail_server_id)

                # Adjust unanswered category
                if mm_guild and mmcog.modmail_unanswered_cat:
                    # Get the category by getting a fresh channel object, otherwise it'll use our cashed object values
                    category = mm_guild.get_channel(mmcog.modmail_unanswered_cat.id)
                    count = len(category.text_channels)
                    cat_type = "Unanswered"
                    await category.edit(name=f"{cat_type} {count}/50")
                    self.bot.log.debug(f"ModMail Task: Updated Cat Counts: New Name: {cat_type} {count}/50")

                # Adjust in progress category
                if mm_guild and mmcog.modmail_in_progress_cat:
                    # Get the category by getting a fresh channel object, otherwise it'll use our cashed object values
                    category = mm_guild.get_channel(mmcog.modmail_in_progress_cat.id)
                    count = len(category.text_channels)
                    cat_type = "In Progress"
                    await category.edit(name=f"{cat_type} {count}/50")
                    self.bot.log.debug(f"ModMail Task: Updated Cat Counts: New Name: {cat_type} {count}/50")

        except Exception as err:
            self.bot.log.exception(
                f"Generic Error updating modmail category counts. {sys.exc_info()[0].__name__}: {err}"
            )

    @modmail_update_cat_counts.before_loop
    async def before_modmail_update_cat_counts(self):
        # Wait until bot is ready
        await self.bot.wait_until_ready()

    @tasks.loop(minutes=60)
    async def update_owner_info(self):
        try:
            # Sets bot owner information
            app_info = await self.bot.application_info()

            if app_info.team:
                self.bot.owner = app_info.team.members
                self.bot.owner_ids = set([member.id for member in app_info.team.members])
                self.bot.log.debug(f"AppInfo found Team, so setting for team of owners.")
            else:
                self.bot.owner = app_info.owner
                self.bot.owner_id = app_info.owner.id
                self.bot.log.debug(f"AppInfo is missing Team, so setting for single owner.")
            self.bot.log.info(f"Bot Owners: {self.bot.owner_ids if self.bot.owner_ids else self.bot.owner_id}")

        except asyncio.CancelledError as err:
            pass
        except Exception as err:
            self.bot.log.exception(f"Generic Error setting owner info. {sys.exc_info()[0].__name__}: {err}")

    @update_owner_info.before_loop
    async def before_update_owner_info(self):
        # Wait until bot is ready
        await self.bot.wait_until_ready()

    @tasks.loop(hours=6)
    async def clean_guild_invites(self):
        try:
            for guild in self.bot.guilds:
                # If the guild is unavailable such as during an outage,
                #   or bot is missing manage guild permission, skip it
                if guild and (guild.unavailable or not guild.me.guild_permissions.manage_guild):
                    continue
                else:
                    less_than_eq_uses = 5
                    older_than_hours = 8
                    only_temporary = False
                    total_invites_above = 300
                    # Currently, this will delete any invites, that have 5 or less uses that are older than 8 hours,
                    # regardless if temporary or not and the total outstanding invites is over 300
                    await self.bot.helpers.clean_guild_invites(
                        guild,
                        less_than_eq_uses,
                        older_than_hours,
                        only_temporary,
                        total_invites_above,
                    )

        except asyncio.CancelledError as err:
            pass
        except Exception as err:
            self.bot.log.exception(f"Generic Error auto cleaning guild invites. {sys.exc_info()[0].__name__}: {err}")

    @clean_guild_invites.before_loop
    async def before_clean_guild_invites(self):
        # Wait until bot is ready
        await self.bot.wait_until_ready()

    @tasks.loop(minutes=1)
    async def clean_auto_vcs(self):
        try:
            for guild in self.bot.guilds:
                # If the guild is unavailable such as during an outage, skip it
                if guild and guild.unavailable:
                    continue

                self.bot.log.info(f"Performing Auto VC Cleaning for guild {guild.id}")

                for channel in guild.voice_channels:
                    await self.bot.helpers.RemoveAutoVC(channel)

        except asyncio.CancelledError as err:
            pass
        except Exception as err:
            self.bot.log.exception(f"Generic Error auto cleaning voice channels. {sys.exc_info()[0].__name__}: {err}")

    @clean_auto_vcs.before_loop
    async def before_clean_auto_vcs(self):
        # Wait until bot is ready
        await self.bot.wait_until_ready()

    async def cancel_all_tasks(self):
        try:
            if self.log_server_stats.is_running():
                self.bot.log.info(f"Tasks: Attempting to stop: log_server_stats")
                self.log_server_stats.stop()
            if self.change_activity_status.is_running():
                self.bot.log.info(f"Tasks: Attempting to stop: change_activity_status")
                self.change_activity_status.stop()
            if self.load_antispam_services_from_db.is_running():
                self.bot.log.info(f"Tasks: Attempting to stop: load_antispam_services_from_db")
                self.load_antispam_services_from_db.stop()
            if self.log_system_health.is_running():
                self.bot.log.info(f"Tasks: Attempting to stop: log_system_health")
                self.log_system_health.stop()
            if self.redis_most_freq_used_cleaning.is_running():
                self.bot.log.info(f"Tasks: Attempting to stop: redis_most_freq_used_cleaning")
                self.redis_most_freq_used_cleaning.stop()
            if self.modmail_update_cat_counts.is_running():
                self.bot.log.info(f"Tasks: Attempting to stop: modmail_update_cat_counts")
                self.modmail_update_cat_counts.stop()
            if self.update_owner_info.is_running():
                self.bot.log.info(f"Tasks: Attempting to stop: update_owner_info")
                self.update_owner_info.stop()
            if self.clean_guild_invites.is_running():
                self.bot.log.info(f"Tasks: Attempting to stop: clean_guild_invites")
                self.clean_guild_invites.stop()
            if self.clean_auto_vcs.is_running():
                self.bot.log.info(f"Tasks: Attempting to stop: clean_auto_vcs")
                self.clean_auto_vcs.stop()

        except Exception as err:
            self.bot.log.exception(
                f"Tasks: Not sure how this happened.. some error cancelling the tasks: {sys.exc_info()[0].__name__}: {err}"
            )
