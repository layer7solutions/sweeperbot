import configparser
import sys
from datetime import datetime, timezone
from os.path import abspath, dirname, join
from typing import List, Optional

import discord
import sentry_sdk
from discord.ext import commands
from sentry_sdk.integrations.redis import RedisIntegration
from sentry_sdk.integrations.sqlalchemy import SqlalchemyIntegration

from _version import __version__
from cogs.utils import prompt
from constants import Constants, __botname__, __description__
from db.manager import DatabaseManager
from utilities.antispam import AntiSpam
from utilities.helpers import Helpers
from utilities.role_assignment import RoleAssignment
from utilities.tasks import Tasks

curdir = join(abspath(dirname(__file__)))
parentdir = join(curdir, "../")

botconfig = configparser.ConfigParser()
botconfig.read(join(parentdir, "botconfig.ini"))
__environment__ = botconfig.get("BotConfig", "ENVIRONMENT", fallback="Development")

# Create the Discord Intents
# https://discordpy.readthedocs.io/en/latest/api.html#discord.Intents
# https://discordpy.readthedocs.io/en/latest/api.html#intents
myintents = discord.Intents(
    bans=True,  # member ban/unban
    dm_messages=True,  # DM message create/update/delete
    dm_reactions=True,  # DM reaction add/remove/clear
    dm_typing=False,  # DM on typing
    emojis_and_stickers=True,  # emoji & sticker update
    guild_messages=True,  # message create/update/delete
    guild_reactions=True,  # reaction add/remove/clear
    guild_typing=False,  # guild on typing
    guilds=True,  # guild/channel join/remove/update
    integrations=False,  # integrations update
    invites=False,  # invite create/delete
    voice_states=True,  # voice state update
    webhooks=False,  # webhook update
    # Checks if certain Privileged ones are disabled (Requires verification in 100+ servers)
    presences=botconfig.getboolean("Intents", "PRIVILEGED_PRESENCE", fallback=False),
    members=botconfig.getboolean("Intents", "PRIVILEGED_SERVER_MEMBERS", fallback=False),
    message_content=botconfig.getboolean("Intents", "PRIVILEGED_MESSAGE_CONTENT", fallback=False),
)


def _prefix_callable(bot, msg):
    # Always allow mentioning the bot as a prefix
    base = [f"<@!{bot.user.id}> ", f"<@{bot.user.id}> "]
    # If in a guild, add the prefixes
    if msg.guild:
        # bot.log.debug(f"PrefixCall: Guild found: {msg.guild.id}")
        try:
            settings = bot.guild_settings.get(msg.guild.id)
            if not settings:
                # bot.log.debug(f"PrefixCall: No settings for {msg.guild.id}")
                return base
            # bot.log.debug(f"PrefixCall: {msg.guild.id} | {settings.bot_prefix}")
            if settings.bot_prefix:
                base.extend(settings.bot_prefix)
        except (KeyError, AttributeError) as error:
            bot.log.error(f"PrefixCall: Key/Att error. {error.__class__.__name__}:** {error}")
            pass

    return base


max_shard_count = 1
shard_ids = [0]


class MyBot(commands.AutoShardedBot):
    def __init__(
        self,
        *args,
        logger,
        initial_extensions: List[str],
        testing_guild_id: Optional[int] = None,
        **kwargs,
    ):
        super().__init__(
            *args,
            command_prefix=_prefix_callable,
            intents=myintents,
            description=__description__,
            help_command=commands.DefaultHelpCommand(dm_help=True),
            case_insensitive=True,
            fetch_offline_members=False,  # Due to intents, fetching offline will take a long time
            chunk_guilds_at_startup=False,  # Due to intents, fetching all members will take a long time
            max_messages=20000,
            **kwargs,
            # slash_commands=True  # Uploads all commands as slash
            # Only for testing, do not push to prod - enables slash in specific guilds, faster than ~1hr for globals
            # slash_command_guilds=[305133671776649216],
        )
        self.initial_extensions = initial_extensions
        self.testing_guild = None
        if testing_guild_id:
            self.testing_guild = discord.Object(id=testing_guild_id)
        self.database = None
        # self.owner = None
        self.owner_id = None
        self.owner_ids = None
        self.guild_settings = {}
        self.activity_index = {}
        self.cooldown_settings = None
        self.version = __version__
        self.log = logger
        self.log.info("/*********Starting App*********\\")
        self.log.info(f"App Name: {__botname__} | Version: {__version__}")
        self.log.info(f"Discord.py Version: {discord.__version__}")
        intents_temp = []
        for intent, value in self.intents:
            intents_temp.append(f"{intent}: {value}")
        self.log.info(f"Intents loaded: {intents_temp}")
        self.started_time = datetime.now(timezone.utc)
        self.botconfig = botconfig
        self.constants = Constants()
        self.imgbb_session = None
        self.log.debug(f"Initialized: Session Loop")
        self.database = DatabaseManager(self.botconfig)
        self.log.debug(f"Initialized: Database Manager")
        self.helpers = Helpers(self)
        self.log.debug(f"Initialized: Helpers")
        # Load the cooldown settings prior to loading Mod Mail or AntiSpam
        self.helpers.db_get_cooldown_settings()
        self.tasks = Tasks(self)
        self.log.debug(f"Initialized: Tasks")
        self.antispam = AntiSpam(self)
        self.log.debug(f"Initialized AntiSpam Feature")
        self.prompt = prompt.Prompt(self)
        self.log.debug(f"Initialized: Prompt")
        self.assignment = RoleAssignment(self)
        self.log.debug(f"Initialized: RoleAssignment")

        # Sets up the sentry_sdk integration:
        sentry_sdk.init(
            debug=False,  # Prints debugging info when issues sending events
            dsn=botconfig.get("BotConfig", "DSN"),
            release=__version__,
            environment=__environment__,
            integrations=[SqlalchemyIntegration(), RedisIntegration()],
            before_send=self.sentry_before_send,
            auto_enabling_integrations=True,
            auto_session_tracking=True,
        )
        self.log.debug(f"Initialized: Sentry SDK")

    async def setup_hook(self):
        # Setup Cloudinary
        self.image_api_key = self.botconfig.get("CLOUDINARY", "API_KEY")
        # csecret = self.botconfig.get("CLOUDINARY", "API_SECRET")
        # CloudinaryConfig(cloud_name="benedict9940", api_key=ckey, api_secret=csecret)

        # Ran during login, before connecting to the websocket, before on_ready
        for extension in self.initial_extensions:
            try:
                await self.load_extension(extension)
                self.log.info(f"Loaded extension {extension}")
            except Exception as err:
                self.log.exception(f"Failed to load extension {extension}. {sys.exc_info()[0].__name__}: {err}")
        self.log.info("Done loading all extensions")

    # Perform any actions to the event prior to it being sent.
    def sentry_before_send(self, event, hint):
        # Adding in the Bot Name and Bot ID to make identifying which bot the issue is on
        event.setdefault("tags", {})["Bot_Name"] = f"{self.user if self.user else None}"
        event.setdefault("tags", {})["Bot_ID"] = f"{self.user.id if self.user else None}"
        return event

    def get_guild_prefixes(self, msg, *, local_inject=_prefix_callable):
        return local_inject(self, msg)

    async def on_command_error(self, ctx, error):
        try:
            if isinstance(error, commands.NoPrivateMessage):
                await ctx.author.send("This command cannot be used in private messages.")
            elif isinstance(error, commands.DisabledCommand):
                await ctx.author.send("Sorry. This command is disabled and cannot be used.")
            elif isinstance(error, commands.CommandInvokeError):
                if isinstance(error.original, discord.Forbidden):
                    # Unable to message user. Nothing we can do
                    pass
                else:
                    self.log.exception(
                        f"In {ctx.command.qualified_name}: {error.original.__class__.__name__}: {error.original}"
                    )
            elif isinstance(error, commands.UserInputError):
                await ctx.author.send(
                    f"Sorry. There was a user error processing command: **{ctx.command.qualified_name}**.\n\n**{error.__class__.__name__}:** {error}"
                )
            elif isinstance(error, commands.BadArgument):
                await ctx.author.send(
                    f"Sorry. There was a bad argument error processing command: **{ctx.command.qualified_name}**.\n\n**{error.__class__.__name__}:** {error}"
                )
            # Responds to user if _the user_ is missing permission to use the command
            elif isinstance(error, commands.MissingPermissions):
                await ctx.author.send(f"**{error.__class__.__name__}:** {error}")
            # Responds to user if _the bot_ is missing permission to use the command
            elif isinstance(error, commands.BotMissingPermissions):
                await ctx.author.send(f"**{error.__class__.__name__}:** {error}")
            # Checks if user is listed as a Bot Owner
            elif isinstance(error, commands.NotOwner):
                await ctx.author.send(f"**{error.__class__.__name__}:** {error} This command is only for bot owners.")
            elif isinstance(error, commands.CommandOnCooldown):
                await ctx.author.send(f"**{error.__class__.__name__}:** {error}.")

        except Exception as err:
            # Wraps everything in a exception block. If error messaging user, nothing
            # we can do
            pass

    async def on_ready(self):
        # Gets all guild settings:
        self.helpers.get_all_guild_settings()
        # Load the cooldowns
        await self.antispam.set_cooldown_buckets()
        # Load the reminders
        await self.helpers.load_reminders()
        # Starts all tasks
        await self.tasks.start_tasks()
        self.log.info(f"Started all Tasks")

        # Load the mod mail
        try:
            # Get the ModMail Cog
            mmcog = self.get_cog("ModMail")
            if mmcog is None or (mmcog and mmcog.modmail_loaded is False):
                await self.load_extension("utilities.modmail")
                self.log.info(f"Loaded extension utilities.modmail")
        except commands.ExtensionAlreadyLoaded:
            pass

        if self.testing_guild:
            self.tree.copy_global_to(guild=self.testing_guild)
            await self.tree.sync(guild=self.testing_guild)
            self.log.warning(f"Synced App Command Tree to {self.testing_guild.id}")

        await self.tree.sync()
        self.log.warning(f"Synced Global App Command Tree")

        # Sets activity status
        try:
            if __environment__ == "Development":
                await self.change_presence(activity=discord.Game(name=f"In Dev v{__version__}"))
        except Exception as err:
            self.log.exception(f"Failed to set presence info. {sys.exc_info()[0].__name__}: {err}")

        # Mostly done setting up - let's try and chunk some guild info
        self.log.info(f"Almost Done Setting Up: {self.user} ({self.user.id})")
        # Chunking is slow for bots in lots of guilds, so only chunk if it's below the verification threshold and if member intent enabled
        if myintents.members and len(self.guilds) < 75:
            for guild in self.guilds:
                await guild.chunk()
                self.log.info(f"Chunked guild {guild.name} ({guild.id})")
        # All ready
        self.log.info(f"Ready: {self.user} ({self.user.id})")

    async def on_resumed(self):
        self.log.debug("Resumed Discord session...")

    async def on_error(self, event_method, *args, **kwargs):
        self.log.exception(
            f"An error was caught. Event Method: {event_method}. {sys.exc_info()[0].__name__}: {sys.exc_info()[1]}"
        )

    async def close(self):
        self.log.info("Gracefully closing sessions.")
        if self.imgbb_session:
            await self.imgbb_session.close()
            self.log.info("Closed imgbb aiohttp session")
        # Close all bot tasks
        try:
            await self.tasks.cancel_all_tasks()
        except Exception as err:
            pass
        # Close database manager
        if self.database:
            try:
                self.database.close_engine()
            except Exception as err:
                pass
        # Close the bot
        await super().close()
