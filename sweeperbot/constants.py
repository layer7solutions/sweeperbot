# All the bots constants and reusable info stored here
from discord import ActivityType

__owner__ = "Mike Wohlrab aka D0cR3d#0001 (82942340309716992)"
__botname__ = "SweeperBot"
__description__ = (
    "A discord moderation bot developed by Layer 7 Solutions (https://Layer7.Solutions). "
    "The bot is inspired from Destiny by Bungie and used in various "
    "other servers. A public version is available via the .invite command."
)


class Constants:
    def __init__(self):
        self.repository_link = "https://bitbucket.org/layer7solutions/sweeperbot/src/python-rewrite/"
        self.patreon_link = "https://www.patreon.com/Layer7"
        self.support_invite_link = "https://discord.gg/JCzdcPU"

        self.reactions = {
            "upvote": 514_265_944_894_472_193,
            "downvote": 514_265_940_817_346_604,
            "spacer": 328_352_361_569_583_105,  # also referred to as 'blank'
            "sweeperbot": 361_145_141_173_682_177,
            "animated_sweeperbot": 501_214_112_504_610_816,
            "checkmark": "\N{WHITE HEAVY CHECK MARK}",
            "guantlet": 443_192_427_587_567_617,
        }

        self.embed_description_limit: int = 4096
        self.embed_field_limit: int = 1024

        self.discord_official_invite_codes = [
            "discord-testers",  # Discord Testers/Bug Reporting server
            "events",  # Discord Events server
            "discord-feedback",  # Discord Feedback server
            "discord-api",  # Discord API Server
            "discord-linux",  # Discord Linux server
            "discord-developers",  # Discord Developers server
        ]

        self.statuses = {
            "online": "<:online:400626710262972416> Online",
            "idle": "<:away:400626681066291210> Away",
            "dnd": "<:dnd:400626641916657674> Do Not Disturb",
            "offline": "<:offline:400626796112117771> Offline or Invisible",
            "streaming": "<:streaming:400626732245450762> Streaming",
            "mobile": "<:Mobile:565663254362193925>",
            "listening": "<:spotify:796117580942671952>",
            "playing": "<:game:796121046348333076>",
            "twitch": "<:twitch:796122035591446610>",
        }

        self.activity_enum = {
            0: "Playing",
            1: "Streaming",
            2: "Listening",
            3: "Watching",
            4: "Custom",
            5: "Completing",
            6: "Unknown",
        }

        self.antispam = {
            "twitch": "abc",
            "discord_invites": r"discord(?:app\.com|\.gg|\.me)\/(?:invite\/)?(?![a-zA-Z0-9\-]+\/\w)(?:[a-zA-Z0-9\-]+)",
            "twitter": r"twitter\.com(\\|\/)\S*",
            "mixer": r"mixer\.com(\\|\/)\S*",
            "youtube": r"youtube\.com(\\|\/)\S*",
            "patreon": r"patreon\.com(\\|\/)\S*",
            "paypal": r"paypal(?:\.com|\.me)(\\|\/)\S*",
            "gofundme": r"gofundme\.com(\\|\/)\S*",
            "kickstarter": r"kickstarter\.com(\\|\/)\S*",
            "kofi": r"ko-fi\.com(\\|\/)\S*",  # ko-fi.com
            "etsy": r"etsy\.com(\\|\/)\S*",
            "soundclound": r"soundcloud\.com(\\|\/)\S*",
        }

        self.log_channels = [
            "bot-logs",
            "deleted-logs",
            "member-logs",
            "name-change-logs",
            "voice-logs",
            "reaction-logs",
        ]

        # All groups require a _default color.
        self.log_colors = {
            "user_action": {
                "_default": 0xFF8C00,
                "Mute": 0xFFCC00,
                "Unmute": 0xFFCC00,
                "Warn": 0xFFEF00,
                "Ban": 0xE50000,
                "Kick": 0x0083FF,
                "reactionAdd": 0x64997D,
                "reactionRemove": 0xED8F45,
                "worldFilter": 0x00B2EE,
            }
        }

        # Message Templates
        self.infraction_header = (
            "You've received a **{action_type}** on **{guild}**. A message from the moderators:\n\n"
        )
        self.footer_with_modmail = "\n\nIf you have questions you may respond to this message and it'll be relayed to **{guild}**'s moderators."
        self.footer_no_modmail = "\n\nIf you have questions about this please reach out to **{guild}**'s moderators."
        self.footer_appeals_server = "\n\nIf you are unable to reach the mod team, such as to appeal a ban you may join this shared Appeals Server: {appeals_invite}"

        self.normalize_regex = "[^a-z0-9]"
        self.modmail_read_receipts = "Thanks for sending us this message. This is to indicate that we've received it. Going forward you will receive a reaction on your message showing when the message is delivered as: ✉"

        # Static Messages
        self.antispam_quickmsg = "Please do not spam our server. This includes sending too many messages too quickly. Please make sure you don't split your thoughts across multiple messages and keep it to one or two messages, and don't copy and paste or send the same message in multiple channels."
