#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages
from sweeperbot.constants import __description__
from sweeperbot._version import __version__

with open("README.rst") as readme_file:
    readme = readme_file.read()

with open("HISTORY.rst") as history_file:
    history = history_file.read()

setup(
    name="sweeperbot",
    version=__version__,
    description=__description__,
    long_description=readme + "\n\n" + history,
    author="Mike Wohlrab",
    author_email="Mike@Layer7.Solutions",
    url="https://bitbucket.org/layer7solutions/sweeperbot/",
    entry_points={"console_scripts": ["sweeperbot=sweeperbot.launch:main"]},
    include_package_data=True,
    license="GNU General Public License v3",
    zip_safe=False,
    keywords=[
        "sweeperbot",
        "sweeper",
        "bot",
        "discord",
        "destinythegame",
        "benedict",
        "benedict 9940",
    ],
    classifiers=[
        "Development Status :: 2- Beta",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Natural Language :: English",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
    ],
)
